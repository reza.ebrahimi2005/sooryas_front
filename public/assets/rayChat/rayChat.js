!function () {
    function t() {
        var t = document.createElement("script");
        t.type = "text/javascript", t.async = !0, localStorage.getItem("rayToken") ? t.src = "https://app.raychat.io/scripts/js/" + o + "?rid=" + localStorage.getItem("rayToken") + "&href=" + window.location.href : t.src = "https://app.raychat.io/scripts/js/" + o;
        var e = document.getElementsByTagName("script")[0];
        e.parentNode.insertBefore(t, e)
    }

    var e = document, a = window, o = "15b0e0dc-cd9e-497a-b941-2cd9a61e3a79";
    "complete" == e.readyState ? t() : a.attachEvent ? a.attachEvent("onload", t) : a.addEventListener("load", t, !1)
}();