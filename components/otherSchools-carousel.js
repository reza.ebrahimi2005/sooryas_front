import React, {Component} from 'react';
import axios from 'axios';
import Rating from "react-rating";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import RoomIcon from '@material-ui/icons/Room';
import ClassIcon from '@material-ui/icons/Class';
import dynamic from "next/dynamic";

const Slider = dynamic(
    () => {
        return import('../components/slider');
    },
    {ssr: false}
);

export default class OtherSchoolsCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobile: false,
            data: []
        };
    }

    updateDimensions() {
        if (window.innerWidth < 575) {
            this.setState({isMobile: true});
        } else {
            this.setState({isMobile: false});
        }
    }

    componentWillMount() {
        axios.get(process.env.NEXT_PUBLIC_API_URL + '/nikarobank', {
            params: {
                id: this.props.schoolId,
                categoryId: this.props.categoryId
            }
        }).then((response) => {
            this.setState({
                data: response.data
            });
        });
    }

    componentDidMount() {
        this.setState({
            isMobile: (window.innerWidth <= 575)
        })
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {
        var content = [];
        if (typeof this.state.data[0] !== "undefined") {
            this.state.data.forEach(function (e, index) {
                content.push(
                    <div className={'school'} key={index}>
                        <div className='carousel-header'>
                            {
                                (e.schoolPic == null)
                                    ?
                                    <>
                                        <img className='img-2' src={'/assets/images/placeholder.jpg'}
                                             style={{'objectFit': 'cover'}}/>
                                    </>
                                    :
                                    <>
                                        <img className='img-1' src={e.schoolPic}/>
                                        <img className='img-2' src={e.schoolPic}/>
                                    </>
                            }
                        </div>
                        <div className='info text-center d-flex flex-column justify-content-between'>
                                            <span className="d-flex justify-content-between align-items-center">
                                                <span className='text-right'>
                                                    <h2 className='course-title d-block'>{e.title}</h2>
                                                    <span className='teacher-name d-block'>{e.manager}</span>
                                                </span>
                                                    <Rating
                                                        className='rating text-nowrap'
                                                        emptySymbol="fa fa-star-o"
                                                        fullSymbol="fa fa-star"
                                                        placeholderSymbol="fa fa-star"
                                                        fractions={2}
                                                        readonly={true}
                                                        placeholderRating={(e.avgRate !== null) ? e.avgRate : 0}
                                                    />
                                            </span>
                            <span className='d-flex justify-content-between align-items-center my-md-2 my-1'>
                                                <span className='text-primary ml-2 text-nowrap'>
                                                    <RoomIcon/>آدرس:</span>
                                                <span
                                                    className='text-nowrap'>{(e.address.region !== null) ? e.address.region.title : ""}</span>
                                            </span>
                            <Divider className='bg-nikaro-light'/>
                            <span className='d-flex justify-content-between align-items-center my-md-2 my-1'>
                                    <span className='text-primary ml-2 text-nowrap'>
                                        <ClassIcon/>دسته بندی:</span>
                                    <span className='text-nowrap'>{e.course_category.title}</span>
                                </span>
                            <Button variant="outlined" href={'/school/' + e.id}
                                    target="_blank"
                                    color={"primary"} style={{
                                'fontFamily': 'IRANSans',
                                'borderColor': '#007aff',
                                'color': '#007aff'
                            }}>اطلاعات بیشتر</Button>
                        </div>
                    </div>
                )
            })
        }
        return (
            <div className='otherSchools-carousel'>
                <strong className='col-12 text-right text-black d-block text-center text-md-right'>آموزشگاه های
                    مشابه</strong>
                <Slider
                    options={{
                        autoPlay: 4000,
                        wrapAround: false,
                        fullscreen: true,
                        pageDots: false
                    }}
                >
                    {content}
                </Slider>
            </div>
        )
    }
}
