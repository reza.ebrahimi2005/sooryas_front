import React from 'react';
import {Editor} from '@tinymce/tinymce-react';

export default class EditorBox extends React.Component {
    handleEditorChange(e) {
        console.log('Content was updated:', e.target.getContent());
    };


    render() {
        return (
            <Editor
                apiKey={'woca1ae0u57u5s36oymrgjr24qpmeelzrwsbzypfx7s5jfet'}
                init={{

                    directionality: 'rtl',
                    font_formats: 'IRANSans=IRANSans;',
                    placeholder: 'توضیحات را اینجا وارد کنید',
                    menubar: false,
                    plugins: "paste lists emoticons table wordcount link",
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | fontselect | lists | table | bullist numlist | emoticons | wordcount | link',
                    content_style: 'body { font-family: IRANSans!important; }',
                }}
                onChange={this.handleEditorChange}
            />
        );
    }
}
