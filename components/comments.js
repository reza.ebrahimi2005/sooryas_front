import React, {Component} from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import ThumbUpRoundedIcon from '@material-ui/icons/ThumbUpRounded';
import ThumbDownRoundedIcon from '@material-ui/icons/ThumbDownRounded';

export default class Comments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: []
        };

        if (props.schoolId) {
            axios.post(process.env.NEXT_PUBLIC_API_URL + '/comments/getResult', {
                type: this.props.type,
                type_id: props.id,
                page: 1
            }).then((response) => {
                this.setState({
                    loading: false,
                    data: response.data
                });
            });
        } else {
            this.setState({
                loading: false
            })
        }
    }

    submitAddCommentForm() {
        var self = this;
        var form = $('#addCommentForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        $('#addCommentForm').validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {},

            messages: {},

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
        });

        if ($('#addCommentForm').valid()) {
            var formData = new FormData($('#addCommentForm')[0]);

            formData.append('nikarobank', true);
            formData.append('type', 'school');
            formData.append('type_id', this.props.id);
            $.ajax({
                url: process.env.NEXT_PUBLIC_API_URL + '/comments/save',
                type: 'post',
                dataType: "json",
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.statusCode === 200) {
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-left",
                            "timeOut": "5000",
                        };
                        toastr['success'](result.message);
                        $('#addCommentModal').modal('hide');
                    } else {
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-center",
                            "timeOut": "5000",
                        };
                        toastr['error'](result.message);
                    }
                }
            });
        } else {
            return;
        }
    }

    handleBarRating() {
 /*      $('.rateBar').barrating('show', {
            theme: 'bars-movie',
            initialRating: 3,
        });*/
    }

    handleCommentLike(id) {
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/comments/like',
            type: 'post',
            data: {'id': id, 'nikarobank': true},
            success: function (result) {
                $('.comment[data-id=' + id + '] .likeNum').text(result);
            }
        });
    }

    handleCommentDislike(id) {
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/comments/dislike',
            type: 'post',
            data: {'id': id, 'nikarobank': true},
            success: function (result) {
                $('.comment[data-id=' + id + '] .dislikeNum').text(result);
            }
        });
    }

    render() {
        var content = [];
        var replays = [];
        var rates = [];
        if (this.state.data.length === 0 && !this.state.loading) {
            content =
                <div className="col-md-6  col-12 mx-auto">
                    <span
                        className="bg-primary text-white d-block rounded-pill p-2 text-center">نظری ثبت نشده است</span>
                </div>
        } else {
            if (typeof this.state.data[0] !== "undefined") {
                this.state.data[0].forEach(function (e, index) {
                    replays = [];
                    if (e.parentID === null)
                        content.push(
                            <div className='comment d-flex flex-column' key={index} data-id={e.id}>
                                <div className='row'>
                                    <div className='col-md-6 col-9 userInfo'>
                                        <img
                                            src={(typeof e.userPic[0].image != "undefined") ? e.userPic[0].image : '/assets/images/avatar.png'}/>
                                        <div className='text text-right'>
                                        <span className='d-block name text-black bold text-right'>
                                            {e.fullname}
                                            {
                                                (e.type === 'course')
                                                    ?
                                                    <span
                                                        className='d-block text-nowrap text-white bg-dark border border-dark p-1 px-2 rounded'
                                                        style={{'fontSize': '10px'}}>{'شرکت کننده در ' + e.courseTitle}</span>
                                                    :
                                                    null
                                            }
                                        </span>
                                            <span className='d-block date mt-2 text-right'>{e.date_time}</span>
                                        </div>
                                    </div>
                                    <div className='col-3 col-md-6 rating'>
                                        <Button className='rounded p-1 border border-primary ml-md-2 mb-1'
                                                style={{'fontFamily': 'IRANSans'}}
                                                onClick={() => this.handleCommentLike(e.id)}>
                                            <ThumbUpRoundedIcon className='text-primary ml-1 likeIcon'/>
                                            <span className='text-primary likeNum'>{e.likeNum}</span>
                                        </Button>
                                        <Button className='rounded p-1 border border-danger'
                                                style={{'fontFamily': 'IRANSans'}}
                                                onClick={() => this.handleCommentDislike(e.id)}>
                                            <ThumbDownRoundedIcon className='text-danger ml-1 likeIcon'/>
                                            <span className='text-danger dislikeNum'>{e.dislikeNum}</span>
                                        </Button>
                                    </div>
                                </div>
                                <div className='col-12 comment-text'>
                                    <p>{e.content}</p>
                                </div>
                                {
                                    this.state.data[0].forEach(function (i, index) {
                                        if (i.parentID !== null && i.parentID === e.id) {
                                            replays.push(
                                                <div className='comment-replay text-right' key={index} data-id={i.id}>
                                                    <div className='col-6 userInfo d-inline-block'>
                                                        <img
                                                            src={(i.parentID !== null) ? i.userPic : (typeof i.userPic[0].image != "undefined") ? i.userPic[0].image : '/assets/images/avatar.png'}/>
                                                        <div className='text'>
                                                            <span
                                                                className='d-block name text-black bold'>{i.fullname}</span>
                                                            <span className='d-block date'>{i.date_time}</span>
                                                        </div>
                                                    </div>
                                                    <div className='col-12 comment-text'>
                                                        <p>{i.content}</p>
                                                    </div>
                                                </div>
                                            )
                                        }
                                    })
                                }
                                {replays}
                                <Divider className='col-12 my-3'/>
                            </div>
                        );
                }.bind(this));

            }

            if (typeof this.state.data[1] !== "undefined") {
                this.state.data[1].forEach(function (e, index) {
                    rates.push(
                        <div className='form-group col-12' index={index}>
                            <label htmlFor={'rate-' + e.id}>{e.title}</label>
                            <select id={"rate-" + e.id} className='form-control rateBar' name={'rate-' + e.id}>
                                <option value="1">بسیارضعیف</option>
                                <option value="2">ضعیف</option>
                                <option value="3">متوسط</option>
                                <option value="4">خوب</option>
                                <option value="5">عالی</option>
                            </select>
                        </div>
                    )
                });
            }
        }
        return (
            <>
                <div className="modal fade" id="addCommentModal" role="dialog" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="panel panel-login">
                                                <strong className='d-block text-center'>افزودن نظر</strong>
                                                <Divider className='bg-nikaro my-2 col-4 mx-auto'/>
                                                <form id={'addCommentForm'} className='row text-right mt-4'>
                                                    <div className='form-group col-6'>
                                                        <input type="text" className='form-control' name='firstName'
                                                               placeholder='نام (اختیاری)'/>
                                                    </div>
                                                    <div className='form-group col-6'>
                                                        <input type="text" className='form-control' name='lastName'
                                                               placeholder='نام خانوادگی (اختیاری)'/>
                                                    </div>
                                                    <div className='form-group col-12'>
                                                        <textarea rows='3' type="text" className='form-control'
                                                                  name='content'
                                                                  placeholder='نظر خود را اینجا وارد کنید'/>
                                                    </div>
                                                    <span className='d-block my-2'>لطفا به سوالات زیر از کمترین (بسیارضعیف) تا بیشترین (عالی) امتیاز بدهید:</span>
                                                    {rates}
                                                    <div className='form-group col-12 mt-2'>
                                                        <Button fullWidth variant="outlined"
                                                                className='bg-primary text-white'
                                                                style={{'fontFamily': 'IRANSans'}}
                                                                onClick={this.submitAddCommentForm.bind(this)}>
                                                            ارسال
                                                        </Button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='d-flex justify-content-between px-2 my-4'>
                    <div className='text-right ml-2'>
                        <strong className='d-block'>شما هم می توانید نظر خود را در باره این آموزشگاه ثبت کنید</strong>
                        <span className='d-block'>همراهی شما موجب خشنودی ماست</span>
                    </div>
                    <Button variant="contained" color="primary" className='float-left bg-primary my-4 text-nowrap'
                            style={{'fontFamily': 'IRANSans'}} data-target='#addCommentModal' data-toggle='modal'
                            onClick={this.handleBarRating}>
                        ثبت نظر
                    </Button>
                </div>
                <div className='comments-box'>
                    {content}
                </div>
            </>
        )
    }
}
