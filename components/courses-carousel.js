import React, {Component} from 'react';
import axios from 'axios';
import Rating from "react-rating";
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import LazyLoad from 'react-lazyload';
import dynamic from "next/dynamic";


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const Slider = dynamic(
    () => {
        return import('../components/slider');
    },
    {ssr: false}
);

export default class CoursesCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        var url = process.env.NEXT_PUBLIC_API_URL+ '/templates/modules/sliderCourses';
        axios.get(url).then((response) => {
            this.setState({
                data: response.data.courses
            });
        });
    }

    render() {
        let content = [];
        let container;
        if (this.state.data) {
            this.state.data.forEach(function (e, index) {
                content.push(
                    <div className='course' key={index}>
                        <div className='header'>
                            <LazyLoad>
                                <img className='item-header-img-2'
                                     src={e.coursePic.original[0].image}/>
                                <img className='item-header-img'
                                     src={e.coursePic.original[0].image}/>
                            </LazyLoad>
                            {/*<img className='rounded teacher-pic'*/}
                            {/*     src={(typeof e.userPic.orginal !== "undefined") ? e.userPic.orginal[0].image : null}/>*/}
                            {
                                (e.discount_percent !== 0)
                                    ?
                                    <div className='discount'>
                                        <span className='text-white'>%{Math.round(e.discount_percent)}</span>
                                    </div>
                                    :
                                    null
                            }
                            {
                                (e.is_virtual)
                                    ?
                                    <div className='virtualBadge bg-success'>
                                        <span className='text-white'>مجازی</span>
                                    </div>
                                    :
                                    null
                            }
                        </div>
                        <div className='info text-center d-flex flex-column justify-content-between'>
                            <span className='d-flex justify-content-between align-items-center'>
                                    <span className=''>
                                        {e.course_start_date}
                                        <i className='fa fa-calendar'/>
                                    </span>
                                    <Rating
                                        className='rating float-left'
                                        emptySymbol="fa fa-star-o"
                                        fullSymbol="fa fa-star"
                                        placeholderSymbol="fa fa-star"
                                        fractions={2}
                                        readonly={true}
                                        placeholderRating={(e.avg_rate != null) ? e.avg_rate : 0}
                                    />
                                </span>
                            <span className='d-block'>
                                                <strong
                                                    className='course-title d-block text-nowrap overflow-hidden'>{e.title}</strong>
                                                <span className='teacher-name d-block'>{e.fullname}</span>
                                            </span>
                            <span className='d-flex justify-content-between align-items-center my-md-2 my-1'>
                                                <span className='course-days text-primary ml-2 text-nowrap'>
                                                    <InsertInvitationIcon className='ml-2'/>
                                                    روزهای برگزاری:
                                                </span>
                                {
                                    (typeof e.formationDays !== "undefined")
                                        ?
                                        (e.formationDaysWeek.id === 1)
                                            ?
                                            <span className='text-nowrap'>{e.formationDays}</span>
                                            :
                                            <span className='text-nowrap'>{e.formationDaysWeek.title}</span>
                                        :
                                        null
                                }

                            </span>
                            <Divider className='bg-nikaro-light'/>
                            <span
                                className='d-flex justify-content-between align-items-center my-1 my-md-2'>
                                                <span className='text-primary ml-2 text-nowrap'>
                                                    <QueryBuilderIcon className='ml-2'/>
                                                     ساعت جلسات:
                                                </span>
                                                <span className={'course-time d-block'}>
                                                    {e.course_formation_start_time} الی {e.course_formation_end_time}
                                                </span>
                                            </span>
                            <strong
                                className={'course-fee d-block'}>{(e.total_amount === "free" || e.discount_percent === 100) ? "رایگان " : numberWithCommas((e.total_amount - ((e.total_amount * e.discount_percent) / 100)) + " تومان")}  </strong>
                            {
                                (e.discount_percent !== 0)
                                    ?
                                    <span
                                        className={'total-amount d-block'}>{numberWithCommas(e.total_amount)} تومان</span>
                                    :
                                    null
                            }

                            <Button variant="outlined" href={'/course/' + e.id} color={"primary"}
                                    style={{
                                        'fontFamily': 'IRANSans',
                                        'borderColor': '#007aff',
                                        'color': '#007aff'
                                    }}>
                                مشاهده دوره
                            </Button>
                        </div>
                    </div>
                )
            });
            container =
                <>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}/>
                    <Slider
                        options={{
                            autoPlay: 4000,
                            pauseAutoPlayOnHover: true,
                            contain: true,
                            fullscreen: true,
                            pageDots: false,
                            wrapAround: true
                        }}
                    >
                        {content}
                    </Slider>
                    <a href='/search?q=&courseSchool=1' className='text-primary d-block mt-2 text-center'>نمایش
                        بیشتر</a>
                </>

        } else {
            container = null;
        }
        return (
            <>
                {container}
            </>
        )
    }
}
