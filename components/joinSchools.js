import React, {useState,useEffect} from 'react';
import {create} from 'jss';
import rtl from 'jss-rtl';
import {createMuiTheme, makeStyles, ThemeProvider, StylesProvider, jssPreset} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import TextField from "@material-ui/core/TextField";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import CategorySelect from './category-select';
import SubCategorySelect from './subCategory-select';
import BankSelect from './bank-select';
import CitySelect from './city-select';
import RegionSelect from './region-select';
import Loading from './loading';
import Map from "./map";
import EditorBox from "./editor";


// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, rtl()]});

const theme = createMuiTheme({
    direction: "rtl",
    typography: {
        fontFamily: 'IRANSans',
        textAlign: 'right'
    },
    palette: {
        primary: {
            main: '#007aff',
        },
        secondary: {
            light: '#0066ff',
            main: '#0044ff',
            contrastText: '#ffcc00',
        },
    },
});

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    formButton: {
        marginRight: theme.spacing(1),
        fontFamily: "IRANSans",
        color: "#007aff",
        borderColor: "#007aff",
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        width: '70%',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '70%',
        height: '400px',
        margin: '0 auto',
        marginTop: '20px',
    },
    map: {
        width: '100%',
        height: '200px',
        border: '1px solid rgba(0, 0, 0, 0.42)',
        borderRadius: '10px'
    },
    button: {
        color: '#ffffff',
        fontSize: 18,
        backgroundColor: '#17ace6',
        marginTop: 20,
        padding: '5px 35px',
        fontFamily: "IRANSans"
    },
    [theme.breakpoints.down('sm')]: {
        paper: {
            width: '100%',
            height: '100%',
            padding: '5px',
        },
        stepper: {
            padding: '0',
        },
        form: {
            width: '95%',
            marginBottom: '30px'
        },
    }
}));

function getSteps() {
    return ['مشخصات آموزشگاه', 'توضیحات آموزشگاه', 'آپلود تصاویر', 'موقعیت مکانی', 'مشخصات بانکی', 'مشخصات کاربری'];
}

export default function joinSchools(props) {
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const [loading, setLoading] = useState(false);
    const [cityId, setCityId] = useState(null);
    const steps = getSteps();
    const [open, setOpen] = useState(!!(props.open));
    const [map, setMap] = useState(null);
    const [lat, setLat] = useState(0);
    const [lng, setLng] = useState(0);
    const [subCategorySelect, setSubCategorySelect] = useState(<SubCategorySelect id={0}/>);
    const [openProp, setOpenProp] = useState(false);
    const Editor = <EditorBox/>;

    useEffect(()=>{
        // setMap(<Map id="null" lat={handleLatChange} lng={handleLngChange}/>)
    })

    function getStepContent(stepIndex) {
        const classes = useStyles();
    }

    const handleLatChange = (value) => {
        setLat(value);
    };

    const handleLngChange = (value) => {
        setLng(value)
    };

    const showFileName = (e) => {
        var input = event.srcElement;
        var fileName = input.files[0].name;
        var inputName = input.name;
        $('input[name="' + inputName + '"]').parent().find('.fileName').text(fileName);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setOpenProp(false);
        props.close(true)
    };

    const handleNext = () => {
        var self = this;
        var form = $('#joinSchoolsForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        $.validator.addMethod("regex", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "");

        $('#joinSchoolsForm').validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                //account
                schoolName: {
                    required: true,
                },
                manager: {
                    required: true,
                },
                category_id: {
                    required: true,
                },
                subCategory_id: {
                    required: true
                }
            },

            messages: {
                schoolName: {
                    required: 'فیلد فامیل را وارد نمایید'
                },
                manager: {
                    required: 'فیلد نام را وارد نمایید'
                },
                category_id: {
                    required: 'فیلد نام را وارد نمایید'

                },
                subCategory_id: {
                    required: 'فیلد نام را وارد نمایید'
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertBefore(element);
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.MuiInput-formControl').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.MuiInput-formControl').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.MuiInput-formControl').removeClass('has-error'); // set success class to the control group
            }
        });

        if ($('#joinSchoolsForm').valid()) {
            if (activeStep === 0) {
                if ($('input[name="category_id"]').val() === "") {
                    $('.categoryLabel').removeClass('d-none').addClass('d-block');
                    return;
                }
                if ($('input[name="subCategory_id"]').val() === "") {
                    $('.subCategoryLabel').removeClass('d-none').addClass('d-block');
                    return;
                }
            }
            if (activeStep === 1) {
                if (tinymce.activeEditor.getContent() === "") {
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "4000",
                    };
                    toastr["error"]("بخش توضیحات را کامل کنید.");
                    return;
                }
            }
            if (activeStep === 3) {
                if ($('#joinSchoolsForm input[name="location_lng"]').val() === "" || $('#joinSchoolsForm input[name="location_lat"]').val() === "") {
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "4000",
                    };
                    toastr["error"]("لطفا مکان خود را روی نقشه مشخص کنید!");
                    return;
                }
                if ($('#joinSchoolsForm input[name="region_id"]').val() === "") {
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "4000",
                    };
                    toastr["error"]("لطفا منطقه شهری را انتخاب کنید!");
                    return;
                }
            }
            setActiveStep(prevActiveStep => prevActiveStep + 1);
            $('.step').addClass('d-none');
            $('.step' + (activeStep + 1)).removeClass('d-none');
        } else {
            return;
        }
    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
        $('.step').addClass('d-none');
        $('.step' + (activeStep - 1)).removeClass('d-none');
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    const handleSubmit = () => {
        if ($('#joinSchoolsForm').valid()) {
            let myForm = document.getElementById('joinSchoolsForm');
            var data = new FormData(myForm);
            data.set('desciption', tinymce.activeEditor.getContent());
            data.set('location_lat', lat);
            data.set('location_lng', lng);

            setLoading(true);
            $.ajax({
                url: process.env.NEXT_PUBLIC_API_URL +'/school/register',
                data: data,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                method: "post",
                success: function (data) {
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "6000",
                    };
                    setLoading(false);
                    toastr["success"]('حساب کاربری مجموعه شما در نیکارو با موفقیت ثبت شد.' +
                        'در اولین فرصت کارشناسان ما جهت ارایه توضیحات درباره خدمات نیکارو  و تکمیل اطلاعات شما تماس خواهند گرفت.');
                    handleClose();
                }
            });
        } else {
            return;
        }
    };

    const handleId = (id) => {
        setMap(<Map id={id.value} lng={handleLngChange} lat={handleLatChange}/>);
        setCityId(id.value);
    };

    const getCategoryId = (value) => {
        setSubCategorySelect(<SubCategorySelect id={value.value}/>)
    };

    if (props.open !== openProp && props.open === true) {
        setOpenProp(true);
        setOpen(true);
    }

    return (
        <StylesProvider jss={jss}>
            <ThemeProvider theme={theme}>
                {loading &&
                <Loading/>
                }
                <Modal
                    id='joinSchoolsModal'
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={open}>
                        <div className={classes.paper}>
                            <div className={classes.root}>
                                <i className='fa fa-times d-block text-black text-right p-2 h5' onClick={handleClose}/>
                                <Stepper activeStep={activeStep} alternativeLabel className={classes.stepper}>
                                    {steps.map(label => (
                                        <Step key={label}>
                                            <StepLabel>{label}</StepLabel>
                                        </Step>
                                    ))}
                                </Stepper>
                                <>
                                    {activeStep === steps.length ? (
                                        <div>
                                            <Typography className={classes.instructions}>All steps
                                                completed</Typography>
                                            <Button onClick={handleReset}>Reset</Button>
                                        </div>
                                    ) : (
                                        <>
                                            <form id={'joinSchoolsForm'} className={classes.form} noValidate
                                                  autoComplete="off">
                                                <Grid container spacing={2} justify={'center'} alignItems={'center'}
                                                      className={'step step0'}>
                                                    <Grid item xs={12}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            id="schoolName"
                                                            label="نام آموزشگاه یا مجموعه"
                                                            name="schoolName"
                                                            autoFocus
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            className={classes.input}
                                                            margin="none"
                                                            required
                                                            fullWidth
                                                            name="manager"
                                                            label="نام مدیریت"
                                                            type="text"
                                                            id="manager"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            className={classes.input}
                                                            margin="none"
                                                            required
                                                            fullWidth
                                                            type='number'
                                                            name="phone"
                                                            label="شماره تماس"
                                                            id="phone"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <CategorySelect func={getCategoryId}/>
                                                        <label className='d-none categoryLabel text-right'
                                                               htmlFor='category_id'>این فیلد الزامی است</label>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        {subCategorySelect}
                                                        <label className='d-none subCategoryLabel text-right'
                                                               htmlFor='subCategory_id'>این فیلد الزامی است</label>
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            margin="none"
                                                            fullWidth
                                                            name="website"
                                                            label="وب سایت"
                                                            type="website"
                                                            id="website"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            margin="none"
                                                            fullWidth
                                                            name="email"
                                                            label="ایمیل"
                                                            type="email"
                                                            id="email"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            margin="none"
                                                            fullWidth
                                                            name="telegram"
                                                            label="آدرس تلگرام"
                                                            type="text"
                                                            id="text"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            margin="none"
                                                            fullWidth
                                                            name="instagram"
                                                            label="آدرس اینستاگرام"
                                                            type="text"
                                                            id="instagram"
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={2} justify={'center'} alignItems={'center'}
                                                      className={'step step1 d-none'}>
                                                    <Grid item xs={12}>
                                                        <span
                                                            className='d-block text-right text-primary mb-4'>لطفا توضیحاتی راجب آموزشگاه خود در کادر زیر وارد نمایید. (این توضیحات در صفحه آموزشگاه شما نمایش داده می شود)</span>
                                                        {Editor}
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={2} justify={'center'} alignItems={'center'}
                                                      className={'step step2 d-none'}>
                                                    <Grid item xs={12}>
                                                        <div className="input-group mb-3 text-right">
                                                            <div className="custom-file">
                                                                <input type="file" className="custom-file-input"
                                                                       id="inputGroupFile01"
                                                                       aria-describedby="inputGroupFileAddon01"
                                                                       name='logo'
                                                                       onChange={showFileName}
                                                                />
                                                                <label className="custom-file-label"
                                                                       htmlFor="inputGroupFile01">بارگذاری
                                                                    لوگو</label>
                                                                <span className='d-block text-primary fileName'/>
                                                            </div>
                                                        </div>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <div className="input-group mb-3 text-right">
                                                            <div className="custom-file">
                                                                <input type="file" className="custom-file-input"
                                                                       id="inputGroupFile02"
                                                                       aria-describedby="inputGroupFileAddon02"
                                                                       name='banner'
                                                                       onChange={showFileName}
                                                                />
                                                                <label className="custom-file-label"
                                                                       htmlFor="inputGroupFile02"> بارگزاری تصویر محیط
                                                                    کلاس یا آموزشگاه</label>
                                                                <span className='d-block text-primary fileName'/>
                                                            </div>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={2} justify={'center'} alignItems={'center'}
                                                      className={'step step3 d-none'}>
                                                    <Grid item xs={12}>
                                                        <CitySelect required={true} func={handleId}/>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <RegionSelect cityId={cityId}/>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            autoFocus
                                                            id="address"
                                                            label="آدرس"
                                                            name="address"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <label className='float-right my-2'>موقعیت جفرافیایی (لطفا پس از
                                                            انتخاب شهر مکان خود را روی نقشه مشخص کنید)</label>
                                                    </Grid>
                                                    <Grid item xs={12} className={classes.map} id='map'>
                                                        {map}
                                                        <Input type='hidden' value='' name='location_lat'/>
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={2} justify={'center'} alignItems={'center'}
                                                      className={'step step4 d-none'}>
                                                    <Grid item xs={12}>
                                                        <TextField
                                                            fullWidth
                                                            autoFocus
                                                            type='text'
                                                            id="bankAccountOwner"
                                                            label="نام صاحب حساب"
                                                            name="bankAccountOwner"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <BankSelect/>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <TextField
                                                            fullWidth
                                                            id="accountId"
                                                            label="شماره حساب"
                                                            type='number'
                                                            name="accountId"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <TextField
                                                            fullWidth
                                                            id="sheba"
                                                            type='number'
                                                            label="شماره شبا"
                                                            name="sheba"
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={2} justify={'center'} alignItems={'center'}
                                                      className={'step step5 d-none'}>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            id="userFirstname"
                                                            label="نام"
                                                            name="userFirstname"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            id="userLastname"
                                                            label="نام خانوادگی"
                                                            name="userLastname"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            id="username"
                                                            label="نام کاربری"
                                                            name="username"
                                                            inputProps={{pattern: "[a-z]{4,}"}}
                                                            title={'لطفا از کارکتر های لاتین و حداقل 4 کاراکتر استفاده کنید'}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            id="password"
                                                            label="کلمه عبور"
                                                            type='password'
                                                            name="password"
                                                        />
                                                    </Grid>
                                                    <Grid item xs={6}>
                                                        <TextField
                                                            required
                                                            fullWidth
                                                            id="confirmPass"
                                                            type='password'
                                                            label="تایید کلمه عبور"
                                                            name="confirmPass"
                                                        />
                                                    </Grid>
                                                </Grid>
                                            </form>
                                            <div className='d-flex justify-content-between'
                                                 style={{'padding': '0 5%'}}>
                                                <Button
                                                    variant={"outlined"}
                                                    disabled={activeStep === 0}
                                                    onClick={handleBack}
                                                    color={"primary"}
                                                    className={classes.formButton}
                                                >
                                                    <KeyboardArrowRight/>
                                                    قبلی
                                                </Button>
                                                {activeStep === steps.length - 1
                                                    ?
                                                    <Button className={classes.formButton} variant="outlined"
                                                            onClick={handleSubmit}>
                                                        ثبت
                                                    </Button>
                                                    :
                                                    <Button className={classes.formButton} variant="outlined"
                                                            onClick={handleNext}>
                                                        بعدی
                                                        <KeyboardArrowLeft/>
                                                    </Button>
                                                }

                                            </div>
                                        </>
                                    )}
                                </>
                            </div>
                        </div>
                    </Fade>
                </Modal>
            </ThemeProvider>
        </StylesProvider>
    );
}
