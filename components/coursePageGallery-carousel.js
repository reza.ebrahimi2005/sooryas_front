import React, {Component} from 'react';
import axios from 'axios';
import Divider from "@material-ui/core/Divider";
import FramerMotionSlider from "../components/framerMotionSlider";


export default class CoursePageGalleryCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gallery: [],
            videos: null,
            data: null,
        };
        var self = this;
        axios({
            method: 'post',
            url: process.env.NEXT_PUBLIC_API_URL+'/singlePage/getCourseGallery',
            data: {'course_id': self.props.id}
        }).then(function (response) {
            var videos = [];
            var items = [];
            if (typeof response.data[0] !== "undefined")
                response.data[0].forEach(function (e, index) {
                    videos.push(
                        <>
                            <video controls key={index} className='my-5'>
                                <source src={e}/>
                            </video>
                            <Divider/>
                        </>
                    )
                });
            var temp =
                <>
                    <div className='d-flex justify-content-center align-items-center flex-column'>
                        {videos}
                    </div>
                    <FramerMotionSlider data={(typeof response.data[1] !== "undefined") ? response.data[1] : items}/>
                </>;

            self.setState({
                data: temp,
            })
        });
    }

    render() {
        return (
            <>
                {this.state.data}
            </>
        )
    }
}
