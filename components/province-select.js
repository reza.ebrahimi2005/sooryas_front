import React, {Component} from 'react';
import AsyncSelect from 'react-select/async';
import axios from 'axios';

const CancelToken = axios.CancelToken;
let cancel;

export default class ProvinceSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: this.props.defaultValue,
            defaultOption: []
        };
        this.getOptions = this.getOptions.bind(this);
        var options = [];
        axios.get(process.env.NEXT_PUBLIC_API_URL+'/selectProvince').then((response) => {
            response.data.forEach(function (e) {
                options[e.id] = {'value': e.id, 'label': e.text}
            });
            var url_string = window.location.href;
            var url = new URL(url_string);
            var selectedOption = null;

            if (url.searchParams.get('provinceId') != null && url.searchParams.get('provinceId') !== 0) {
                this.setState({
                    selectedOption: options[url.searchParams.get('provinceId')],
                    defaultOption: options
                })
            } else {
                this.setState({
                    selectedOption: (this.props.id) ? options[this.props.id] : null,
                    defaultOption: options
                })
            }
        });


    }


    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
    };

    mapOptionsToValues(options) {
        return options.map(option => ({
            value: option.id,
            label: option.text
        }));
    };

    getOptions(inputValue, callback) {
        if (!inputValue) {
            return callback([]);
        }

        cancel && cancel();

        axios.get(process.env.NEXT_PUBLIC_API_URL+'/selectProvince', {
            params: {
                term: inputValue
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel = c;
            })
        }).then((response) => {
            const results = response.data;
            if (this.props.mapOptionsToValues)
                callback(this.props.mapOptionsToValues(results));
            else callback(this.mapOptionsToValues(results));
        });

    };


    render() {
        const customStyles = {
            input: (provided, state) => ({
                ...provided,
                border: 'none'
            }),
        };
        return (
            <AsyncSelect
                onChange={this.handleChange.bind(this)}
                placeholder={'انتخاب استان'}
                loadOptions={this.getOptions}
                value={this.state.selectedOption}
                defaultOptions={this.state.defaultOption}
                classNamePrefix={'react-select'}
                isClearable={true}
            />
        )
    }
}
