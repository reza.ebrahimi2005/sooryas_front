import React, {Component} from 'react';
import TelegramIcon from '@material-ui/icons/Telegram';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import Button from '@material-ui/core/Button';


export default class InviteFriends extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        console.log(props);
    }

    copyCode() {
        var copyText = document.getElementById("inviteCode");
        copyText.select();
        copyText.setSelectionRange(7, 999999);
        document.execCommand("copy");
        toastr.options = {
            "progressBar": true,
            "positionClass": "toast-top-left",
            "timeOut": "4000",
        };
        toastr['success']('کد شما کپی شد');
    }

    render() {
        return (
            <div className='row mt-4' id='inviteFriends'>
                <div className="col-md-4 col-12 text-center">
                    <img src={'/assets/images/invite.svg'}/>
                </div>
                <div className="col-md-8 col-12 text-right mt-4">
                    <span className="text-black bold d-block">سوریاس  را به دوستانتان معرفی کنید!</span>
                    <p className="text-justify mt-2">
                        کد تخفیف زیر را برای دوستانتان ارسال کنید. به ازای اولین ثبت نام هر یک از دوستان که با کدمعرف
                        شما ثبت نهایی را انجام داده اند 6 هزار تومان به اعتبار کیف پول شما اضافه خواهد شد و دوستتان نیز
                        4 هزار تومان تخفیف خواهد گرفت.
                    </p>
                    <form className="text-center">
                        <input type="text" value={'کد شما: ' + this.props.data} id={'inviteCode'} className="mt-4"
                               style={{'height': '40px'}}/>
                        <button type="button" className="btn btn-primary rounded-pill mr-2" onClick={this.copyCode}>کپی
                            کردن
                        </button>
                        <span className='text-center mt-4 d-block'>همین الان به اشتراک بگذارید</span>
                        <Button className='mx-auto'
                                href={"https://telegram.me/share/url?url=https://www.nikaro.ir/users/sharereferral?code=" + this.props.data}>
                            <TelegramIcon className='text-primary'/>
                        </Button>
                        <Button className='mx-auto'
                                href={"whatsapp://send?text=نیکارو | پلتفرم دوره های آموزش حضوری https://www.nikaro.ir/users/sharereferral?code=" + this.props.data}
                                data-action="share/whatsapp/share">
                            <WhatsAppIcon className='text-primary'/>
                        </Button>
                    </form>

                </div>
            </div>

        );
    }
}
