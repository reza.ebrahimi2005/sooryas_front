import React from "react";
import jquery from "jquery";
import axios from 'axios';

export class ModalCompose extends React.Component {
    constructor(props) {
        super(props);
        this.show = this.show.bind(this);
        this.state = {};
    }

    show(idx) {
        /* open message in modal */
        jquery(this.refs.composeModal).modal("show");
    }

    submitForm() {
        var form = document.getElementById('replayForm');
        var data = new FormData(form);
        axios.post('/helpdesk/reply', data).then((response) => {
            toastr.success(response.data.message);
            $('.modal').modal('hide');
            $('.modal-backdrop').remove();
            this.props.parent.refreshMessages();
        });
    }

    render() {
        return (
            <div
                id="composeModal"
                className="modal fade mt-0 mt-md-5"
                tabIndex="-1"
                role="dialog"
                aria-hidden="true"
                ref="composeModal"
            >
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="text-uppercase text-muted">ایجاد پیام</h5>
                            <button
                                type="button"
                                className="close float-left m-0 p-0"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">×</span>
                                <span className="sr-only">Close</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form className="form" role="form" autoComplete="off" id='replayForm'>
                                <div className="form-row py-2">
                                    <label htmlFor="sendTo" className="col-sm-2 mb-0">ارسال به</label>
                                    <div className="col">
                                        <input
                                            type="hidden"
                                            name="typeId"
                                            id="typeId"
                                            className="form-control"
                                            required=""
                                            value={this.props.typeId}
                                        />
                                        <input
                                            type="hidden"
                                            name="parentId"
                                            id="parentId"
                                            className="form-control"
                                            required=""
                                            value={this.props.parentId}
                                        />
                                        <input
                                            type="text"
                                            name="sendTo"
                                            id="sendTo"
                                            className="form-control"
                                            required=""
                                            value={this.props.sendTo}
                                        />
                                    </div>
                                </div>
                                <div className="form-row py-2">
                                    <label htmlFor="subject" className="col-sm-2 mb-0">عنوان</label>
                                    <div className="col">
                                        <input
                                            type="text"
                                            name="title"
                                            id="subject"
                                            className="form-control"
                                            required=""
                                        />
                                    </div>
                                </div>
                                <div className="form-row py-2">
                                    <label htmlFor="message2" className="col-sm-2 mb-0">متن پیام</label>
                                    <div className="col">
                    <textarea
                        rows="6"
                        name="textBody"
                        id="message2"
                        className="form-control"
                        required=""
                    />
                                    </div>
                                </div>
                                <div className="form-row py-2" style={{'direction': 'ltr'}}>
                                    <div className="col-auto py-1">
                                        <button
                                            type="button"
                                            className="btn btn-secondary float-right ml-1"
                                            onClick={this.submitForm.bind(this)}
                                        >ارسال
                                        </button>
                                        {/*<button*/}
                                        {/*type="submit"*/}
                                        {/*className="btn btn-outline-secondary float-right"*/}
                                        {/*>ذخیره پیش نویس</button>*/}
                                    </div>
                                    {/*<div className="col-sm-auto py-1">*/}
                                    {/*<button*/}
                                    {/*type="submit"*/}
                                    {/*className="btn btn-outline-secondary btn-block"*/}
                                    {/*>فایل<i className="align-middle icon-paper-clip fa fa-paperclip ml-1" />*/}
                                    {/*</button>*/}
                                    {/*</div>*/}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalCompose;
