import React from "react";
import jquery from "jquery";

export class ModalMessage extends React.Component {
    constructor(props) {
        super(props);
        this.show = this.show.bind(this);
    }

    show() {
        jquery(this.refs.messageModal).modal("show");
    }

    render() {
        return (
            <div
                id="messageModal"
                className="modal fade mt-0 mt-md-5 text-right"
                tabIndex="-1"
                role="dialog"
                aria-hidden="true"
                ref="messageModal"
            >
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <div>
                                <small className="text-uppercase text-muted">عنوان</small>
                                <h6 className="modal-title">{this.props.message.subject}</h6>
                            </div>
                            <button
                                type="button"
                                className="close float-left m-0 p-0"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">×</span>
                                <span className="sr-only">Close</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-sm-8">
                                    <small className="text-uppercase text-muted">فرستنده</small>
                                    <h6>
                                        <a href="'mailto:'+selected.fromAddress">
                                            {this.props.message.from}
                                        </a>
                                    </h6>
                                </div>
                                <div className="col-sm-4">
                                    <small className="text-uppercase text-muted">درتاریخ</small>
                                    <h6>{this.props.message.dtSent}</h6>
                                </div>
                                <div className="col-sm-12">
                                    <p
                                        dangerouslySetInnerHTML={{
                                            __html: this.props.message.body
                                        }}
                                    />
                                </div>
                            </div>
                            <p className="my-3"/>
                            <button
                                className="btn btn-primary float-left mr-2"
                                data-dismiss="modal"
                                aria-hidden="true"
                            >بستن
                            </button>
                            {
                                (this.props.parent.state.selectedType === 'inbox')
                                    ?
                                    <button
                                        className="btn btn-outline-primary float-left"
                                        data-dismiss="modal"
                                        data-toggle="modal"
                                        data-target="#composeModal"
                                    >پاسخ</button>
                                    :
                                    null
                            }

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalMessage;
