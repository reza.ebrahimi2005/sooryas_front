import React, {Component} from "react";
import {InboxHtml} from "./InboxHtml";
import ModalCompose from "./ModalCompose";
import ModalMessage from "./ModalMessage";
import axios from 'axios';
import Loading from '../loading';
import Cookies from "js-cookie";

export default class Inbox extends Component {
    constructor(props) {
        super(props);
        this.markRead = this.markRead.bind(this);
        this.doShow = this.doShow.bind(this);
        this.doDelete = this.doDelete.bind(this);
        this.toggleMark = this.toggleMark.bind(this);
        this.toggleMarkAll = this.toggleMarkAll.bind(this);
        this.deleteMarked = this.deleteMarked.bind(this);
        this.refreshMessages = this.refreshMessages.bind(this);
        this.deleteMessages = this.deleteMessages.bind(this);
        this.doSent = this.doSent.bind(this);
        this.doTrash = this.doTrash.bind(this);
        this.ModalMessage = React.createRef();
        this.ModalCompose = React.createRef();
        this.state = {
            initMessages: [],
            messages: [],
            selectedType: 'inbox',
            selected: {},
            deleted: [],
            loading: false,
        };
    }

    componentWillMount() {
        const uid = Cookies.get('uid');
        axios.get('http://nikaro.co/api/web/helpdesk/getUserMessage?boxType=inbox&userId='+uid).then((response) => {
            this.setState({
                messages: response.data
            })
        });
    }

    markRead(idx) {
        /* mark this message as read */
        let messages = [...this.state.messages];
        messages[idx].read = true;
        this.setState({messages});
    }

    doSent() {
        this.setState({
            loading: true
        });
        axios.get('http://nikaro.co/api/web/helpdesk/getUserMessage?boxType=sent').then((response) => {
            this.setState({
                selectedType: 'sent',
                messages: response.data,
                loading: false,
            })
        });
    }

    doTrash() {
        this.setState({
            loading: true
        });
        axios.get('http://nikaro.co/api/web/helpdesk/getUserMessage?boxType=trash').then((response) => {
            this.setState({
                selectedType: 'trash',
                messages: response.data,
                loading: false,
            })
        });
    }

    doShow(idx) {
        var index;
        this.state.messages.filter((v, k) => {
            if (v.id === idx) {
                index = k;
            }
        });
        this.markRead(index);
        axios.get('/helpdesk/view?id='+idx).then((response) => {
            console.log(response.data.message);
        });
        this.setState({
            selected: this.state.messages[index]
        });
        /* open message in modal */
        this.ModalMessage.current.show();
    }

    doCompose() {
        /* open compose modal */
        this.ModalCompose.current.show();
    }

    toggleMark(idx) {
        let messages = [...this.state.messages];
        messages[idx].marked = messages[idx].marked ? 0 : 1;
        this.setState({messages});
    }

    doDelete(idx) {
        this.setState({
            loading: true
        });
        axios.get('http://nikaro.co/api/web/helpdesk/deleteById?id=' + idx).then((response) => {
            this.setState({
                selectedType: 'trash',
                loading: false,
            });
            toastr.success(response.data.message);
            this.refreshMessages();
            $('#inboxBtn').trigger('click');
        });
    }

    toggleMarkAll() {
        let messages = [...this.state.messages];
        messages.map((v, k) => {
            return (v.marked = v.marked ? 0 : 1);
        });
        this.setState({messages});
    }

    deleteMarked() {
        var self = this;
        let messages = [...this.state.messages];
        var tbd = [];
        for (var k = 0; k < messages.length; k++) {
            if (messages[k].marked === 1) {
                tbd.push(k);
            }
        }
        if (tbd.length > 0) {
            self.deleteMessages(tbd);
        }
    }

    refreshMessages() {
        this.setState({
            loading: true
        });
        axios.get('http://nikaro.co/api/web/helpdesk/getUserMessage?boxType=inbox').then((response) => {
            this.setState({
                selectedType: 'inbox',
                messages: response.data,
                loading: false,
            })
        });
    }

    deleteMessages(arr) {
        let messages = [...this.state.messages];
        let deleted = [...this.state.deleted];
        for (var i = arr.length - 1; i >= 0; i--) {
            deleted.push(messages[i]);
            messages.splice(arr[i], 1);
        }
        this.setState({messages, deleted});
    }

    render() {
        return (
                <div>
                    {
                        (this.state.loading) ? <Loading/> : null
                    }
                    <InboxHtml parent={this}/>
                    <ModalCompose sendTo={this.state.selected.from} typeId={this.state.selected.fromAddress}
                                  parentId={this.state.selected.id} parent={this}/>
                    <ModalMessage ref={this.ModalMessage} message={this.state.selected} parent={this}/>
                </div>
        );
    }
}
