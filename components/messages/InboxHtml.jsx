import React from "react";

export const InboxHtml = ({parent}) => {
    return (
        <main className="px-2 flex-fill text-right">
            <div className="row">
                <div className="col-12 px-4 d-flex flex-column">
                    <div className="row">
                        <div className="col-lg-3 col-md-4 py-3">
                            <ul className="list-group sticky-top sticky-offset px-0"
                                style={{'top': '8vh', 'z-index': '99', 'marginTop': '50px'}}>
                                {/*<button*/}
                                {/*className="btn btn-block btn-outline-secondary my-1 text-uppercase"*/}
                                {/*data-target="#composeModal"*/}
                                {/*data-toggle="modal"*/}
                                {/*>ایجاد پیام<i className="align-middle icon-pencil"/>*/}
                                {/*</button>*/}
                                <div className="nav nav-pills py-2 flex-md-column justify-content-center">
                                    <a
                                        id='inboxBtn'
                                        href={{void: 0}}
                                        className="nav-link active"
                                        title="Messages"
                                        data-toggle="tab"
                                        data-target="#messages"
                                        onClick={parent.refreshMessages}
                                    >
                                        <span className="icon icon-envelope fa fa-fw fa-inbox mr-md-1"/>
                                        <span className="d-none d-md-inline"> پیام های دریافتی </span>
                                        <span
                                            className="badge badge-pill badge-danger small font-weight-light mr-1"
                                            title="Unread"
                                        >
                                          {
                                              (parent.state.selectedType === 'inbox')
                                                  ?
                                                  parent.state.messages.filter((v, k) => {
                                                      return !v.read;
                                                  }).length
                                                  :
                                                  null
                                          }
                                        </span>
                                    </a>
                                    <a
                                        href={{void: 0}}
                                        className="nav-link"
                                        title="ارسالی ها"
                                        data-toggle="tab"
                                        data-target="#sent"
                                        onClick={parent.doSent}
                                    >
                                        <span className="icon icon-trash fa fa-fw fa-edit mr-md-1"/>
                                        <span className="d-none d-md-inline"> پیام های ارسالی</span>
                                    </a>
                                    <a
                                        href={{void: 0}}
                                        className="nav-link"
                                        title="حذف شده ها"
                                        data-toggle="tab"
                                        data-target="#deleted"
                                        onClick={parent.doTrash}
                                    >
                                        <span className="icon icon-pencil fa fa-fw fa-trash mr-md-1"/>
                                        <span className="d-none d-md-inline"> پیام های حذف شده</span>
                                    </a>
                                </div>
                            </ul>
                        </div>
                        <div className="col-md py-3 tab-content">
                            <div id="messages" className="tab-pane active">
                                <div className="d-flex flex-sm-row flex-column py-1 mb-1" style={{'direction': 'ltr'}}>
                                    <div className="btn-group">
                                        <button
                                            type="button"
                                            className="btn btn-outline-secondary text-uppercase"
                                            onClick={parent.toggleMarkAll}
                                        >
                                            <div
                                                className="custom-control custom-checkbox"
                                                onClick={parent.toggleMarkAll}
                                            >
                                                <input
                                                    type="checkbox"
                                                    className="custom-control-input"
                                                    id="checkAll"
                                                    defaultChecked={false}
                                                    onChange={parent.toggleMarkAll}
                                                />
                                                <label
                                                    className="custom-control-label"
                                                    htmlFor="checkAll"
                                                >انتخاب همه</label>
                                            </div>
                                        </button>
                                        {parent.state.messages &&
                                        parent.state.messages.filter((v, k) => {
                                            if (v.marked === 1) {
                                                return v;
                                            }
                                        }).length > 0 ? (
                                            <div className="btn-group mr-sm-auto mr-none">
                                                <button
                                                    type="button"
                                                    className="btn btn-outline-secondary dropdown-toggle text-uppercase"
                                                    data-toggle="dropdown"
                                                />
                                                <div className="dropdown-menu" id="dd1">
                                                    <a
                                                        className="dropdown-item"
                                                        onClick={parent.deleteMarked}
                                                    >حذف موارد علامت دار</a>
                                                </div>
                                            </div>
                                        ) : null}
                                    </div>
                                    {/*<button*/}
                                    {/*type="button"*/}
                                    {/*className="btn btn-outline-secondary mx-sm-1 mx-none"*/}
                                    {/*onClick={parent.refreshMessages}*/}
                                    {/*>*/}
                                    {/*<i className="align-middle icon-refresh fa fa-sync"/>*/}
                                    {/*</button>*/}
                                    {/*<button*/}
                                    {/*type="button"*/}
                                    {/*className="btn btn-outline-secondary ml-1 mr-none"*/}
                                    {/*data-target="#composeModal"*/}
                                    {/*data-toggle="modal"*/}
                                    {/*>*/}
                                    {/*<i className="align-middle icon-pencil fa fa-edit"/>*/}
                                    {/*</button>*/}
                                </div>
                                {/* message list */}
                                <ul className="list-group py-2 mx-auto px-0">
                                    {parent.state.messages && parent.state.messages.length > 0
                                        ? parent.state.messages.map((item, idx) => (
                                            <li
                                                key={item.id}
                                                className="list-group-item list-group-item-action d-block py-1"
                                            >
                                                <summary className="row">
                                                    <div className="col py-2 order-1">
                                                        <div
                                                            onClick={() => parent.toggleMark(item.id)}
                                                            className="custom-control custom-checkbox"
                                                        >
                                                            <input
                                                                type="checkbox"
                                                                className="custom-control-input"
                                                                name={"check" + item.id}
                                                                checked={item.marked === 1}
                                                                onChange={() => parent.toggleMark(item.id)}
                                                            />
                                                            <label
                                                                className="custom-control-label text-nowrap"
                                                                htmlFor={"check" + item.id}
                                                            >
                                                                <a
                                                                    href={"mailto:" + item.fromAddress}
                                                                >
                                                                    <i className={(item.read) ? "icon icon-envelope-o fa fa-fw fa-envelope-o ml-1" : "icon icon-envelope fa fa-fw fa-envelope ml-1"}/>
                                                                    {item.from}{" "}
                                                                </a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div
                                                        className="col-auto px-0 order-last order-sm-2 d-none d-sm-block align-self-center text-right">
                                                        <a
                                                            className="text-secondary px-md-1"
                                                            title="Deleted"
                                                            onClick={() => parent.doDelete(item.id)}
                                                        >
                                                            <span className="icon icon-trash fa fa-fw fa-trash"/>
                                                        </a>
                                                    </div>
                                                    <div
                                                        className="col-sm-12 col-10 py-2 order-3"
                                                        onClick={() => parent.doShow(item.id)}
                                                    >
                                                        <div className="float-left text-left">
                                                        <span
                                                            className={
                                                                " d-none d-sm-block " +
                                                                (!item.read ? "font-weight-bold" : "")
                                                            }
                                                        >
                                                          {item.dtSent}
                                                        </span>
                                                        </div>
                                                        <p className="lead mb-0">
                                                            <a
                                                                title={
                                                                    !item.read
                                                                        ? "This is a new message"
                                                                        : "View this message"
                                                                }
                                                                onClick={() => parent.doShow(item.id)}
                                                            >
                                                                {item.subject}
                                                            </a>
                                                            {item.attachment ? (
                                                                <i className="align-middle fa fa-paperclip icon-paper-clip"/>
                                                            ) : null}
                                                            <button
                                                                type="button"
                                                                className="btn btn-outline-secondary btn-sm ml-2 d-none d-md-inline mr-2"
                                                                onClick={() => parent.doShow(item.id)}
                                                            >نمایش
                                                            </button>
                                                        </p>
                                                    </div>
                                                </summary>
                                            </li>
                                        ))
                                        :
                                        <li className="list-group-item list-group-item-action d-block py-1 mt-5">
                                            <span className='d-block text-center'>موردی یافت نشد!</span>
                                        </li>
                                    }
                                </ul>
                            </div>
                            <div id="sent" className="tab-pane">
                                <div className="d-flex flex-sm-row flex-column py-1 mb-1" style={{'direction': 'ltr'}}>
                                    <div className="btn-group">
                                        <button
                                            type="button"
                                            className="btn btn-outline-secondary text-uppercase"
                                            onClick={parent.toggleMarkAll}
                                        >
                                            <div
                                                className="custom-control custom-checkbox"
                                                onClick={parent.toggleMarkAll}
                                            >
                                                <input
                                                    type="checkbox"
                                                    className="custom-control-input"
                                                    id="checkAll"
                                                    defaultChecked={false}
                                                    onChange={parent.toggleMarkAll}
                                                />
                                                <label
                                                    className="custom-control-label"
                                                    htmlFor="checkAll"
                                                >انتخاب همه</label>
                                            </div>
                                        </button>
                                        {parent.state.messages &&
                                        parent.state.messages.filter((v, k) => {
                                            if (v.marked === 1) {
                                                return v;
                                            }
                                        }).length > 0 ? (
                                            <div className="btn-group mr-sm-auto mr-none">
                                                <button
                                                    type="button"
                                                    className="btn btn-outline-secondary dropdown-toggle text-uppercase"
                                                    data-toggle="dropdown"
                                                />
                                                <div className="dropdown-menu" id="dd1">
                                                    <a
                                                        className="dropdown-item"
                                                        onClick={parent.deleteMarked}
                                                    >حذف موارد علامت دار</a>
                                                </div>
                                            </div>
                                        ) : null}
                                    </div>
                                    {/*<button*/}
                                    {/*type="button"*/}
                                    {/*className="btn btn-outline-secondary mx-sm-1 mx-none"*/}
                                    {/*onClick={parent.refreshMessages}*/}
                                    {/*>*/}
                                    {/*<i className="align-middle icon-refresh fa fa-sync"/>*/}
                                    {/*</button>*/}
                                    {/*<button*/}
                                    {/*type="button"*/}
                                    {/*className="btn btn-outline-secondary ml-1 mr-none"*/}
                                    {/*data-target="#composeModal"*/}
                                    {/*data-toggle="modal"*/}
                                    {/*>*/}
                                    {/*<i className="align-middle icon-pencil fa fa-edit"/>*/}
                                    {/*</button>*/}
                                </div>
                                {/* sent items */}
                                <ul className="list-group py-2 mx-auto px-0">
                                    {parent.state.messages && parent.state.messages.length > 0
                                        ? parent.state.messages.map((item, idx) => (
                                            <li
                                                key={item.id}
                                                className="list-group-item list-group-item-action d-block py-1"
                                            >
                                                <summary className="row">
                                                    <div className="col py-2 order-1">
                                                        <div
                                                            onClick={() => parent.toggleMark(item.id)}
                                                            className="custom-control custom-checkbox"
                                                        >
                                                            <input
                                                                type="checkbox"
                                                                className="custom-control-input"
                                                                name={"check" + item.id}
                                                                checked={item.marked === 1}
                                                                onChange={() => parent.toggleMark(item.id)}
                                                            />
                                                            <label
                                                                className="custom-control-label text-nowrap"
                                                                htmlFor={"check" + item.id}
                                                            >
                                                                <a
                                                                    href={"mailto:" + item.fromAddress}
                                                                >
                                                                    {item.from}{" "}
                                                                </a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div
                                                        className="col-auto px-0 order-last order-sm-2 d-none d-sm-block align-self-center text-right">
                                                        <a
                                                            className="text-secondary px-md-1"
                                                            title="Deleted"
                                                            onClick={() => parent.doDelete(item.id)}
                                                        >
                                                            <span className="icon icon-trash fa fa-fw fa-trash"/>
                                                        </a>
                                                    </div>
                                                    <div
                                                        className="col-sm-12 col-10 py-2 order-3"
                                                        onClick={() => parent.doShow(item.id)}
                                                    >
                                                        <div className="float-left text-left">
                                <span
                                    className={
                                        " d-none d-sm-block " +
                                        (!item.read ? "font-weight-bold" : "")
                                    }
                                >
                                  {item.dtSent}
                                </span>
                                                        </div>
                                                        <p className="lead mb-0">
                                                            <a
                                                                title={
                                                                    !item.read
                                                                        ? "This is a new message"
                                                                        : "View this message"
                                                                }
                                                                onClick={() => parent.doShow(item.id)}
                                                            >
                                                                {item.subject}
                                                            </a>
                                                            {item.attachment ? (
                                                                <i className="align-middle fa fa-paperclip icon-paper-clip"/>
                                                            ) : null}
                                                            <button
                                                                type="button"
                                                                className="btn btn-outline-secondary btn-sm ml-2 d-none d-md-inline mr-2"
                                                                onClick={() => parent.doShow(item.id)}
                                                            >نمایش
                                                            </button>
                                                        </p>
                                                    </div>
                                                </summary>
                                            </li>
                                        ))
                                        :
                                        <li className="list-group-item list-group-item-action d-block py-1 mt-5">
                                            <span className='d-block text-center'>موردی یافت نشد!</span>
                                        </li>
                                    }
                                </ul>
                            </div>
                            <div id="deleted" className="tab-pane">
                                {/* deleted items */}
                                <ul className="list-group py-2 mx-auto px-0 mt-5">
                                    {parent.state.messages && parent.state.messages.length > 0
                                        ? parent.state.messages.map((item, idx) => (
                                            <li
                                                key={item.id}
                                                className="list-group-item list-group-item-action d-block py-1"
                                            >
                                                <summary className="row">
                                                    <div className="col py-2 order-1">
                                                        <label className="text-nowrap">
                                                            {item.from}{" "}
                                                        </label>
                                                    </div>
                                                    <div
                                                        className="col-sm-12 col-10 py-2 order-3"
                                                        onClick={() => parent.doShow(item.id)}
                                                    >
                                                        <div className="float-left text-left">
                                                            <span className={" d-none d-sm-block " + (!item.read ? "font-weight-bold" : "")}>
                                                              {item.dtSent}
                                                            </span>
                                                        </div>
                                                        <p className="lead mb-0">
                                                            <a
                                                                title={
                                                                    !item.read
                                                                        ? "This is a new message"
                                                                        : "View this message"
                                                                }
                                                                onClick={() => parent.doShow(item.id)}
                                                            >
                                                                {item.subject}
                                                            </a>
                                                            {item.attachment ? (
                                                                <i className="align-middle fa fa-paperclip icon-paper-clip"/>
                                                            ) : null}
                                                            <button
                                                                type="button"
                                                                className="btn btn-outline-secondary btn-sm ml-2 d-none d-md-inline mr-2"
                                                                onClick={() => parent.doShow(item.id)}
                                                            >
                                                                نمایش
                                                            </button>
                                                        </p>
                                                    </div>
                                                </summary>
                                            </li>
                                        ))
                                        :
                                        <li className="list-group-item list-group-item-action d-block py-1 mt-5">
                                            <span className='d-block text-center'>موردی یافت نشد!</span>
                                        </li>
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    );
};

export default InboxHtml;
