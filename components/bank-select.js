import React, {Component} from 'react';
import AsyncSelect from 'react-select/async';
import axios from 'axios';

const CancelToken = axios.CancelToken;
let cancel;

export default class BankSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: this.props.defaultValue,
            defaultOption: []
        };
        this.getOptions = this.getOptions.bind(this);
        var options = [];
        axios.get(process.env.NEXT_PUBLIC_API_URL +'/select2Banks').then((response) => {
            response.data.forEach(function (e) {
                options[e.id] = {'value': e.id, 'label': e.text}
            });

            this.setState({
                defaultOption: options
            })
        });
    }


    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
        this.props.func(selectedOption);
    };

    mapOptionsToValues(options) {
        return options.map(option => ({
            value: option.id,
            label: option.text
        }));
    };

    getOptions(inputValue, callback) {
        if (!inputValue) {
            return callback([]);
        }

        cancel && cancel();

        axios.get(process.env.NEXT_PUBLIC_API_URL +'/select2Banks', {
            params: {
                term: inputValue
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel = c;
            })
        }).then((response) => {
            const results = response.data;
            if (this.props.mapOptionsToValues)
                callback(this.props.mapOptionsToValues(results));
            else callback(this.mapOptionsToValues(results));
        });

    };


    render() {
        const customStyles = {
            input: (provided, state) => ({
                ...provided,
                border: 'none'
            }),
        };
        return (
            <AsyncSelect
                onChange={this.handleChange.bind(this)}
                placeholder={'انتخاب بانک*'}
                loadOptions={this.getOptions}
                value={this.state.selectedOption}
                defaultOptions={this.state.defaultOption}
                classNamePrefix={'react-select'}
                isSearchable={false}
                name='bank_id'
                id='bank_id'
            />
        )
    }
}
