import React, {Component, useEffect} from 'react';
import Link from 'next/link'
import {Dropdown} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faBars, faUser, faSignOutAlt} from '@fortawesome/free-solid-svg-icons'
import LoginRegister from '../login-register';
import UsersRequest from "../userCourseRequest";
import useAuth from "../../services/contexts/AuthContext";
import Cookies from "js-cookie";

export default function HeaderWithAuth(props) {
    return <Header {...props} authData={useAuth()}/>
}

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'loginDone': false,
            'loading': props.authData.loading,
            'user': props.authData.user,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.authData.isAuthenticated !== this.state.loginDone || nextProps.authData.loading !== this.state.loading) {
            this.setState({
                'loginDone': nextProps.authData.isAuthenticated,
                'user': nextProps.authData.user,
                'loading': nextProps.authData.loading,
            })
        }
    }

    showMobileMenu() {
        $('#mobileMenu').fadeIn();
    }

    hideMobileMenu() {
        $('#mobileMenu').fadeOut();
    }

    setLoginState(value) {
        if (value) {
            this.setState({
                loginDone: 'true'
            });
        }
    }

    setIdState(value) {
        $('#coursePage .registerBtn').attr('data-target', '#finalRegisterModal');
        if (value) {
            this.setState({
                id: value
            });
        }
    }

    logOut() {
        Cookies.remove('token')
        Cookies.remove('uid')
        window.location.reload();
    }

    render() {
        let menu;
        let mobileMenu;
        if ((this.state.loginDone && !this.state.loading) && this.state.user !== null) {
            let profileUrl = "/profile/" + this.state.user.id;
            mobileMenu =
                <>
                    <a href='/'>
                        <li>صفحه اصلی</li>
                    </a>
                    <a href='/blog'>
                        <li>بلاگ</li>
                    </a>
                    <a href='/login'>
                        <li>پنل آموزشگاه</li>
                    </a>
                    <a href='/landing/schools'>
                        <li>تعرفه ها</li>
                    </a>
                    <a href={profileUrl}>
                        <li>
                            پروفایل کاربری
                        </li>
                    </a>
                    <a onClick={this.logOut}>
                        <li>
                            خروج
                        </li>
                    </a>
                </>;
            menu =
                <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic">
                        <img src={'/assets/images/user.svg'}/>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item href={profileUrl}>
                            <FontAwesomeIcon icon={faUser}/> پروفایل کاربری
                        </Dropdown.Item>
                        <Dropdown.Item>
                            <span onClick={this.logOut}><FontAwesomeIcon icon={faSignOutAlt}/> خروج</span>
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
        } else {
            mobileMenu =
                <>
                    <a href='/'>
                        <li>صفحه اصلی</li>
                    </a>
                    <a href='/blog'>
                        <li>بلاگ</li>
                    </a>
                    <a href='/login'>
                        <li>پنل آموزشگاه</li>
                    </a>
                    <a href='/landing/schools'>
                        <li>تعرفه ها</li>
                    </a>
                    <a href="https://help.nikaro.ir" target='_blank'>
                        <li>راهنما</li>
                    </a>
                    <a>
                        <li>
                            <button className='btn bg-white login-btn' data-toggle="modal"
                                    data-target="#loginRegisterModal"
                                    style={{"borderRadius": '25px', 'padding': '5px 15px'}}>ورود | عضویت
                            </button>
                        </li>
                    </a>

                </>;
            menu =
                <button
                    className='btn btn-outline-primary login-btn'
                    style={{"borderRadius": '25px', ' padding': '5px 15px'}}
                    data-toggle="modal"
                    data-target="#loginRegisterModal"
                    id={'loginBtn'}
                >ورود | عضویت
                </button>
        }
        return (
            <>
                <header className={(!this.props.notHome) ? 'homeHeader' : null}>
                    {/*<div className='row bg-primary justify-content-center align-items-center p-2 text-justify'>*/}
                    {/*    <h5 className='h6 text-white text-justify'>با توجه به تغیرات فنی در سایت عملکرد بعضی از قسمت ها تا ساعت ۱۶ با اختلال مواجه میباشد.از صبر و شکیبایی شما سپاس گزاریم</h5>*/}
                    {/*</div>*/}
                    <nav className={(this.props.notHome) ? ' row top-nav bg-white not-home' : ' row top-nav bg-white'}>
                        <div className=' col-6 col-md-2 logo py-1 d-flex align-items-center '>
                            <a className='m-auto text-right' href={'/'}>
                                <img className='' id={'logo'} src={'/assets/images/sooryas-top-logo.jpg'}/>
                            </a>
                        </div>
                        <div
                            className='col-md-10 d-none d-md-flex justify-content-end align-items-center menu js-color-stop'>
                            <ul>
                                <li><a href={'https://online.sooryas.com/'}>صفحه اصلی</a></li>
                             
                                <li><a href={'/login'}>پنل آموزشگاه</a></li>
                              
                                <li>
                                    {menu}
                                </li>

                            </ul>
                        </div>
                        <div className='col-6 mobile_menu_btn' style={{'display': 'none'}}>
                                <span onClick={this.showMobileMenu}>
                                    <FontAwesomeIcon icon={faBars} size={"2x"} className='text-primary'/>
                                </span>
                        </div>
                        <div id='mobileMenu' onClick={this.hideMobileMenu} style={{'display': 'none'}}
                             className='text-center text-white'>
                            <img src={'/assets/images/crossOut.svg'}/>
                            <ul>
                                {mobileMenu}
                            </ul>
                        </div>
                    </nav>
                    <
                        input
                        type="hidden"
                        name="user_id"
                        value={(this.state.user !== null) ? this.state.user.id : ""}
                    />
                    <
                        input
                        type="hidden"
                        name="profileComplete"
                        value={(this.state.user !== null) ? this.state.user.profileComplete : 0}
                    />
                </header>
                <LoginRegister func={this.setLoginState.bind(this)} userId={this.setIdState.bind(this)}/>
                <UsersRequest/>
            </>
        );
    }
}


