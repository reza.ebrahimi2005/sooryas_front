import HeaderWithAuth from './header';
import Footer from './footer';
import React from "react";

const DefaultLayout = ({children}) => (
    <>
        <div className='container-fluid'>
            <HeaderWithAuth/>
            <div className="content-wrapper">{children}</div>
            <Footer/>
        </div>
    </>
);

export default DefaultLayout;
