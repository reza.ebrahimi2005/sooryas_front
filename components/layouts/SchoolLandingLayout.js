import React from "react";
import Head from "next/head";

const SchoolLandingLayout = ({children}) => (
    <>
        <div className='container-fluid schoolLandingLayout'>
            <div className="content-wrapper">{children}</div>
        </div>
    </>
);

export default SchoolLandingLayout;