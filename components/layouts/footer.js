import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faChevronUp} from '@fortawesome/free-solid-svg-icons'

const scrollToTop = () => {
    $('html, body').animate({scrollTop: 0}, 500);
};

export default class Footer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: null,
        };
    }

    handelEnamd() {
        window.open(
            "https://trustseal.enamad.ir/?id=98397&Code=sGJQR4oG7u6SLArkQ56O",
            "Popup",
            "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30"
        )
    }
    handelSamandehi() {
      
        window.open(
            "https://logo.samandehi.ir/Verify.aspx?id=1016305&p=rfthobpdrfthgvkaxlaoobpddshw",
            "Popup",
            "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30"
        )
    }
    render() {
        return (
                <footer className={(!this.props.notHome) ? 'row footer align-items-center homeFooter py-2' : 'row footer align-items-center py-2'}>
                    <img className={'scroll-vector'} src={'/assets/images/footer.svg'}/>
                    <div className='bg-nikaro scroll-btn text-white' onClick={scrollToTop}>
                        <FontAwesomeIcon icon={faChevronUp}/>
                    </div>
                    <div className='col-sm-4 col-12 footer-menu text-center order-1 mb-2 mb-sm-0'>
                        <ul>
                        
                            <a href="https://sooryas.com/about-us/#1377359144-2-22">
                                <li>درباره ما</li>
                            </a>
                            <a href="https://sooryas.com/%d8%aa%d9%85%d8%a7%d8%b3-%d8%a8%d8%a7-%d9%85%d8%a7/">
                                <li>تماس با ما</li>
                            </a>
                        </ul>
                    </div>
                    <div className='col-sm-4 col-12 copyright text-center order-sm-2 order-4'>
                        <span className='d-block'>تمامی حقوق این سایت متعلق به <strong
                        className='text-white'>سوریاس مهر </strong> می باشد.</span>
                     <span className='d-block'>copyright 2018 - 2020 © Design & Powered by   <strong
                       ><a  className='text-white' href="https://nikaro.ir">Nikaro</a></strong></span>
                        
                    </div>
                       <a href={"https://trustseal.enamad.ir/?id=98397&Code=sGJQR4oG7u6SLArkQ56O"}
                       className='col-sm-2 col-6 Enamad text-center order-sm-2 order-sm-3 order-2 mt-2 mt-md-0'>
                        <img src={'/assets/images/enamad.svg'}/>
                        <span className='text-small d-block'>نماد اعتماد الکترونیکی</span>
                    </a>
                    {/*<a onClick={this.handelSamandehi}
                      className='col-sm-2 col-6 Enamad text-center order-sm-2 order-sm-3 order-2 mt-2 mt-md-0'>
                    <div className='col-sm-2 col-6 text-center order-sm-4 order-3 mt-sm-2 mt-md-0 '>
                        <img className='bg-white rounded p-2' src={'https://logo.samandehi.ir/logo.aspx?id=1016305&amp;p=nbpdlymanbpdwlbqqftilymaujyn'} style={{'width': '85%'}}/>
                    </div>
        </a>*/}
                </footer>
        )
    }
}