import React, {Component} from 'react';
import AsyncSelect from 'react-select/async';
import axios from 'axios';

const CancelToken = axios.CancelToken;
let cancel;

export default class ChildrenSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.status){
            var options = [];
            var userId = $('input[name="user_id"]').val();
            axios.post(process.env.NEXT_PUBLIC_API_URL+'/users/getChildren', {'userId': userId}).then((response) => {
                response.data.forEach(function (e) {
                    options[e.id] = {'value': e.id, 'label': e.fullName}
                });
                this.setState({
                    selectedOption: null,
                    defaultOption: options,
                    status: nextProps.status,
                })
            });
        }
    }


    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
        this.props.func(selectedOption);
    };

    mapOptionsToValues(options) {
        return options.map(option => ({
            value: option.id,
            label: option.text
        }));
    };

    render() {
        return (
            <AsyncSelect
                onChange={this.handleChange.bind(this)}
                placeholder={'افراد ثبت شده'}
                // loadOptions={this.getOptions}
                value={this.state.selectedOption}
                defaultOptions={this.state.defaultOption}
                classNamePrefix={'react-select'}
                name={'member_id'}
                isSearchable={false}
                isDisabled={!this.state.status}
            />
        )
    }
}
