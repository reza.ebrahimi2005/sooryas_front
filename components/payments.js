import React, {Component, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Loading from "./loading";


const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    tableWrapper: {
        maxHeight: '80vh',
        overflow: 'auto',
    },
    tableCell: {
        fontFamily: 'IRANSans',
        fontSize: '13px'

    },
    button: {
        fontFamily: 'IRANSans',
        fontSize: '13px'
    },
    showIcon: {
        marginRight: 10
    }
});

function createData(billId, courseName, payDate, type, amount, action) {
    return {billId, courseName, payDate, type, amount, action};
}


export default function StickyHeadTable(props) {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);

    function handleInstallmentsPay(memberId, cost, totalAmount, courseId, step) {
        var data = {};

        data.transport_price = 0;
        data.discountCode = '';
        data.member_id = memberId;
        data.transport = 0;
        data.transport_code = 0;
        data.installment = step;
        data.step = step;
        data.course_id = courseId;
        data.costFee = totalAmount;
        data.payment = cost;

        setLoading(true);

        $.ajax({
            type: 'post',
            url: '/admin/api/web/bill',
            dataType: "json",
            data: data,
            success: function (response) {
                if (response['result-free']) {
                    bootbox.alert('ثبت نام شما با کد پیگیری ' + response['result-free'] + ' با موفقیت انجام شد', function () {
                        window.location.replace('/');
                    })
                } else {
                    $.ajax({
                        type: 'post',
                        url: '/pay',
                        dataType: "json",
                        data: response,
                        success: function (response) {
                            $('.loadingDarkPage').fadeIn();
                            var html = '<form id="paymentUTLfrm" action="https://mabna.shaparak.ir:8080/Pay" method="POST">' +
                                '<input type="hidden" id="TerminalID" name="TerminalID" value="' + response.TerminalID + '">' +
                                '<input type="hidden" id="Amount" name="Amount" value="' + response.Amount + '">' +
                                '<input type="hidden" id="callbackURL" name="callbackURL" value="' + response.callbackURL + '">' +
                                '<input type="hidden" id="InvoiceID" name="InvoiceID" value="123">' +
                                '<input type="hidden" id="Payload" name="Payload" value="' + response.Payload + '">' +
                                '</form>';
                            $('#installment_' + courseId + '_' + step).html(html);
                        },
                    });
                }
            },
            complete: function () {
                setTimeout(function () {
                    document.getElementById("paymentUTLfrm").submit();
                }, 2000);
            }
        });
    }

    var rows = [];
    props.data.map(item => (
        (item.installments.length > 0)
            ?
            item.installments.map(e => (
                rows.push(
                    createData('-', e.course_name, e.date, 'قسطی', e.cost, {
                        'step': e.step,
                        'courseId': e.course_id,
                        'totalAmount': e.total_amount,
                        'cost': e.cost,
                        'memberId': e.member_id
                    })
                )
            ))
            :
            rows.push(createData(item.bill_id, item.course_name, item.payDate, item.payType, item.amount, null))
    ));
    return (
        <Paper className={classes.root}>
            <div className={classes.tableWrapper}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.tableCell} align="center">شماره پرداخت</TableCell>
                            <TableCell className={classes.tableCell} align="center">عنوان</TableCell>
                            <TableCell className={classes.tableCell} align="center">تاریخ</TableCell>
                            <TableCell className={classes.tableCell} align="center">نوع پرداخت</TableCell>
                            <TableCell className={classes.tableCell} align="center">مبلغ</TableCell>
                            <TableCell className={classes.tableCell} align="center">عملیات</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell className={classes.tableCell} align='center'>{row.billId}</TableCell>
                                <TableCell className={classes.tableCell} align='center'>{row.courseName}</TableCell>
                                <TableCell className={classes.tableCell} align="center">{row.payDate}</TableCell>
                                <TableCell className={classes.tableCell} align="center">{row.type}</TableCell>
                                <TableCell className={classes.tableCell} align="center">{row.amount}</TableCell>
                                <TableCell className={classes.tableCell} align="center">
                                    {
                                        (row.action !== null)
                                            ?
                                            <>
                                                <Button
                                                    onClick={() => handleInstallmentsPay(row.action.memberId, row.action.cost, row.action.totalAmount, row.action.courseId, row.action.step)}
                                                    variant="contained"
                                                    color="primary"
                                                    className={classes.button}
                                                >
                                                    پرداخت قسط
                                                </Button>
                                                <div id={'installment_' + row.action.courseId + '_' + row.action.step}></div>
                                            </>
                                            :
                                            '-'
                                    }

                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {
                    (loading)
                        ?
                        <Loading/>
                        :
                        null
                }
            </div>
        </Paper>
    );
}
