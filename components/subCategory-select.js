import React, {Component} from 'react';
import AsyncSelect from 'react-select/async';
import axios from 'axios';

const CancelToken = axios.CancelToken;
let cancel;

export default class SubCategorySelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryId: 0,
            selectedOption: this.props.defaultValue,
            defaultOption: []
        };
        this.getOptions = this.getOptions.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.id !== this.state.categoryId) {
            var options = [];
            axios.get(process.env.NEXT_PUBLIC_API_URL + '/select2subCourseCategory', {
                params: {
                    'id': nextProps.id
                }
            }).then((response) => {
                response.data.forEach(function (e) {
                    options[e.id] = {'value': e.id, 'label': e.title}
                });
                var url_string = window.location.href;
                var url = new URL(url_string);
                var selectedOption = null;

                this.setState({
                    defaultOption: options,
                    categoryId: nextProps.id
                });
            });
        }
    }


    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
    };

    mapOptionsToValues(options) {
        return options.map(option => ({
            value: option.id,
            label: option.title
        }));
    };

    getOptions(inputValue, callback) {
        if (!inputValue) {
            return callback([]);
        }

        cancel && cancel();

        axios.get(process.env.NEXT_PUBLIC_API_URL + '/select2subCourseCategory', {
            params: {
                id: this.props.id,
                term: inputValue
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel = c;
            })
        }).then((response) => {
            const results = response.data;
            if (this.props.mapOptionsToValues)
                callback(this.props.mapOptionsToValues(results));
            else callback(this.mapOptionsToValues(results));
        });

    };


    render() {
        return (
            <AsyncSelect
                onChange={this.handleChange.bind(this)}
                placeholder={'انتخاب زیردسته بندی*'}
                loadOptions={this.getOptions}
                value={this.state.selectedOption}
                defaultOptions={this.state.defaultOption}
                classNamePrefix={'react-select'}
                name={'subCategory_id'}
                id={'subCategory_id'}
                isClearable={true}
            />
        )
    }
}
