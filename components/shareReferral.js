import React, {Component} from 'react';

if (document.getElementById('referrallData')) {
    var data = document.getElementById('referrallData').getAttribute('data');
}

export default class ShareReferral extends Component {
    constructor(props) {
        super(props);
        var temp = JSON.parse(data);
        this.state = {
            userName: temp.username,
            code: temp.data,
        };
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center align-items-center" id='shareReferral'>
                    <div className="col-sm-12 d-flex flex-column justify-content-center align-items-center">
                        <img className="img-fluid img-responsive wallet_img"
                             src={'/assets/images/add-user.png'} style={{'width': '20%'}}/>
                        <div className="text-right">
                            <h4>دوست شما، {this.state.userName} </h4>
                            <p>
                                کد تخفیف زیر را برای شما ارسال کرده تا با ثبت نام در نیکارو با این کد از اعتبار
                                رایگان برخورددار شوید!
                            </p>
                        </div>

                        <div className="border border-primary current_credit rounded p-4">
                            <h5>کد تخفیف شما : {this.state.code}</h5>
                            <h5 id="credit_amount" className="text-success text-underline"></h5>
                        </div>
                        {
                            ($('input[name="user_id"]').val() !== "")
                                ?
                                null
                                :
                                <button className="btn bg-nikaro text-white mt-4" data-toggle="modal"
                                        data-target="#loginRegisterModal">ثبت نام در نیکارو
                                </button>
                        }

                    </div>
                </div>
            </div>
        );
    }
}
