import React, {Component} from 'react';
import axios from 'axios';
import RegisterForOthersSwitch from './regiosterForOtehersSwitch';
import CourseInstallments from './course-installments';
import ChildrenSelect from "./chidren-select";
import Cookies from "js-cookie";

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default class FinalRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cityId: false,
            loading: false,
            registerForOthers: false,
            payFromWallet: false,
            installment: false,
            children: null,
            bootboxMsg: null,
            bootboxShow: false,
        };
    }

    installmentCheckbox() {
        if ($('#installmentCheck').is(':checked')) {
            this.setState({
                installment: true
            })
        } else {
            this.setState({
                installment: false
            })
        }
    }

    cityId(value) {
        this.setState({
            cityId: value
        })
    }

    registerForOthersValue(value) {
        this.setState({registerForOthers: value});
        if (value) {
            $('.othersDiv').fadeIn();
        } else {
            $('.othersDiv').fadeOut();
        }
    }

    submitDiscountCode() {
        if ($('#discountCodeForm input').val().length > 0) {
            this.refs.btn.setAttribute("disabled", "disabled");
            var self = this;
            const token = Cookies.get('token');
            axios({
                method: 'get',
                url: process.env.NEXT_PUBLIC_API_URL + '/discountCode?discountCode=' + $('#discountCodeForm input').val() + '&course_id=' + this.props.data.id,
                headers: {
                    "Authorization": `Bearer ${token}`
                },
            }).then(function (result) {
                toastr.options = {
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "timeOut": "4000",
                };
                if (result.data == null) {
                    toastr["error"]("کد تخفیف نامعتبر!");
                    self.refs.btn.removeAttribute("disabled");
                } else if (result.data == 'already_used') {
                    toastr["error"]('شما قبلا ازین کد تخفیف استفاده کرده اید!');
                    self.refs.btn.removeAttribute("disabled");
                } else if (result.data == 'number_of_use_compeleted') {
                    toastr["error"]('محدودیت استفاده ازین کدتخفیف تمام شده است.لطفا از کد تخفیف دیگری استفاده کنید!');
                    self.refs.btn.removeAttribute("disabled");
                } else if (result.data == "owner") {
                    toastr["error"]('کد تخفیف وارد شده برای خودتان قابل استفاده نمی باشد!');
                    self.refs.btn.removeAttribute("disabled");
                } else if (result.data == "school_id_not_match") {
                    toastr["error"]('کد تخفیف وارد شده مربوط به این آموزشگاه نمی باشد!');
                    self.refs.btn.removeAttribute("disabled");
                } else if (result.data == "course_id_not_match") {
                    toastr["error"]('کد تخفیف وارد شده مربوط به این دوره نمی باشد!');
                    self.refs.btn.removeAttribute("disabled");
                } else {
                    toastr["success"]('کد تخفیف ' + result.data.discount_name + ' اعمال شد');
                    $('#discountCodeForm input ,#discountCodeForm button').attr('disabled', 'disabled');
                    $('input[name="discount_code"]').val($('#discountCodeForm input').val());
                    var currentTotalPayment = (parseInt($('input[name="total_payment"]').val()) - result.data.discount_amount);
                    $('input[name="total_payment"]').val(0);
                    $('input[name="total_payment"]').val(currentTotalPayment);
                    $('#finalRegisterModal .discountText').text(result.data.discount_amount + ' تومان');
                    $('#finalRegisterModal .discountText').fadeIn();
                    $('#finalRegisterModal .payAmount').text(currentTotalPayment + ' تومان');
                }
            });

        } else {
            toastr.options = {
                "progressBar": true,
                "positionClass": "toast-top-left",
                "timeOut": "4000",
            };
            toastr["warning"]("کد تخفبف را وارد کنید.");
        }
    };

    submitRegisterForm() {
        var self = this;
        toastr.options = {
            "progressBar": true,
            "positionClass": "toast-top-left",
            "timeOut": "4000",
        };
        if ($('#termOfUseCheckBox').is(':checked') == false) {
            toastr["info"](' لطفا گزینه شرایط و مقررات نیکارو رو پذیرفته ام را علامت دار کنید');
            return;
        }
        var payment = $('input[name="total_payment"]').val();
        var data = {};

        var formData = new FormData($('#final-register')[0]);

        if ($('#installmentCheck').is(':checked')) {
            formData.append('installment', true);
            formData.append('step', "1");
        }

        if ($('#discountCodeForm button').is(':disabled') && $('#discountCodeForm input[name="discountCode"]').val().length !== 0) {
            data.discountCode = $('#discountCodeForm input[name="discountCode"]').val();
            formData.append('discountCode', $('#discountCodeForm input[name="discountCode"]').val());
        }
        if (!this.state.registerForOthers) {
            data.member_id = 'owner';
            formData.append('member_id', 'owner');
        } else if ($('input[name="member_id"]').val() !== "") {
            formData.append('member_id', $('input[name="member_id"]').val());
        } else {
            var loginForm = $('#othersInfoForm');
            var loginError = $('.alert-danger', loginForm);
            var loginSuccess = $('.alert-success', loginForm);
            $.validator.addMethod("regex", function (value, element, regexpr) {
                return regexpr.test(value);
            }, "");
            $('#othersInfoForm').validate({
                debug: true,
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    first_name: {
                        required: true,

                    },
                    last_name: {
                        required: true,
                    },
                },

                messages: { // custom messages for radio buttons and checkboxes
                    last_name: {
                        required: 'فیلد فامیل را وارد نمایید'
                    },
                    first_name: {
                        required: 'فیلد نام را وارد نمایید'
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    loginSuccess.hide();
                    loginError.show();
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
            });

            if ($('#othersInfoForm').valid()) {
                formData.append('member_id', 'other');
                formData.append('memberFirstName', $('#othersInfoForm input[name="first_name"]').val());
                formData.append('memberLastName', $('#othersInfoForm input[name="last_name"]').val());
            } else {
                return;
            }
        }

        data.course_id = $('input[name="course_id"]').val();
        data.payment = payment;
        formData.append('course_id', $('input[name="course_id"]').val());
        formData.append('payment', payment);

        if ($('#payFromWallet').is(':checked') == true) {
            data.pay_type = 'payFromWallet';
            formData.append('pay_type', 'payFromWallet');
            this.setState({
                payFromWallet: true
            })
        }

        if (payment > 0) {
            this.setState({
                loading: true
            });
        } else {
            this.setState({
                loading: true,
                payFromWallet: true
            });
        }

        const token = Cookies.get('token');
        $.ajax({
            type: 'post',
            url: process.env.NEXT_PUBLIC_API_URL + '/bill',
            dataType: "json",
            data: formData,
            headers: {
                "Authorization": `Bearer ${token}`
            },
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                if (response['result-free']) {
                    // ga('create', {
                    //     trackingId: 'UA-125303310-1',
                    //     cookieDomain: 'auto'
                    // });
                    // ga('send', 'event',
                    //     {
                    //         eventCategory: 'users',
                    //         eventAction: 'course_register',
                    //         eventLabel: 'course_register',
                    //         eventValue: '1'
                    //     });
                    self.setState({
                        loading: false,
                    });
                    alert('ثبت نام شما با کد پیگیری ' + response['result-free'] + ' با موفقیت انجام شد', function () {
                        window.location.replace('/');
                    })
                } else if (response === 'before_registered') {
                    self.setState({
                        loading: false,
                    });
                    alert('شما در این دوره قبلا ثبت نام شده  اید. نمی توانید دوباره ثبت نام نمایید.');
                } else if (response.bank === 'parsian') {
                    if( response.token != "" && response.status==true){

                        window.location.href='https://pec.shaparak.ir/NewIPG/?Token='+response.token;
                    }else{

                        alert(response.message);

                    }


                } else if (response.payFromWallet) {
                    if (response.status === "200") {
                        // ga('create', {
                        //     trackingId: 'UA-125303310-1',
                        //     cookieDomain: 'auto'
                        // });
                        // ga('send', 'event',
                        //     {
                        //         eventCategory: 'users',
                        //         eventAction: 'course_register',
                        //         eventLabel: 'course_register',
                        //         eventValue: '1'
                        //     });
                        self.setState({
                            loading: false,
                        });
                        alert(response.message, function () {
                            window.location.replace('/');
                        });
                    } else {
                        self.setState({
                            loading: false,
                        });
                        alert(response.message);
                    }
                } else {
                    self.setState({
                        loading: false,
                    });
                    alert(response.message);
                }
            }
        });
    }

    render() {
        return (
            <>
                <div className="modal fade" id="finalRegisterModal" tabIndex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style={{'padding': '0'}}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <h5 className="modal-title d-block text-black" id="exampleModalLongTitle">ثبت نام نهایی
                                    <hr/>
                                </h5>
                                <div className='courseInfo text-right'>
                                    <span className='d-block'><strong className='sectionTitlePrimary text-black'>خلاصه اطلاعات</strong></span>
                                    <span className='d-block text-black'>{this.props.data.title}</span>
                                    <span className='d-block'>{this.props.data.fullname}</span>
                                </div>
                                <div className='registerForOthers text-right'>
                                    <span className='text-black sectionTitlePrimary ml-2'>ثبت نام برای دیگران</span>
                                    <RegisterForOthersSwitch func={this.registerForOthersValue.bind(this)}/>
                                    <div className='othersDiv' style={{'display': 'none'}}>
                                        <ChildrenSelect status={this.state.registerForOthers}/>
                                        <form id='othersInfoForm'
                                              className={(this.state.registerForOthers) ? null : "d-none d-md-block"}>
                                            <span className='d-block mb-2'>در صورتی که فرد موردنظر در لیست بالا قرار ندارد فرم زیر را تکمیل کنید.</span>
                                            <div className='form-group'>
                                                <input className='form-control' type="text" name='first_name'
                                                       placeholder='نام'
                                                       disabled={(!this.state.registerForOthers) ? 'disabled' : null}/>
                                                <input className='form-control' type="text" name='last_name'
                                                       placeholder='نام خانوادگی'
                                                       disabled={(!this.state.registerForOthers) ? 'disabled' : null}/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className='costInfo text-right'>
                                    <span className='d-block'><strong className='sectionTitlePrimary text-black'>اطلاعات مالی</strong></span>
                                    <span
                                        className='d-block text-black sectionTitlePrimary'>شهریه دوره: {numberWithCommas(this.props.data.costWithDiscount)} تومان</span>
                                    <span className='d-block sectionTitlePrimary'
                                          style={{'display': 'none'}}>تخفیف : <span
                                        className='discountText'>{this.props.data.discount_percent}%</span> </span>
                                </div>
                                {
                                    (this.props.data.allowDiscount)
                                        ?
                                        <div className='discountCode'>
                                            <form id='discountCodeForm'>
                                                <div className='form-group'>
                                                    <input className='form-control' type="text" name='discountCode'
                                                           placeholder='کد تخفیف'/>
                                                    <button ref="btn" type='button' className='btn btn-outline-primary'
                                                            onClick={this.submitDiscountCode.bind(this)}>اعمال کد
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        :
                                        null
                                }
                                <div className='payment text-right'>
                                <span className='d-block text-black sectionTitlePrimary'>مبلغ قابل پرداخت:
                                    <span className='payAmount text-primary' id='total_amount'>
                                        {
                                            (this.state.installment)
                                                ?
                                                numberWithCommas(this.props.data.installments[0].cost)
                                                :
                                                numberWithCommas(this.props.data.costWithDiscount)
                                        }
                                        تومان
                                    </span>
                                </span>
                                    <span className='d-block small mt-2'><i
                                        className='fa fa-exclamation-triangle text-warning ml-1'/>نیکارو هیچ مسئولیتی در قبال تکمیل ظرفیت کلاس ها تا قبل از نهایی شدن ثبت نام شما ندارد.</span>
                                    {
                                        (this.props.data.installments.length > 1)
                                            ?

                                            <div className="form-check mt-2 mr-0">
                                                <input className="form-check-input mr-0" type="checkbox"
                                                       id="installmentCheck"
                                                       onChange={this.installmentCheckbox.bind(this)}/>
                                                <label className="form-check-label mr-4">
                                                    پرداخت اقساطی
                                                    <CourseInstallments data={this.props.data.installments}/>
                                                </label>
                                            </div>
                                            :
                                            null
                                    }
                                    {
                                        (this.props.data.payFromWallet === 1)
                                            ?
                                            <div className="form-check form-check-inline mt-2 mr-0">
                                                <input className="form-check-input mr-0" type="checkbox"
                                                       id="payFromWallet"/>
                                                <label className="form-check-label mr-2" htmlFor="payFromWallet">پرداخت
                                                    از کیف پول</label>
                                            </div>
                                            :
                                            null
                                    }

                                    <div className="form-check form-check-inline mt-2 mr-0">
                                        <input className="form-check-input mr-0" type="checkbox" id="termOfUseCheckBox"
                                               defaultChecked/>
                                        <label className="form-check-label mr-2" htmlFor="termOfUseCheckBox">
                                            <a href='#' className='text-primary'>شرایط استفاده </a>
                                            از نیکارو را مطالعه کرده و آنرا می پذیرم
                                        </label>
                                    </div>
                                    <div className='text-center'>
                                        {
                                            (this.state.loading && !this.state.payFromWallet)
                                                ?
                                                <button className='btn btn-outline-primary mt-4' disabled>
                                                    <img className='ml-2'
                                                         src={'/assets/images/loading.svg'}
                                                         style={{'width': '10%'}}/>
                                                    در حال انتقال به بانک...
                                                </button>
                                                :
                                                (this.state.loading)
                                                    ?
                                                    <button className='btn btn-outline-primary mt-4 text-center'
                                                            disabled>
                                                        <img src={'/assets/images/loading.svg'}
                                                             style={{'width': '20%'}}
                                                             className='ml-2'/>
                                                        لطفا صبر کنید...
                                                    </button>
                                                    :
                                                    <span className='btn btn-primary mt-4'
                                                          onClick={this.submitRegisterForm.bind(this)}>ثبت نام</span>
                                        }

                                    </div>
                                </div>
                                <input type="hidden" name="course_id" value={this.props.data.id}/>
                                <input type="hidden" name="discount_code" value=""/>
                                <input type="hidden" name="total_payment" value={this.props.data.costWithDiscount}/>
                                <div id='payment_info'/>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}