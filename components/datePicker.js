import React, {useState} from "react";
import 'react-modern-calendar-datepicker/lib/DatePicker.css';
import DatePicker from 'react-modern-calendar-datepicker';

export default function DatePickerComponent(props) {
    const [selectedDay, setSelectedDay] = useState(null);

    // render regular HTML input element
    const renderCustomInput = ({ref}) => (
        <input
            readOnly
            ref={ref} // necessary
            placeholder={props.placeholder}
            value={(selectedDay) ? selectedDay.year + "/" + selectedDay.month + "/" + selectedDay.day : ""}
            name={props.name}
        />
    );

    return (
        <DatePicker
            value={selectedDay}
            onChange={setSelectedDay}
            renderInput={renderCustomInput} // render a custom input
            shouldHighlightWeekends
            locale='fa'
            wrapperClassName="col-md-3 col-12 pl-2 form-group"
        />
    );
};

