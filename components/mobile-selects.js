import React, {Component} from 'react';
import ProvinceSelect from './province-select';
import CitySelect from './city-select';
import CategorySelect from './category-select';
import AgeSelect from './age-select';
import GenderSelect from './gender-select';
import ActiveCourses from './activeCourses-switch';
import HasDiscount from './has-discout-switch';

export default class MobileSelect extends Component {
    constructor(props) {
        super(props);
        this.state({
            provinceId: false,
            cityId: false,
            categoryFilter: false,
            ageFilter: false,
            genderFilter: false,
            active: false,
            discount: false,
        })
    }

    handleClick() {
        $('.selects-wrapper').toggle();
        $('.mobile-selects button i').toggleClass('fa-filter fa-times');
    }

    provinceValue(value) {
        if(value!=null) {
            this.setState({provinceId: value.value});
        }else{
            this.setState({provinceId: 0});
        }
    }

    cityValue(value) {
        if(value!=null) {
            this.setState({cityId: value.value});
        }else{
            this.setState({cityId: 0});
        }

    }

    categoryFilter(value) {
        this.setState({categoryFilter: value.value});
    }

    ageFilter(value) {
        this.setState({ageFilter: value.value});
    }

    genderFilter(value) {
        this.setState({genderFilter: value.value});
    }

    active(value) {
        this.setState({active: value});
    }

    discount(value) {
        this.setState({discount: value});
    }

    render() {
        return (
            <div className={'mobile-selects d-md-none d-xl-none d-lg-none'}>
                <button onClick={this.handleClick.bind(this)} className='btn bg-nikaro'>
                    <i className='fa fa-filter text-white'></i>
                </button>
                <div className='selects-wrapper' style={{'display': 'none'}}>
                    <div className='selects-container bg-white'>
                        <div className='row filters '>
                            <div className='col-12 header justify-content-center'>
                                <span>فیلتر جستجو</span>
                            </div>
                            <div className='col-12 selects'>
                                <div className='select text-right'>
                                    <ProvinceSelect func={this.provinceValue.bind(this)}/>
                                </div>
                                <div className='select text-right'>
                                    <CitySelect
                                        func={this.cityValue.bind(this)}
                                        provinceId={this.state.provinceId}
                                    />
                                </div>
                                <div className='select text-right'>
                                    <CategorySelect func={this.categoryFilter.bind(this)}/>
                                </div>
                                <div className='select text-right'>
                                    <AgeSelect func={this.ageFilter.bind(this)}/>
                                </div>
                                <div className='select text-right'>
                                    <GenderSelect func={this.genderFilter.bind(this)}/>
                                </div>
                                <div className='select d-flex justify-content-center align-items-center'>
                                    <ActiveCourses func={this.active.bind(this)}/>
                                </div>
                                <div className='select d-flex justify-content-center align-items-center'>
                                    <HasDiscount
                                        func1={this.discount.bind(this)}
                                        discount={this.state.discount}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}