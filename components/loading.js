import * as React from "react";
import {motion} from "framer-motion";

const icon = {
    hidden: {
        pathLength: 0,
        pathOffset: 0,
        fill: "rgba(255, 255, 255, 0)"
    },
    visible: {
        pathLength: 1,
        pathOffset: 1,
        fill: "url(#linear-gradient)",
        transition: {
            default: {duration: 1, ease: "easeInOut"},
            fill: {duration: 1, ease: [1, 0, 0.8, 1]}
        },
    }
};

const icon1 = {
    hidden: {
        opacity: .3,
        mixBlendMode: 'multiply',
        fill: "url(#linear-gradient-2)",
    },
    visible: {
        opacity: .3,
    }
};

const icon2 = {
    hidden: {
        opacity: 1,
        fill: "rgba(255, 255, 255, 1)"
    },
    visible: {
    }
};


const Loading = () => (
    <div id={'loading'}

    >
        <motion.div
            animate={{rotate: 360}}
            transition={{
                loop: Infinity,
                duration: 3
            }}
            style={{'overflow': 'visible'}}
        >
            <motion.svg xmlns="http://www.w3.org/2000/svg" xlinkHref="http://www.w3.org/1999/xlink"
                        viewBox="0 0 84.45 84.45"
                        width={200}
                        height={200}
                        initial={{opacity: 1}}
                        animate={{opacity: .5}}
                        transition={{
                            yoyo: Infinity,
                            ease: "easeIn",
                            duration: 2,
                        }}
            >
                <defs>
                    <linearGradient id="linear-gradient" x1="1.36" y1="2.35" x2="90.11" y2="88.95"
                                    gradientUnits="userSpaceOnUse">
                        <stop offset="0" stopColor="#3fffbc"/>
                        <stop offset="1" stopColor="#007bff"/>
                    </linearGradient>
                    <linearGradient id="linear-gradient-2" x1="24.51" y1="49.84" x2="75.19" y2="49.84"
                                    xlinkHref="#linear-gradient"/>
                </defs><g className="cls-1">
                    <g id="Layer_1" data-name="Layer 1">
                        <motion.path className="cls-2 item"
                                     d="M38 74.707l24.647 24.646L116.5 45.5"
                                     variants={icon}
                                     initial="hidden"
                                     animate="visible"
                        />
                        <motion.path className="cls-3"
                                     d="M38 74.707l24.647 24.646L116.5 45.5"
                                     variants={icon1}
                                     initial="hidden"
                                     animate="visible"
                        />
                        <motion.rect className="cls-4" x="27.71" y="27.7" width="29.03" height="29.03" rx="3.48"
                                     ry="3.48"
                                     transform="translate(42.22 -17.49) rotate(45)"
                                     variants={icon2}
                                     initial="hidden"
                                     animate="visible"
                        />
                    </g>
                </g>
            </motion.svg>
        </motion.div>
        <motion.span className='text-white h3 text-center mt-5 d-block'
                     initial={{opacity: 0}}
                     animate={{opacity: 1}}
                     transition={{
                         yoyo: Infinity,
                         ease: "easeIn",
                         duration: 2,
                         delay: 2
                     }}
                    style={{'textShadow':'0px 0px 5px #fff'}}

        >لطفا صبر کنید...
        </motion.span>
    </div>
);

export default Loading;
