import React from 'react';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
    popover: {
        pointerEvents: 'none',
    },
    paper: {
        padding: theme.spacing(1),
        maxWidth: 400,
        width: '100%',
        height: 200,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        textAlign:'center'
    },
    label: {
        marginRight: 5,
        fontFamily: "IRANSans",
        display: "inline",
        fontSize: 13,
    }
}));

export default function CourseInstallments(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handlePopoverOpen = event => {
        setAnchorEl(event.currentTarget);
    };

    const handlePopoverClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    var content = [];
    props.data.forEach(function (e) {
        content.push(
            <Box bgcolor='primary.main' p={2} m={1} borderRadius={16}>
                {
                    (e.order === 1)
                        ?
                        <span className='d-block'>پیش پرداخت</span>
                        :
                        <span className='d-block'>قسط  {e.order}</span>
                }
                <span className='d-block'>{e.pay_date}</span>
                <span className='d-block'>{e.cost} تومان</span>
            </Box>
        )
    });
    return (
        <span>
            <Typography
                aria-owns={open ? 'mouse-over-popover' : undefined}
                aria-haspopup="true"
                onMouseEnter={handlePopoverOpen}
                onMouseLeave={handlePopoverClose}
                className={classes.label}
            >
                (مشاهده اقساط)
            </Typography>
            <Popover
                id="mouse-over-popover"
                className={classes.popover}
                classes={{
                    paper: classes.paper,
                }}
                open={open}
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                onClose={handlePopoverClose}
                disableEnforceFocus
            >
                {content}
            </Popover>
        </span>
    );
}