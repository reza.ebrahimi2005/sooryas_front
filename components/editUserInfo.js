import React, {Component} from 'react';
import ProvinceSelect from './province-select';
import CitySelect from './city-select';
import Map from './map';
import RegionSelect from './region-select';
import DatePickerComponent from './datePicker';
import Cookies from "js-cookie";
import {ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class EditUserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            map: null,
            cityId: props.data.city_id,
            regionId: props.data.region_id,
            birthdayDate: null,
        };
    }

    componentDidMount() {
        this.setState({
            map: <Map lat={this.props.data.address_lat} lng={this.props.data.address_lng} regionId={this.state.regionId}
                      profile={true}/>
        })
    }

    handleCityChange(id) {
        this.setState({
            cityId: id.value,
            map: <Map id={id}/>
        })
    }

    handleRegionId(data) {
        this.setState({
            regionId: data.value,
            map: <Map lat={this.props.data.address_lat} lng={this.props.data.address_lng} regionId={data.value}
                      profile={true}/>
        })
    }

    submitUserInfoForm(event) {
        event.preventDefault();
        var self = this;
        var form = $('#userInfoForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        $.validator.addMethod("regex", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "");

        $('#userInfoForm').validate({
            ignore: [],
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                //account
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                city_id: {
                    required: true
                },


            },

            messages: {
                first_name: {
                    required: 'این فیلد الزامی است',
                },
                last_name: {
                    required: 'این فیلد الزامی است',
                },
                city_id: {
                    required: 'این فیلد الزامی است',
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        if ($('#userInfoForm').valid()) {
            var formdata = new FormData($('#userInfoForm')[0]);
            const token = Cookies.get('token');
            self.setState({
                loading: true
            });
            $.ajax({
                url: process.env.NEXT_PUBLIC_API_URL+'/saveEditedData',
                type: "POST",
                data: formdata,
                dataType: "json",
                processData: false,
                contentType: false,
                headers: {
                    "Authorization": `Bearer ${token}`
                },
                success: function (result) {
                    if (result.status === 'success') {

                        toast.success("اطلاعات با موفقیت بروزرسانی شد", {
                            position: toast.POSITION.TOP_CENTER
                        });
                        window.location.reload();
                    } else {
                        toast.error("مشکلی پیش آمد", {
                            position: toast.POSITION.TOP_CENTER
                        });
                    }
                },
            });
        }
    }

    handleDatePickerChange(value) {
        this.setState({
            birthdayDate: value,
        })
    }

    render() {
        return (
            <>
                <div className='mt-4' id='editUserInfo'>
                    <form className="row px-4" id='userInfoForm' onSubmit={this.submitUserInfoForm.bind(this)}>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input className="" type="text" name='first_name' placeholder={'نام'}
                                   defaultValue={this.props.data.first_name}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input type="text" name='last_name' placeholder={'نام خانوادگی'}
                                   defaultValue={this.props.data.last_name}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input type="text" name='father_name' placeholder={'نام پدر'}
                                   defaultValue={this.props.data.father_name}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input type="number" name='national_code' placeholder={'کدملی'}
                                   defaultValue={this.props.data.national_code}/>
                        </div>
                        <DatePickerComponent
                            placeholder={(this.props.data.birth_day !== null) ? this.props.data.birth_day : 'تاریخ تولد'}
                            name={'birth_day'}/>
                        <div className="col-md-3 col-12 pl-2 text-right form-group">
                            <ProvinceSelect id={this.props.data.province}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 text-right form-group">
                            <CitySelect id={this.props.data.city_id} func={this.handleCityChange.bind(this)}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 text-right form-group">
                            <RegionSelect cityId={this.state.cityId} selectedOption={this.props.data.address_region_id}
                                          func={this.handleRegionId.bind(this)}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input type="number" name='phone' placeholder={'تلفن ثابت'}
                                   defaultValue={this.props.data.phone}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input type="number" name='mobile' placeholder={'شماره همراه'}
                                   defaultValue={this.props.data.mobile}/>
                        </div>
                        <div className="col-md-3 col-12 pl-2 form-group">
                            <input type="email" name='email' placeholder={'ایمیل'}
                                   defaultValue={this.props.data.email}/>
                        </div>
                        <div className="col-12 pl-2 form-group">
                            <input type="text" name='address' placeholder={'آدرس'}
                                   defaultValue={this.props.data.address}/>
                        </div>
                        <div className="userLocation d-none d-md-block col-12 pl-2 form-group">
                            <span className="d-block text-black pt-4 pr-4 text-right">موقعیت مکانی</span>
                            <div className="col-12 mx-2 mt-2 map" id='map'>
                                {this.state.map}
                                <input type="hidden" name='location_lat' defaultValue={this.state.data.location_lat}/>
                                <input type="hidden" name='location_lng' defaultValue={this.state.data.location_lng}/>
                            </div>
                        </div>
                        <button type='submit' className="btn btn-primary col-2 mt-4 rounded-pill"
                                style={{'margin': '0 auto'}}>ثبت
                        </button>
                    </form>
                </div>
            </>
        );
    }
}
