import React, {Component} from 'react';
import ProgressBar from 'progressbar.js'

export default class ShowRatingCircle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rate: 0
        };


    }
    componentDidMount() {
        var rate = this.props.rate;
        var line = new ProgressBar.Circle('.progressCircle', {
            duration: 3000,
            easing: 'easeInOut',
            color: '#007bff',
            from: {color: '#007bff'},
            to: {color: '#00cbff'},
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) + ' %');
            }
        });
        line.animate(rate);
    }

    render() {
        return (
            <div className='progressCircle'></div>
        )
    }
}

