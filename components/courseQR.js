import React from 'react';
var QRCode = require('qrcode.react');

export default function CourseQR(){
    return(
        <div>
            <QRCode value="test" renderAs='svg'/>
        </div>
    )
}