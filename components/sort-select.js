import React, {Component} from 'react';
import dynamic from "next/dynamic";


const options = [
    {value: '1', label: 'جدیدترین'},
    {value: '2', label: 'محبوب ترین'},
    {value: '3', label: 'نزدیک ترین'},
];


const Select = dynamic(
    () => {
        return import('react-select');
    },
    {ssr: false}
);

export default class SortSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null
        };
    }

    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var selectedOption = null;

        if (url.searchParams.get('sort') != null && url.searchParams.get('sort') !== 0) {
            if (url.searchParams.get('sort') === 1)
                selectedOption = {value: '1', label: 'جدیدترین'};
            else if (url.searchParams.get('sort') === 2)
                selectedOption = {value: '2', label: 'محبوب ترین'};
            else
                selectedOption = {value: '3', label: 'نزدیک ترین'};
        }
        this.setState({
            selectedOption: selectedOption
        })

    }

    handleGetLocation(self) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                console.log({'lat': position.coords.latitude, 'lng': position.coords.longitude});
                self.props.LatLng({'lat': position.coords.latitude, 'lng': position.coords.longitude});
            });
        } else {
            alert("شما اجازه دسترسی به موقعیت خود را ندادید!");
            return;
        }
    }

    handleChange(selectedOption) {
        var self = this;
        if (selectedOption.value === "3") {
            this.handleGetLocation(self)
        }
        this.setState({selectedOption});
        this.props.func(selectedOption);

    };

    render() {
        const {selectedOption} = this.state;
        return (
            <Select
                value={selectedOption}
                onChange={this.handleChange.bind(this)}
                options={options}
                classNamePrefix={'sort-select'}
                placeholder={'مرتب سازی'}
                isSearchable={false}
            />
        );
    }
}
