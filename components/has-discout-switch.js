import React, {Component} from "react";
import Switch from "react-switch";

export default class HasDiscountSwitch extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            checked: false
        }
    }

    componentDidMount() {
        const url = window.location;
        const urlObject = new URL(url);
        const discount = urlObject.searchParams.get('discount');

        this.setState({
            checked: !!(discount)
        })

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            checked: nextProps.discount
        })
    }


    handleChange(checked) {
        this.setState({
            checked: checked
        });
        this.props.func1(checked);
    }

    render() {
        return (
            <div className={'react-switch-container'}>
                <Switch
                    onChange={this.handleChange}
                    checked={this.state.checked}
                    onColor="#86d3ff"
                    onHandleColor="#2693e6"
                    uncheckedIcon={false}
                    checkedIcon={false}
                    width={40}
                    height={20}
                    className="react-switch"
                    id="small-radius-switch"
                />
                <span>دوره های تخفیف دار</span>
            </div>
        );
    }
}