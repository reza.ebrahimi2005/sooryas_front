import React, {Component} from 'react';
import axios from 'axios';
import Cookies from "js-cookie";

export default class LoginRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            register: false,
            cityId: false,
            loading: false,
            forgetPassword: false,
            content: null,
            activation: false,
        };
        this.submitConfirmCodeForm = this.submitConfirmCodeForm.bind(this);
    }

    showRegister() {
        this.setState({
            register: true
        })
    };

    hideRegister() {
        this.setState({
            register: false
        })
    };

    showActivation() {
        this.setState({
            activation: true
        })
    }

    hideActivation() {
        this.setState({
            activation: false
        })
    }

    showForgetPassword() {
        this.setState({
            forgetPassword: true
        })
    }

    cityId(value) {
        this.setState({
            cityId: value
        })
    }

    submitRegisterForm(event) {
        event.preventDefault();
        var self = this;
        var form = $('#loginForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        $.validator.addMethod("regex", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "");

        $('#loginForm').validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                //account
                username: {
                    minlength: 11,
                    maxlength: 11,
                    number: true,
                    required: true
                },
                password: {
                    minlength: 5,
                    required: true
                },

            },

            messages: {
                username: {
                    required: 'شماره همراه را وارد کنید',
                    minlength: 'حداقل 11 کاراکتر وارد نمایید'
                },
                password: {
                    required: 'کلمه عبور را وارد نمایید',
                    minlength: 'حداقل 5 کاراکتر وارد نمایید'
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        if ($('#loginForm').valid()) {
            var formdata = new FormData($('#loginForm')[0]);
            self.setState({
                loading: true
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log( process.env.NEXT_PUBLIC_AUTH_API_URL+'/api/v3/login', process.env.NEXT_PUBLIC_AUTH_API_URL);
            $.ajax({
                url: process.env.NEXT_PUBLIC_AUTH_API_URL+'/api/v3/login',
                type: "POST",
                data: formdata,
                dataType: "json",
                processData: false,
                contentType: false,
                success: function (result) {
                    setTimeout(function () {
                        $('#loginRegisterModal').delay(2500).modal('hide');
                        $('#confirmCodeForm input[name="mobile"]').val($('#loginForm input[name="username"]').val());
                        $('#registrationConfirmModal').modal('show');
                        self.setState({loading: false})
                    }, 1000);
                },
                error: function (response) {
                    var response = jQuery.parseJSON(response.responseText);
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-left",
                        "timeOut": "4000",
                    };
                    toastr["error"](response.message);
                },
            });
        }

    }

    submitConfirmCodeForm() {
        var self = this;
        var confirmCodeForm = $('#confirmCodeForm');
        var confirmCodeFormError = $('.alert-danger', confirmCodeForm);
        var confirmCodeFormSuccess = $('.alert-success', confirmCodeForm);

        $.validator.addMethod("regex", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "");
        $.validator.addMethod("enNumber", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "");
        confirmCodeForm.validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                //account
                mobile: {
                    minlength: 11,
                    number: true,
                    required: true,
                    enNumber: /^[0-9]{11}$/
                },
                confirmCode: {
                    required: true,
                    minlength: 5,
                    number: true,
                },
            },

            messages: { // custom messages for radio buttons and checkboxes

                mobile: {
                    required: 'شماره همراه را وارد کنید',
                    minlength: 'حداقل 11 کاراکتر وارد نمایید',
                    enNumber: 'اعداد انگلیسی وارد نمایید'
                },

                confirmCode: {
                    required: 'کدفعال سازی را وارد نمایید ',
                    minlength: 'حداقل 5 کاراکتر وارد نمایید'
                },

            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                confirmCodeFormSuccess.hide();
                confirmCodeFormError.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });
        if ($('#confirmCodeForm').valid()) {
            var formdata = new FormData($('#confirmCodeForm')[0]);
            self.setState({loading: true});

            axios({
                method: 'post',
                url: process.env.NEXT_PUBLIC_AUTH_API_URL+'/api/v3/registerConfirm',
                data: formdata,
            })
                .then(function (response) {
                    $('form input').val("");
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-left",
                        "timeOut": "4000",
                    };
                    toastr["success"]("با موفقیت وارد شدید");
                    if (response.data.newUser) {
                        ga('create', {
                            trackingId: 'UA-125303310-1',
                            cookieDomain: 'auto'
                        });
                        ga('send', 'event',
                            {
                                eventCategory: 'user',
                                eventAction: 'signUp',
                                eventLabel: 'signUp',
                                eventValue: '1'
                            }
                        );
                    }
                    Cookies.set('token', response.data.access_token, {expires: 60})
                    Cookies.set('uid', response.data.user_id, {expires: 60})
                    $('#registrationConfirmModal').modal('hide');
                    window.location.reload();
                    self.props.func(true);
                    self.setState({loading: false});
                })
                .catch(function (error) {
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-left",
                        "timeOut": "4000",
                    };
                    toastr["error"]("کد صحیح نمی باشد");
                    self.setState({loading: false});
                });
        }
    }

    submitLoginForm(e) {
        e.preventDefault();
        var self = this;
        var loginForm = $('#loginForm');
        var loginError = $('.alert-danger', loginForm);
        var loginSuccess = $('.alert-success', loginForm);

        $.validator.addMethod("regex", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "");
        $('#loginForm').validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                //account
                username: {
                    required: true
                },
                password: {
                    required: true
                },
            },

            messages: { // custom messages for radio buttons and checkboxes
                username: {
                    required: 'نام کاربری را وارد نمایید'
                },
                password: {
                    required: 'کلمه عبور را وارد نمایید',
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                loginSuccess.hide();
                loginError.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
        });

        if ($('#loginForm').valid()) {
            var formdata = new FormData($('#loginForm')[0]);
            self.setState({loading: true});
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/login',
                type: "POST",
                data: formdata,
                dataType: "json",
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.status !== 'fail') {
                        setTimeout(function () {
                            toastr.options = {
                                "progressBar": true,
                                "positionClass": "toast-top-left",
                                "timeOut": "5000",
                            };
                            toastr["success"]("با موفقیت وارد شدید");
                            $('#loginRegisterModal').modal('hide');
                            self.props.func(true);
                            self.props.userId(result.id);
                            self.setState({loading: false});
                            window.location.reload();
                        }, 1000);
                    } else {
                        self.setState({loading: false});
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-center",
                            "timeOut": "5000",
                        };
                        toastr['error'](result.message);
                        self.props.func(false);
                    }
                }
            });
        }
    }

    submitChangePassForm(e) {
        e.preventDefault();
        var self = this;
        if ($('#changePassForm input[name="newPassword"]').val() != "" && $('#changePassForm input[name="confirmNewPassword"]').val() != "") {
            var newPass = $('#changePassForm input[name="newPassword"]').val();
            if (newPass == $('#changePassForm input[name="confirmNewPassword"]').val()) {
                $.ajax({
                    url: '/users/resetPasswordDone',
                    type: 'post',
                    data: {'newPass': newPass},
                    success: function (result) {
                        if (result == 1) {
                            toastr['success']('رمزعبور با موفقیت تغیر کرد');
                            $('#loginRegisterModal').modal('hide');
                            self.setState({
                                forgetPassword: false
                            });
                            return;
                        } else {
                            toastr['error']('مشکلی پیش آمد لطفا مجددا سعی نمایید');
                        }
                    }
                });
            } else {
                toastr['error']("رمزها یکسان نمی باشد")
            }
        } else {
            toastr['error']("لطفا همه فیلدها را کامل کنید")
        }
    }

    submitPinCodeForm(e) {
        e.preventDefault();
        if ($('#submitPinCodeForm input[name="pinCode"]').val() != "") {
            var self = this;
            var pinCode = $('#submitPinCodeForm input[name="pinCode"]').val();
            $.ajax({
                url: '/users/resetPassword',
                type: 'post',
                data: {'pin': pinCode},
                success: function (result) {
                    if (result['status'] == 200) {
                        var data =
                            <form id={'changePassForm'}>
                                <div className='form-group'>
                                    <input className='form-control' type="password" name='newPassword'
                                           placeholder='رمز جدید را وارد کنید'/>
                                    <input className='form-control' type="password" name='confirmNewPassword'
                                           placeholder='تکرار رمزجدید'/>
                                </div>
                                <button className='btn btn-outline-primary'
                                        onClick={self.submitChangePassForm.bind(self)}>اعمال
                                </button>
                                <a href="#" onClick={self.hideGetPinForm.bind(self)}>بازگشت</a>
                            </form>;
                        self.setState({
                            content: data
                        });
                    }
                },
                error: function () {
                    toastr['error']("کد امنیتی درست نمی باشد");
                }
            });

        } else {
            toastr['error']('لطفا کد را وارد کنید')
        }
    }

    submitGetPinCodeForm(value) {
        value.preventDefault();
        if ($('#getPinCodeForm input[name="username"]').val() != null || value != null) {
            var self = this;
            var mobile;
            var data;
            if (value.length > 0) {
                mobile = value;
            } else {
                mobile = $('#getPinCodeForm input[name="username"]').val();
            }
            toastr.options = {
                "progressBar": true,
                "positionClass": "toast-top-left",
                "timeOut": "4000",
            };
            $.ajax({
                url: '/users/sendPinCode',
                type: 'post',
                data: {'mobile_no': mobile},
                success: function (result) {
                    if (result == 'not_exist') {
                        toastr["error"]("هیج کاربری با این شماره وجود ندارد لطفا ثبت نام کنید");
                    } else if (result == 'no_data_received') {
                        toastr["error"]("مشکلی پیش آمد لطفا دوباره سعی کنید");
                    } else if (result['status'] == 200) {

                        var number = $('#getPinCodeForm input[name="username"]').val();
                        $('#getPinCodeForm input[name="username"]').val("");

                        toastr["success"]("کد امنیتی تا لحظاتی دیگر به شماره شما ارسال می شود");

                        data =
                            <form id={'submitPinCodeForm'}>
                                <div className='form-group'>
                                    <input className='form-control' type="number" name={'pinCode'}
                                           placeholder='کد را وارد کنید'/>
                                </div>
                                <button className='btn btn-outline-primary'
                                        onClick={self.submitPinCodeForm.bind(self)}>بازنشانی رمزعبور
                                </button>
                                <a href="#" onClick={self.hideGetPinForm.bind(self)}>بازگشت</a>
                                <div className="countdown"></div>
                            </form>;
                        self.setState({
                            content: data
                        });

                        var timer2 = "01:00";
                        var interval = setInterval(function () {
                            var timer = timer2.split(':');
                            //by parsing integer, I avoid all extra string processing
                            var minutes = parseInt(timer[0], 10);
                            var seconds = parseInt(timer[1], 10);
                            --seconds;
                            minutes = (seconds < 0) ? --minutes : minutes;
                            if (minutes < 0) clearInterval(interval);
                            seconds = (seconds < 0) ? 59 : seconds;
                            seconds = (seconds < 10) ? '0' + seconds : seconds;
                            $('.countdown').html(minutes + ':' + seconds);
                            timer2 = minutes + ':' + seconds;
                            if (minutes == 0 && seconds == 0) {
                                $('.countdown').html('<a id="resendPinCode" href="#">ارسال مجدد</a>');
                                $('#resendPinCode').on('click', function () {
                                    self.submitGetPinCodeForm(number);
                                });
                                clearInterval(interval);
                            }
                        }, 1000);
                    }
                }
            });
        } else {
            toastr['error']('لطفا شماره را وارد کنید')
        }
    }

    hideGetPinForm() {
        this.setState({
            forgetPassword: false
        })
    }

    submitGetActivationCodeForm(event) {
        event.preventDefault();

        var self = this;
        var form = $('#getActivationCodeForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        $('#getActivationCodeForm').validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                //account
                mobile: {
                    minlength: 11,
                    number: true,
                    required: true
                },
            },

            messages: {
                mobile: {
                    required: 'شماره همراه را وارد کنید',
                    minlength: 'حداقل 11 کاراکتر وارد نمایید'
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            }
        });

        if ($('#getActivationCodeForm').valid()) {
            self.setState({
                loading: true
            });
            $.ajax({
                url: '/users/resetConfirmCode',
                type: 'post',
                data: {'mobile': $('#getActivationCodeForm input[name="mobile"]').val()},
                success: function (result) {
                    if (result.status === 'success') {
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-center",
                            "timeOut": "4000",
                        };
                        toastr["success"](result.message);
                        $('form input').val("");
                        $('#loginRegisterModal').delay(2500).modal('hide');
                        $('#confirmCodeForm input[name="mobile"]').val(result.mobile);
                        $('#registrationConfirmModal').modal('show');
                        self.setState({
                            loading: false
                        });
                    } else if (result.status === 'failed') {
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-center",
                            "timeOut": "4000",
                        };
                        toastr["error"](result.message);
                        self.setState({
                            loading: false
                        });
                    }
                }
            });
        }


    }

    render() {
        var content;
        var title;
        if (this.state.register) {
            title = 'عضویت در نیکارو';
            content =
                <form id={'registerForm'}>
                    <div className='form-group'>
                        {/*<input className='form-control' type="text" placeholder='نام' name='first_name'/>*/}
                        {/*<input className='form-control' type="text" placeholder='نام خانوداگی' name='last_name'/>*/}
                        <input className='form-control' type="number" placeholder='شماره همراه' name='mobile'/>
                        <input className='form-control' type="password" placeholder='کلمه عبور' name='password'/>
                        {/*<CitySelect func={this.cityId.bind(this)}/>*/}
                    </div>
                    {
                        (this.state.loading)
                            ?
                            <button className='btn btn-outline-primary' disabled>
                                <img className='ml-2' src={'/assets/images/loading.svg'}
                                     style={{'width': '5%'}}/>
                            </button>
                            :
                            <button className='btn btn-outline-primary'
                                    onClick={this.submitRegisterForm.bind(this)}>عضویت</button>
                    }
                    <a href='#' className='switchToRegister' onClick={this.hideRegister.bind(this)}>ورود به حساب
                        کاربری</a>
                </form>
        } else if (this.state.activation) {
            title = 'فعالسازی حساب کاربری';
            content =
                <form id={'getActivationCodeForm'} onSubmit={this.submitGetActivationCodeForm.bind(this)}>
                    <div className='form-group'>
                        <input name='mobile' type="number" placeholder='شماره همراه'/>
                    </div>
                    {
                        (this.state.loading)
                            ?
                            <button className='btn btn-outline-primary' disabled>
                                <img className='ml-2' src={'/assets/images/loading.svg'}
                                     style={{'width': '5%'}}/>
                            </button>
                            :
                            <button type='submit' className='btn btn-outline-primary'>دریافت کد</button>
                    }
                    <a href="#" onClick={this.hideActivation.bind(this)}>بازگشت</a>
                </form>
        } else if (this.state.forgetPassword) {
            title = 'بازنشانی رمزعبور';
            (this.state.content == null)
                ?
                content =
                    <form id={'getPinCodeForm'}>
                        <div className='form-group'>
                            <input className='form-control' type="number" name={'username'} placeholder='شماره همراه'/>
                        </div>
                        {
                            (this.state.loading)
                                ?
                                <button className='btn btn-outline-primary' disabled>
                                    <img className='ml-2' src={'/assets/images/loading.svg'}
                                         style={{'width': '5%'}}/>
                                </button>
                                :
                                <button className='btn btn-outline-primary'
                                        onClick={this.submitGetPinCodeForm.bind(this)}>دریافت کد
                                </button>
                        }

                        <a href="#" onClick={this.hideGetPinForm.bind(this)}>بازگشت</a>
                    </form>
                :
                content = this.state.content;
        } else {
            title = 'ورود یا عضویت';
            content =
                <form id={'loginForm'} onSubmit={this.submitRegisterForm.bind(this)}>
                    <span className='d-block mb-2'>برای ورود یا عضویت شماره همراه خود را وارد کنید.</span>
                    <div className='form-group'>
                        <input className='form-control' type="tel" name={'username'} placeholder='شماره همراه'/>
                    </div>
                    {
                        (this.state.loading)
                            ?
                            <button className='btn btn-outline-primary' disabled>
                                <img className='ml-2' src={'/assets/images/loading.svg'}
                                     style={{'width': '5%'}}/>
                            </button>
                            :
                            <button className='btn btn-outline-primary' type='submit'>ارسال</button>
                    }
                </form>
        }
        return (
            <>
                <div className="modal fade" id="loginRegisterModal" role="dialog"
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style={{'padding': '0'}}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title text-center" id="exampleModalLongTitle">{title}
                                    <hr/>
                                </h5>
                                <button type="button" className="close float-left" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {content}
                            </div>
                        </div>
                    </div>
                </div>
                {/*<!--ConfirmCode Modal -->*/}
                <div className="modal fade" id="registrationConfirmModal" role="dialog" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="panel panel-login">
                                                <div className="row">
                                                    <div className="col-lg-12">
                                                        <button type="button" className="close float-left"
                                                                data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div className="col-lg-12">
                                                        <div className="alert alert-info text-right">
                                                            <span className='d-block'>کد فعالسازی برای شما ارسال شد. لطفا کد خود را در کادر زیر وارد کنید.</span>
                                                        </div>
                                                        <form id={'confirmCodeForm'}>
                                                            <div className='form-group'>
                                                                <input className='form-control' type="tel"
                                                                       name='mobile'
                                                                       placeholder='شماره همراه'/>
                                                                <input className='form-control' type="number"
                                                                       name='confirmCode' placeholder='کدفعالسازی'/>
                                                            </div>
                                                            {
                                                                (this.state.loading)
                                                                    ?
                                                                    <button className='btn btn-outline-primary'
                                                                            disabled>
                                                                        <img className='ml-2'
                                                                             src={'/assets/images/loading.svg'}
                                                                             style={{'width': '5%'}}/>
                                                                    </button>
                                                                    :
                                                                    <div className='btn btn-outline-primary'
                                                                         onClick={this.submitConfirmCodeForm}>ارسال
                                                                    </div>
                                                            }

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<!---  END ConfirmCode Modal    -->*/}
                {/*<!--resetCode Modal -->*/}
                <div className="modal fade" id="resetConfirmcodeModal" role="dialog"
                     aria-labelledby="resetConfirmcodeModal"
                     aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="panel panel-login">
                                                <div className="panel-heading">
                                            <span data-toggle="modal" data-target="#resetConfirmcodeModal">
                                                <i className="fa fa-times"/>
                                            </span>
                                                </div>
                                                <div className="panel-body">
                                                    <div className="row">
                                                        <div className="col-lg-12">
                                                            <div className="alert alert-info text-right">
                                                                <button type="button" className="close"
                                                                        data-dismiss="alert"
                                                                        aria-hidden="true"/>
                                                                <strong>توجه</strong> شمارهای که با آن ثبت نام کرده اید
                                                                وارد
                                                                نمایید
                                                            </div>
                                                            <form className="login-form" data-form-validate="true"
                                                                  id="resetCode-form" method="POST" action="#"
                                                            >
                                                                <div className="form-group">
                                                                    <input type="number" name="mobile" id="mobile"
                                                                           className="form-control"
                                                                           placeholder="شماره موبایل "/>
                                                                </div>

                                                                <div className="form-group">
                                                                    <div className="row">
                                                                        <div className="col-12 align-self-center">
                                                                            {
                                                                                (this.state.loading)
                                                                                    ?
                                                                                    <button
                                                                                        className='btn btn-outline-primary'
                                                                                        disabled>
                                                                                        <img className='ml-2'
                                                                                             src={'/assets/images/loading.svg'}
                                                                                             style={{'width': '5%'}}/>
                                                                                    </button>
                                                                                    :
                                                                                    <input type="submit" name=""
                                                                                           id=""
                                                                                           className="form-control btn btn-login"
                                                                                           value="ارسال"/>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<!---  END resetCode Modal    -->*/}
            </>
        );
    }
}
