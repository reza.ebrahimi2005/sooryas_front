import React, {Component} from "react";
import request from "superagent";
import debounce from "lodash.debounce";
import Loading from '../components/loading';
import Rating from "react-rating";
import Divider from '@material-ui/core/Divider';
import RoomIcon from '@material-ui/icons/Room';
import ClassIcon from '@material-ui/icons/Class';
import DateRangeIcon from '@material-ui/icons/DateRange';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import Button from '@material-ui/core/Button';

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default class InfiniteItems extends Component {
    constructor(props) {
        super(props);
        // Sets up our initial state
        this.state = {
            page: 1,
            error: false,
            hasMore: true,
            isLoading: false,
            total: false,
            sort: false,
            courseSchoolSelect: false,
            scroll: false,
            regionId: false,
            cityId: false,
            categoryFilter: false,
            ageFilter: false,
            genderFilter: false,
            active: false,
            discount: false,
            items: [],
            LatLng: null,
            q: null
        };
    }

    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        this.setState({
            q: url.searchParams.get('q')
        })

        // Binds our scroll event handler
        window.onscroll = debounce(() => {
            const {
                loadItems,
                state: {
                    error,
                    isLoading,
                    hasMore,
                },
            } = this;

            // Bails early if:
            // * there's an error
            // * it's already loading
            // * there's nothing left to load
            if (error || isLoading || !hasMore || this.state.items.length >= this.state.total) return;

            // Checks that the page has scrolled to the bottom

            if (window.innerHeight + document.documentElement.scrollTop > (document.documentElement.offsetHeight) - 400) {
                this.setState({
                    page: (this.state.page + 1),
                    scroll: true
                });
                this.loadItems();
            }
        }, 10);

        // Loads some items on initial load
        this.loadItems();

    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.sort !== 'undefined' && nextProps.sort !== this.state.sort) {
            this.setState({
                sort: nextProps.sort
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('sort') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&sort=' + nextProps.sort);
            } else {
                url.searchParams.set('sort', nextProps.sort);
                window.history.replaceState("Details", "Title", url);
            }

        } else if (typeof nextProps.sort !== 'undefined' && nextProps.courseSchoolSelect !== this.state.courseSchoolSelect) {
            this.setState({
                courseSchoolSelect: nextProps.courseSchoolSelect
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('courseSchool') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&courseSchool=' + nextProps.courseSchoolSelect);
            } else {
                url.searchParams.set('courseSchool', nextProps.courseSchoolSelect);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (typeof nextProps.regionId !== 'undefined' && nextProps.regionId !== this.state.regionId) {
            this.setState({
                regionId: nextProps.regionId
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('regionId') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&regionId=' + nextProps.regionId);
            } else {
                url.searchParams.set('regionId', nextProps.regionId);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (typeof nextProps.cityId !== 'undefined' && nextProps.cityId !== this.state.cityId) {
            this.setState({
                cityId: nextProps.cityId
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('cityId') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&cityId=' + nextProps.cityId);
            } else {
                url.searchParams.set('cityId', nextProps.cityId);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (typeof nextProps.regionId !== 'undefined' && nextProps.regionId !== this.state.regionId) {
            this.setState({
                regionId: nextProps.regionId
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('regionId') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&regionId=' + nextProps.regionId);
            } else {
                url.searchParams.set('regionId', nextProps.regionId);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (typeof nextProps.categoryFilter !== 'undefined' && nextProps.categoryFilter !== this.state.categoryFilter) {
            this.setState({
                categoryFilter: nextProps.categoryFilter
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('category') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&category=' + nextProps.categoryFilter);
            } else {
                url.searchParams.set('category', nextProps.categoryFilter);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (typeof nextProps.ageFilter !== 'undefined' && nextProps.ageFilter !== this.state.ageFilter) {
            this.setState({
                ageFilter: nextProps.ageFilter
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('age') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&age=' + nextProps.ageFilter);
            } else {
                url.searchParams.set('age', nextProps.ageFilter);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (typeof nextProps.genderFilter !== 'undefined' && nextProps.genderFilter !== this.state.genderFilter) {

            this.setState({
                genderFilter: nextProps.genderFilter
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('gender') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&gender=' + nextProps.genderFilter);
            } else {
                url.searchParams.set('gender', nextProps.genderFilter);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (nextProps.active !== this.state.active) {
            this.setState({
                active: nextProps.active
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('active') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&active=' + nextProps.active);
            } else {
                url.searchParams.set('active', nextProps.active);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (nextProps.discount !== this.state.discount) {

            this.setState({
                discount: nextProps.discount
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('discount') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&discount=' + nextProps.discount);
            } else {
                url.searchParams.set('discount', nextProps.discount);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (nextProps.q !== this.state.q) {
            this.setState({
                q: nextProps.q,
                page: 1,
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('q') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&q=' + nextProps.q);
            } else {
                url.searchParams.set('q', nextProps.q);
                window.history.replaceState("Details", "Title", url);
            }
        } else if (nextProps.LatLng !== this.state.LatLng) {
            this.setState({
                LatLng: nextProps.LatLng
            });
            var url_string = window.location.href;
            var url = new URL(url_string);

            if (url.searchParams.get('LatLng') == null) {
                window.history.replaceState("Details", "Title", window.location.href + '&lat=' + nextProps.LatLng.lat + '&lng=' + nextProps.LatLng.lng);
            } else {
                url.searchParams.set('lat', nextProps.LatLng.lat);
                window.history.replaceState("Details", "Title", url);
                url.searchParams.set('lng', nextProps.LatLng.lng);
                window.history.replaceState("Details", "Title", url);
            }

        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if
        (
            prevProps.sort !== this.state.sort
            ||
            prevProps.courseSchoolSelect !== this.state.courseSchoolSelect
            ||
            prevProps.cityId !== this.state.cityId
            ||
            prevProps.regionId !== this.state.regionId
            ||
            prevProps.categoryFilter !== this.state.categoryFilter
            ||
            prevProps.ageFilter !== this.state.ageFilter
            ||
            prevProps.genderFilter !== this.state.genderFilter
            ||
            prevProps.active !== this.state.active
            ||
            prevProps.discount !== this.state.discount
            ||
            prevProps.q !== this.state.q
            ||
            prevProps.LatLng !== this.state.LatLng
        ) {
            this.loadItems();
        }
    }

    loadItems() {
        var reqUrl;
        var url_string = window.location.href;
        var temp = url_string.split('?');
        if (temp[1]) {
            reqUrl = process.env.NEXT_PUBLIC_API_URL+'/search?page=' + this.state.page + '&' + temp[1];
        } else {
            reqUrl = process.env.NEXT_PUBLIC_API_URL+'/search?page=' + this.state.page + '&';
        }
        this.setState({isLoading: true}, () => {
            request.get(reqUrl)
                .then((results) => {
                    // Creates a massaged array of user data
                    const nextitems = results.body[0].map(e => ({
                        type: e.type,
                        title: e.title,
                        id: e.id,
                        coverPic: (e.type === 'course') ? (e.coursePic.original.length > 0) ? e.coursePic.original[0].image : null : e.schoolPic,
                        defaultPic: e.defaultPic,
                        // userPic: (e.type ===  'course') ? (typeof e.userPic.original[0].image != "undefined") ? e.userPic.original[0].image : null : null,
                        discount_percent: (e.type === 'course') ? e.discount_percent : null,
                        is_virtual: (e.type === 'course') ? e.is_virtual : null,
                        fullname: (e.type === 'course') ? e.fullname : null,
                        course_formation_end_time: (e.type === 'course') ? e.course_formation_end_time : null,
                        course_formation_start_time: (e.type === 'course') ? e.course_formation_start_time : null,
                        days: (e.type === 'course') ? e.formationDays[1].day : null,
                        course_start_date: (e.type === 'course') ? e.course_start_date : null,
                        manager: (e.type === 'school') ? e.manager : null,
                        region: (e.type === 'school') ? e.region : null,
                        category: (e.type === 'school') ? e.category : null,
                        total_amount: (e.type === 'course') ? e.total_amount : null,
                        formationDaysWeek: (e.type === 'course') ? e.formationDaysWeek : null,
                        formationDays: (e.type === 'course') ? e.formationDays : null,
                        rate: (e.type === 'course') ? e.avg_rate : e.rate
                    }));
                    if (this.state.scroll) {
                        // Merges the next items into our existing items
                        this.setState({
                            // Note: Depending on the API you're using, this value may
                            // be returned as part of the payload to indicate that there
                            // is no additional data to be loaded
                            items: [
                                ...this.state.items,
                                ...nextitems,
                            ],

                            hasMore: ((this.state.items.length < results.body.totalCount)),

                            total: results.body.totalCount,

                            isLoading: false,

                            scroll: false
                        });
                    } else {
                        // Merges the next items into our existing items
                        this.setState({
                            // Note: Depending on the API you're using, this value may
                            // be returned as part of the payload to indicate that there
                            // is no additional data to be loaded
                            items: [
                                ...nextitems
                            ],

                            hasMore: (nextitems.length < results.body.totalCount),

                            total: results.body.totalCount,

                            isLoading: false,
                        });
                    }
                })
                .catch((err) => {
                    this.setState({
                        error: err.message,
                        isLoading: false,
                    });
                })
        });
    }

    render() {
        const {
            error,
            hasMore,
            isLoading,
            items,
        } = this.state;
        // $('.results .header .numFound').text(this.state.total + ' مورد پیدا شد');
        return (
            <div className='row results-items-wrapper justify-content-right'>
                {
                    (this.state.total === 0 || this.state.total === false)
                        ?
                        <div className="noResult col-12 text-center mt-5">
                            <img src={'/assets/images/noResult.png'}/>
                            <span className="d-block mt-2">متاسفانه نتیجه ای یافت نشد</span>
                        </div>
                        :
                        items.map((e, index) => (
                            (e.type === 'course')
                                ?
                                <div className='col-lg-4 col-md-6 col-12 my-2' key={index}>
                                    <div className='items'>
                                        <div className='item-header'>
                                            <img className='item-header-img-2'
                                                 src={e.coverPic}/>
                                            <img className='item-header-img'
                                                 src={e.coverPic}/>
                                            <img className='rounded teacher-pic'
                                                 src={e.userPic}/>
                                            {
                                                (e.discount_percent !== 0)
                                                    ?
                                                    <div className='discount'>
                                                        <span
                                                            className='text-white'>%{Math.round(e.discount_percent)}</span>
                                                    </div>
                                                    :
                                                    null
                                            }
                                            {
                                                (e.is_virtual)
                                                    ?
                                                    <div className='virtualBadge bg-success'>
                                                        <span className='text-white'>مجازی</span>
                                                    </div>
                                                    :
                                                    null
                                            }
                                        </div>
                                        <div className='info text-center d-flex flex-column justify-content-between'>
                                            <span className='d-flex justify-content-between align-items-center'>
                                                <span className=''>
                                                    {e.course_start_date.split(" ")[0]}
                                                    <i className='fa fa-calendar'/>
                                                </span>
                                                <Rating
                                                    className='rating float-left'
                                                    emptySymbol="fa fa-star-o"
                                                    fullSymbol="fa fa-star"
                                                    placeholderSymbol="fa fa-star"
                                                    fractions={2}
                                                    readonly={true}
                                                    placeholderRating={(e.rate != null) ? e.rate : 0}
                                                />
                                            </span>
                                            <span className='d-block'>
                                                <h3 className='course-title d-block'>{e.title}</h3>
                                                <span className='teacher-name d-block'>{e.fullname}</span>
                                            </span>
                                            <span
                                                className='d-flex justify-content-between align-items-center my-md-2 my-1'>
                                                <span className='course-days text-primary ml-2 text-nowrap'>
                                                    <DateRangeIcon className='ml-1'/>
                                                    روزهای برگزاری:
                                                </span>
                                                {
                                                    (e.formationDaysWeek.id === 1)
                                                        ?
                                                        <span className='d-block text-nowrap'>{e.formationDays}</span>
                                                        :
                                                        <span
                                                            className='d-block text-nowrap'>{e.formationDaysWeek.title}</span>
                                                }
                                            </span>
                                            <Divider className='bg-nikaro-light'/>
                                            <span
                                                className='d-flex justify-content-between align-items-center my-1 my-md-2'>
                                                <span className='text-primary ml-2 text-nowrap'>
                                                    <QueryBuilderIcon className='ml-1'/>
                                                     ساعت جلسات:
                                                </span>
                                                <span className={'course-time d-block'}>
                                                    {e.course_formation_start_time} الی {e.course_formation_end_time}
                                                </span>
                                            </span>
                                            <span
                                                className={'course-fee d-block'}>{(e.total_amount === "free" || e.discount_percent === 100) ? "رایگان " : numberWithCommas((e.total_amount - ((e.total_amount * e.discount_percent) / 100)) + " تومان")}  </span>
                                            {
                                                (e.discount_percent !== 0)
                                                    ?
                                                    <span
                                                        className={'total-amount d-block'}>{numberWithCommas(e.total_amount)}
                                                        تومان</span>
                                                    :
                                                    null
                                            }

                                            <Button target="_blank" variant="outlined" href={'/course/' + e.id}
                                                    color={"primary"}
                                                    style={{
                                                        'fontFamily': 'IRANSans',
                                                        'borderColor': '#007aff',
                                                        'color': '#007aff'
                                                    }}>
                                                مشاهده دوره
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                :
                                <div className='col-lg-4 col-md-6 col-12 my-2' key={index}>
                                    <div className='items'>
                                        <div className='item-header'>
                                            {
                                                (e.coverPic == null)
                                                    ?
                                                    <>
                                                        <img className='item-header-img-2'
                                                             src='../public/assets/images/placeholder.jpg'
                                                             style={{'objectPosition': 'center'}}/>
                                                        <img className='item-header-img'
                                                             src='../public/assets/images/images/placeholder.jpg'
                                                             style={{'objectPosition': 'center'}}/>
                                                    </>
                                                    :
                                                    <>
                                                        <img className='item-header-img-2' src={e.coverPic}/>
                                                        <img className='item-header-img' src={e.coverPic}/>
                                                    </>
                                            }
                                        </div>
                                        <div className='info text-center d-flex flex-column justify-content-between'>
                                            <h3 className='course-title d-block text-right text-nowrap'>{e.title}</h3>
                                            <div className="d-flex flex-row justify-content-between">
                                                <span className='teacher-name text-right'>{e.manager}</span>
                                                <Rating
                                                    className='rating text-nowrap'
                                                    emptySymbol="fa fa-star-o"
                                                    fullSymbol="fa fa-star"
                                                    placeholderSymbol="fa fa-star"
                                                    fractions={2}
                                                    readonly={true}
                                                    placeholderRating={(e.rate != null) ? e.rate : 0}
                                                />
                                            </div>

                                            <span
                                                className='d-flex justify-content-between align-items-center my-md-2 my-1'>
                                                    <span className='text-primary ml-2 text-nowrap'>
                                                    <RoomIcon className='ml-1'/>ناحیه شهری:</span>
                                                    <span className='text-nowrap'>{e.region}</span>
                                            </span>
                                            <Divider className='bg-nikaro-light'/>
                                            <span
                                                className='d-flex justify-content-between align-items-center my-md-2 my-1'>
                                                <span className='text-primary ml-2 text-nowrap'>
                                                    <ClassIcon className='ml-1'/>دسته بندی:</span>
                                                <span>{e.category}</span>
                                            </span>

                                            <Button variant="outlined" href={'/school/' + e.id}
                                                    color={"primary"} style={{
                                                'fontFamily': 'IRANSans',
                                                'borderColor': '#007aff',
                                                'color': '#007aff'
                                            }}
                                                    target="_blank"
                                            >
                                                اطلاعات بیشتر
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                        ))
                }
                {isLoading &&
                <Loading/>
                }
            </div>
        );
    }
}
