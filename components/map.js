import React, {Component} from 'react';

export default class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lat: 36.29820678050358,
            lng: 59.60557148208686,
            marker: null,
            map: null,
            poly: null,
        };
        if (props.profile) {
            var map = new L.Map('map', {
                key: 'web.HYeCa52JbXNCnJsXWDbITIPJd4AJLf4sdi5no76A',
                center: (props.lat !== null) ? [props.lat, props.lng] : [36.29820678050358, 59.60557148208686],
                zoom: 14
            });

            map.setMapType('neshan');

            var marker = {};
            var myIcon = L.icon({
                iconUrl: '/assets/js/pages/school/images/pin24.png',
                iconRetinaUrl: '/assets/js/pages/school/images/pin48.png',
                iconSize: [32, 26],
                iconAnchor: [9, 21],
                popupAnchor: [0, -14]
            });
            if (typeof props.lat !== "undefined" && props.lat !== null)
                marker = L.marker([props.lat, props.lng], {icon: myIcon}).addTo(map);
            this.state = {
                map: map,
                lat: props.lat,
                lng: props.lng,
                marker: marker,
                poly: null,
            };
        } else {
            this.state = {
                lat: 36.29820678050358,
                lng: 59.60557148208686,
                marker: null,
                map: null,
                poly: null,
            };
        }
    }

    drawPolygon(id) {
        var data = {};
        data.id = id;
        var self = this;
        $.ajax({
            type: 'post',
            url: '/admin/api/web/courses/getPolygons',
            dataType: "json",
            data: data,
            success: function (result) {
                var polystring = result.split(',');
                var marker = {};
                var polyDraw = {};
                var firstMarker;
                var a = polystring.length;
                for (var i = 0; i < a; i++) {
                    polystring[i] = polystring[i].replace("[", "");
                    polystring[i] = polystring[i].replace("]", "");
                    polystring[i] = parseFloat(polystring[i]);
                }
                var b = polystring.length / 2;
                var array = [];
                for (i = 0; i < b; i++) {
                    array[i] = [0, 0];
                }
                var k = 0;
                for (i = 0; i < b; i++) {
                    for (var j = 0; j < 2; j++) {
                        array[i][j] = polystring[k];
                        k++;
                    }
                }
                self.state.map.panTo(new L.LatLng(polystring[0], polystring[1]));
                //Handle click on polygon
                // var onPolyClick = function (event) {
                //     $('input[name="location_lat"]').val(event.latlng.lng);
                //     $('input[name="location_lng"]').val(event.latlng.lat);
                //     var markers;
                //     if ($('input[name="lat"]').val() !== '') {
                //         self.state.map.eachLayer(function (layer) {
                //             self.state.map.setMapType('neshan');
                //         });
                //         var poly = new L.Polygon([
                //             array
                //         ]);
                //         poly.on('click', self.onMapClick);
                //         //Add polygon to map
                //         if (polyDraw != undefined) {
                //             self.state.map.removeLayer(polyDraw);
                //         }
                //         if (marker != undefined) {
                //             self.state.removeLayer(marker);
                //         }
                //         polyDraw = poly.addTo(map);
                //         markers = [
                //             {
                //                 "lat": $('#edit_user_pro input[name="location_lat"]').val(),
                //                 "lng": $('#edit_user_pro input[name="location_lng"]').val()
                //             }
                //         ];
                //         marker = L.marker([markers[0].lat, markers[0].lng], {icon: myIcon}).addTo(map);
                //     }
                // };
                //Create polygon
                var poly = new L.Polygon([
                    array
                ]);
                poly.on('click', self.onMapClick.bind(self));
                //Add polygon to map
                if (self.state.poly !== null) {
                    self.state.map.removeLayer(self.state.poly);
                }
                // if (marker !== null) {
                //     self.state.map.removeLayer(marker);
                // }
                polyDraw = poly.addTo(self.state.map);
                self.setState({
                    poly: polyDraw,
                })
            }
        });
    }

    onMapClick(e) {
        var self = this;
        if (this.state.marker !== null) {
            self.state.map.removeLayer(self.state.marker);
        }
        var myIcon = L.icon({
            iconUrl: '/assets/js/pages/school/images/pin24.png',
            iconRetinaUrl: '/assets/js/pages/school/images/pin48.png',
            iconSize: [29, 24],
            iconAnchor: [9, 21],
            popupAnchor: [0, -14]
        });
        var marker = L.marker([e.latlng.lat, e.latlng.lng], {icon: myIcon}).addTo(this.state.map);
        self.props.lng(e.latlng.lng);
        self.props.lat(e.latlng.lat);
        $('input[name="location_lng"]').val(e.latlng.lng);
        $('input[name="location_lat"]').val(e.latlng.lat);
        self.setState({
            lat: e.latlng.lat,
            lng: e.latlng.lng,
            marker: marker
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (typeof nextProps.id !== "undefined" && nextProps.id !== "null") {
            var self = this;
            $.ajax({
                url: '/admin/api/web/school/getCityLatLng',
                data: {'id': nextProps.id},
                method: "post",
                success: function (response) {
                    if (self.state.map === null) {
                        var map = new L.Map('map', {
                            key: 'web.HYeCa52JbXNCnJsXWDbITIPJd4AJLf4sdi5no76A',
                            center: [response.location_lat, response.location_lng],
                            zoom: 14
                        });
                        map.setMapType('neshan');
                        self.setState({
                            lat: response.location_lat,
                            lng: response.location_lng,
                            map: map,
                        });
                    } else {
                        self.state.map.setView([response.location_lat, response.location_lng], 13);
                        self.setState({
                            lat: response.location_lat,
                            lng: response.location_lng,
                        });
                    }
                }
            });
        } else if (nextProps.lat !== null && nextProps.lat !== this.state.lat) {
        //     var map = new L.Map('map', {
        //         key: 'web.HYeCa52JbXNCnJsXWDbITIPJd4AJLf4sdi5no76A',
        //         center: (typeof nextProps.lat !== "undefined" && nextProps.lat !== null) ? [nextProps.lat, nextProps.lng] : [this.state.lat, this.state.lng],
        //         zoom: 14
        //     });
        //     map.setMapType('neshan');
        //     var marker = {};
        //     var myIcon = L.icon({
        //         iconUrl: '/assets/js/pages/school/images/pin24.png',
        //         iconRetinaUrl: '/assets/js/pages/school/images/pin48.png',
        //         iconSize: [32, 26],
        //         iconAnchor: [9, 21],
        //         popupAnchor: [0, -14]
        //     });
        //     if (typeof nextProps.lat !== "undefined" && nextProps.lat !== null)
        //         marker = L.marker([nextProps.lat, nextProps.lng], {icon: myIcon}).addTo(map);
        //     this.setState({
        //         lat: nextProps.lat,
        //         lng: nextProps.lng,
        //     });
        // } else if (nextProps.profile && nextProps.regionId !== "undefined") {
        //     this.drawPolygon(nextProps.regionId);
        }
    }

    componentDidMount() {
        if (this.props.schoolPage) {
            var map = new L.Map('map', {
                key: 'web.HYeCa52JbXNCnJsXWDbITIPJd4AJLf4sdi5no76A',
                center: (this.props.lat !== null) ? (typeof this.props.lat.city !== "undefined") ? [this.props.lat.city, this.props.lng.city] : [this.props.lat, this.props.lng] : [this.state.lat, this.state.lng],
                zoom: 12
            });
            map.setMapType('neshan');
            var marker = {};
            var myIcon = L.icon({
                iconUrl: '/assets/js/pages/school/images/pin24.png',
                iconRetinaUrl: '/assets/js/pages/school/images/pin48.png',
                iconSize: [32, 26],
                iconAnchor: [9, 21],
                popupAnchor: [0, -14]
            });
            if (typeof this.props.lat.city === "undefined")
                marker = L.marker([this.props.lat, this.props.lng], {icon: myIcon}).addTo(map);
            this.setState({
                lat: this.props.lat,
                lng: this.props.lng,
            });
        }
    }

    directionToSchool() {
        navigator.geolocation.getCurrentPosition(function (location) {
            var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
            marker = L.marker([latlng.lat, latlng.lng], {icon: myIcon}).addTo(this.state.map);
            map.setView([36.32042434952838, 59.68998606107842], 12);

            var destination_lat = $('input[name="location_lat"]').val();
            var destination_lng = $('input[name="location_lng"]').val();

            $.ajax({
                type: "post",
                url: "/admin/api/web/nikarobank/direction",
                data: {origin: latlng.lat + "," + latlng.lng, destination: destination_lat + "," + destination_lng},
                success: function (response) {
                    var array = [];
                    for (var i = 0; i < response.length; i++) {
                        array[i] = response[i];
                    }
                    var polyLine = L.polyline(array).addTo(this.state.map);
                    // map.panTo(location.coords.latitude,location.coords.langitude);
                },
                complete: function () {
                }
            });
        });
    }

    render() {
        if ((this.state.map !== null && this.props.id !== null) || (this.state.map !== null && this.props.profile)) {
            this.state.map.on('click', this.onMapClick.bind(this));
        }
        return (
            <>
            </>
        )
    }
}

