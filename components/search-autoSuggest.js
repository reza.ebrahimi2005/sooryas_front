import React from 'react';
import AutoSuggest from 'react-autosuggest';
import axios from 'axios';
import SearchIcon from '@material-ui/icons/Search';
import $ from 'jquery/dist/jquery';


const CancelToken = axios.CancelToken;
let cancel;

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.title;

function renderSuggestion(suggestion, {query}) {

    var match = require('autosuggest-highlight/match');
    var parse = require('autosuggest-highlight/parse');

    const matches = match(suggestion.title, query);
    const parts = parse(suggestion.title, matches);
    return (
        <span className={'suggestion'}>
      {parts.map((part, index) => {
          const className = part.highlight ? 'react-autosuggest__suggestion-match' : null;
          return (
              <span className={className} key={index}>
            {part.text}
          </span>
          );
      })}
    </span>
    );
}

const handleSubmitClick = () => {
    $('#autoSuggestForm').submit();
};

const renderInputComponent = inputProps => (
    <div className={'searchInput-container'} style={{'position': 'relative'}}>
        <input {...inputProps}/>
        <SearchIcon className='searchIcon text-primary' onClick={handleSubmitClick}/>
    </div>
);

export default class SearchAutoSuggest extends React.Component {
    constructor(props) {
        super(props);

        // Autosuggest is a controlled component.
        // This means that you need to provide an input value
        // and an onChange handler that updates this value (see below).
        // Suggestions also need to be provided to the Autosuggest,
        // and they are initially empty because the Autosuggest is closed.
        this.state = {
            value: '',
            suggestions: [],
            placeholder: null,
        };
    }

    componentDidMount() {
        var url = new URL(window.location.href);
        var c = url.searchParams.get('q');
        if (c != null && c.length > 0) {
            this.setState({
                placeholder: c
            })
        } else {
            this.setState({
                placeholder: 'جستجوی کلاس یا آموزشگاه'
            })
        }
    }

    componentWillMount() {
        this.timer = null;
    }

    onChange(event, {newValue, method}) {
        event.persist();
        this.setState({
            value: newValue
        });
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested({value}) {
        cancel && cancel();

        axios.get(process.env.NEXT_PUBLIC_API_URL + '/search/suggestion', {
            params: {
                q: value
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel = c;
            })
        }).then((response) => {
            this.setState({
                suggestions: response.data[0]
            });
        });
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested(event) {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const {value, suggestions} = this.state;
        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: this.state.placeholder,
            value,
            onChange: this.onChange.bind(this),
            className: 'form-control',
            name: 'q',
            type: 'search'
        };

        // Finally, render it!
        return (
            <AutoSuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                renderInputComponent={renderInputComponent}
                inputProps={inputProps}
            />
        );
    }
}
