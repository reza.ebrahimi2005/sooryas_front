import React, {Component} from 'react';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import dynamic from "next/dynamic";

const Slider = dynamic(
    () => {
        return import('../components/slider');
    },
    {ssr: false}
);

export default class SchoolCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isMobile: true
        }
    }

    updateDimensions() {

        if (window.innerWidth < 575) {

            this.setState({isMobile: false});
        } else {
           // alert('ggggggggggg');
            this.setState({isMobile: true});
        }
       // console.log(this.state.isMobile);
    }
    componentDidMount() {
        this.setState({
            isMobile: (window.innerWidth <= 575)
        })

        console.log("componentDidMount");

        window.addEventListener("resize", this.updateDimensions());


    }

    componentWillUnmount() {
        console.log(this.state.data);
        window.removeEventListener("resize", this.updateDimensions.bind(this));

    }

    render() {
        console.log(this.props);
        let content=''
        if (typeof this.state.data.schoolsLogo !== "undefined") {

            if (this.state.isMobile) {
                var temp = [];
                this.state.data.schoolsLogo.forEach(function (e,index) {
                    temp.push(
                        <div key={index} className='col-4 school-logo'>
                            <a href={(e.website !== null) ? e.website : e.shortLink}><img src={e.logo}/></a>
                        </div>
                    )
                });
                content =
                    <div className='row'>
                        {temp}
                    </div>
            } else {
                var temp = [];

                this.state.data.schoolsLogo.forEach(function (e, index) {
                    temp.push(
                        <div className="schoolCarouselCell" key={index}>
                            <LazyLoad>
                                <a href={(e.website !== null) ? e.website : e.shortLink}><img src={e.logo}
                                                                                              data-id={e.id}/></a>
                            </LazyLoad>
                        </div>
                    )
                });
                content =
                    <div className='logo-carousel'>
                        <div style={{display: 'flex', justifyContent: 'space-between'}}/>
                        <Slider
                            options={{
                                autoPlay: 4000,
                                pauseAutoPlayOnHover: true,
                                fullscreen: true,
                                pageDots: false,
                                wrapAround: true
                            }}
                        >
                            {temp}
                        </Slider>
                    </div>
            }
        }
        return (
            <>
                {content}
            </>
        )
    }
}
