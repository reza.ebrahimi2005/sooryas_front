import React, {Component} from 'react';
import Cookies from "js-cookie";
export default class UserWallet extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    submitWalletChargeForm(){
        event.preventDefault();
        var amount=$('#walletChargeForm input[name="amount"]').val();
        const token = Cookies.get('token');
        if(amount!=null && amount.length>0){
            $.ajax({
                url: '/admin/api/web/walletCharge',
                data: {'amount': amount, 'pay_type': "wallet"},
                beforeSend: function (xhr) {

                    xhr.setRequestHeader('Authorization', "Bearer" + " " + token);
                },
                method: 'POST',
                success: function (response) {
                    // console.log(JSON.stringify(response.Payload));
                    var html = '<form id="paymentUTLfrm" action="https://mabna.shaparak.ir:8080/Pay" method="POST">' +
                        '<input type="hidden" id="TerminalID" name="TerminalID" value="' + response.TerminalID + '">' +
                        '<input type="hidden" id="Amount" name="Amount" value="' + response.Amount + '">' +
                        '<input type="hidden" id="callbackURL" name="callbackURL" value="' + response.callbackURL + '">' +
                        '<input type="hidden" id="InvoiceID" name="InvoiceID" value="123">' +
                        '<input type="hidden" id="Payload" name="Payload" value="' + response.Payload + '">' +
                        '</form>';
                    //'+encodeURIComponent(JSON.stringify(response.Payload))+'
                    $('#increaseCredit').html(html);
                },
                complete: function () {
                    if ($('#paymentUTLfrm').length) {
                        document.getElementById("paymentUTLfrm").submit();
                    }
                }
            });
        }else{
            toastr.options = {
                "progressBar": true,
                "positionClass": "toast-top-left",
                "timeOut": "4000",
            };
            toastr["error"]("لطفا مبلغ را وارد کنید");
        }

    }

    // addComma(e){
    //     e.persist();
    //     var commaNumber = require('comma-number');
    //     var temp=e.target;
    //     console.log(temp.val());
    // }

    render() {
        return (
            <div className='row mt-4' id='userWallet'>
                <div className="col-md-4 col-12 text-center">
                    <img src={'/assets/images/wallet.svg'}/>
                </div>
                <div className="col-md-8 col-12 text-right mt-4">
                    <span className="text-black bold d-md-block d-none ">کیف پول سوریاس  </span>
                    <p className="text-justify mt-2"> کیف پول سوریاس  سرویسی است كه امكان استفاده از اعتبار مالی برای ثبت نام دوره های آموزشی را فراهم
                        می کند. کاربران سوریاس مهر می‌توانند با شارژ حساب یا کسب اعتبار از طریق معرفی دوستان اعتبارشان را
اضافه کنند تا در دوره های بعدی استفاده کنند.</p>
                    <span className='d-block btn btn-outline-primary'>موجودی کیف پول شما:  {this.props.data.currentWalletAmount} تومان</span>
                    <form className="text-center" id={'walletChargeForm'} onSubmit={this.submitWalletChargeForm.bind(this)}>
                        <input type="number" placeholder='مبلغ موردنظر (تومان)' className="mt-4" style={{'height':'40px'}} name={'amount'} />
                        <button type="submit" className="btn btn-primary rounded-pill mr-2">افزایش اعتبار</button>
                    </form>
                    <div id='increaseCredit'/>
                </div>
            </div>

        );
    }
}
