import React, {Component} from 'react';
import dynamic from "next/dynamic";


const options = [
    {value: '1', label: 'دوره'},
    {value: '2', label: 'آموزشگاه'},
];

const Select = dynamic(
    () => {
        return import('react-select');
    },
    {ssr: false}
);

export default class CourseSchoolSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,
        };
    }

    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var selectedOption = null;

        if (url.searchParams.get('courseSchool') != null && url.searchParams.get('courseSchool') != 0) {
            if (url.searchParams.get('courseSchool') == 1)
                selectedOption = {value: '1', label: 'دوره'};
            else
                selectedOption = {value: '2', label: 'آموزشگاه'};
        }
    }

    handleChange(selectedOption) {
        this.setState({selectedOption});
        this.props.func(selectedOption);
    };

    render() {
        const {selectedOption} = this.state;
        return (
            <Select
                value={selectedOption}
                onChange={this.handleChange.bind(this)}
                options={options}
                classNamePrefix={'course-school-select'}
                placeholder={'دوره / آموزشگاه'}
                isSearchable={false}
                isClearable={true}
            />
        );
    }
}
