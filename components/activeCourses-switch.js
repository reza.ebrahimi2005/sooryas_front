import React, { Component } from "react";
import Switch from "react-switch";

export default class ActiveCourseSwitch extends Component {
    constructor() {
        super();
        this.state = { checked: false };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(checked) {
        this.setState({ checked });
        this.props.func(checked);
    }

    render() {
        return (
            <div className={'react-switch-container'}>
                <Switch
                    onChange={this.handleChange}
                    checked={this.state.checked}
                    onColor="#86d3ff"
                    onHandleColor="#2693e6"
                    uncheckedIcon={false}
                    checkedIcon={false}
                    width={40}
                    height={20}
                    className="react-switch"
                    id="small-radius-switch"
                />
                <span>فقط دوره های فعال</span>
            </div>
        );
    }
}