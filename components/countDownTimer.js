import React, {useEffect, useState} from "react";
import {forEach} from "react-bootstrap/cjs/ElementChildren";

export default function CountdownTimer(props) {
    const calculateTimeLeft = () => {
        var offset = new Date().getTimezoneOffset();
        const difference = (+new Date(props.date) - +new Date()) + (offset * 60 * 1000) + (24 * 60 * 60 * 1000);
        let timeLeft = null;

        if (difference > 0) {
            timeLeft = {
                'second': (Math.floor((difference / 1000) % 60) < 10)
                    ?
                    '0' + Math.floor((difference / 1000) % 60)
                    :
                    Math.floor((difference / 1000) % 60),
                'minute': (Math.floor((difference / 1000 / 60) % 60) < 10)
                    ?
                    '0' + Math.floor((difference / 1000 / 60) % 60)
                    :
                    Math.floor((difference / 1000 / 60) % 60),
                'hour': (Math.floor(difference / (1000 * 60 * 60) % 24) < 10)
                    ?
                    '0' + Math.floor(difference / (1000 * 60 * 60)) % 24
                    :
                    Math.floor(difference / (1000 * 60 * 60)) % 24,
                'day': Math.floor(difference / (1000 * 60 * 60 * 24))
            }

            // days: Math.floor(difference / (1000 * 60 * 60 * 24)),
            // hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
            // minutes: Math.floor((difference / 1000 / 60) % 60),
            // seconds: Math.floor((difference / 1000) % 60)
        }
        return timeLeft;

    };

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {
        setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);
    });

    const timerComponents = [];

    if (timeLeft !== null) {
        timerComponents.push(
            <>
                <span className='px-2 d-inline-block'>{timeLeft.second}</span>
                <span className='px-2 d-inline-block'>{timeLeft.minute}</span>
                <span className='px-2 d-inline-block'>{timeLeft.hour}</span>
                <span className='px-2 d-inline-block'>{timeLeft.day}</span>
            </>
        )
    }
    return (
        <div className='text-center timer'>
            {timerComponents.length ? timerComponents : <span className='text-nowrap'>تمام شده !</span>}
        </div>
    );
}
