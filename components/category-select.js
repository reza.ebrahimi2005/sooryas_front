import React, {Component} from 'react';
import axios from 'axios';
import dynamic from "next/dynamic";

const CancelToken = axios.CancelToken;
let cancel;

const AsyncSelect=dynamic(
    () => {
        return import('react-select/async');
    },
    {ssr: false}
);

export default class CategorySelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: this.props.defaultValue,
            defaultOption: []
        };
        this.getOptions = this.getOptions.bind(this);
        var options = [];
        axios.get(process.env.NEXT_PUBLIC_API_URL+'/select2CourseCategory').then((response) => {
            response.data.forEach(function (e) {
                options[e.id] = {'value': e.id, 'label': e.text}
            });
            var url_string = window.location.href;
            var url = new URL(url_string);
            var selectedOption = null;

            if (url.searchParams.get('category') != null && url.searchParams.get('category') !== "0") {
                this.setState({
                    selectedOption:options[url.searchParams.get('category')],
                    defaultOption: options
                })
            }else{
                this.setState({
                    defaultOption: options
                })
            }
        });
    }


    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
        this.props.func(selectedOption);
    };

    mapOptionsToValues(options) {
        return options.map(option => ({
            value: option.text,
            label: option.text
        }));
    };

    getOptions(inputValue, callback) {
        if (!inputValue) {
            return callback([]);
        }

        cancel && cancel();

        axios.get(process.env.NEXT_PUBLIC_API_URL+'/select2CourseCategory', {
            params: {
                term: inputValue
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel = c;
            })
        }).then((response) => {
            const results = response.data;
            if (this.props.mapOptionsToValues)
                callback(this.props.mapOptionsToValues(results));
            else callback(this.mapOptionsToValues(results));
        });

    };


    render() {
        const customStyles = {
            input: (provided, state) => ({
                ...provided,
                border:'none'
            }),
        };
        return (
            <AsyncSelect
                onChange={this.handleChange.bind(this)}
                placeholder={'انتخاب دسته بندی'}
                loadOptions={this.getOptions}
                value={this.state.selectedOption}
                defaultOptions={this.state.defaultOption}
                classNamePrefix={'react-select'}
                isClearable={true}
                name={"category_id"}
                id={"category_id"}
            />
        )
    }
}
