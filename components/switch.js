import React, {useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';

const IOSSwitch = withStyles(theme => ({
    root: {
        width: 42,
        height: 26,
        padding: 0,
        margin: theme.spacing(1),
    },
    switchBase: {
        padding: 1,
        '&$checked': {
            transform: 'translateX(16px)',
            color: theme.palette.common.white,
            '& + $track': {
                backgroundColor: '#52d869',
                opacity: 1,
                border: 'none',
            },
        },
        '&$focusVisible $thumb': {
            color: '#52d869',
            border: '6px solid #fff',
        },
    },
    thumb: {
        width: 24,
        height: 24,
    },
    track: {
        borderRadius: 26 / 2,
        border: `1px solid ${theme.palette.grey[400]}`,
        backgroundColor: theme.palette.grey[500],
        opacity: 1,
        transition: theme.transitions.create(['background-color', 'border']),
    },
    checked: {},
    focusVisible: {},
}))(({classes, ...props}) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});

const useStyles = makeStyles({
    label: {
        width: 150
    }
});

export default function SwitchButton(props) {
    const classes = useStyles();

    const [state, setState] = useState({
        checked: (props.value === 1)
    });

    const handleChange = name => event => {
        setState({...state, [name]: event.target.checked});
        props.state(event.target.checked);
    };

    return (
        <>
            <label className={classes.label}>{props.label}</label>
            <IOSSwitch
                checked={state.checked}
                onChange={handleChange('checked')}
                value="checked"
            />
        </>
    );
}