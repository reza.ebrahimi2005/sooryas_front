import React, {Component} from 'react';
import ProgressBar from 'progressbar.js'


export default class ShowRatingBar extends Component {
    componentDidMount() {
        var rate=this.props.rate;
        var line = new ProgressBar.Line('.progress-'+this.props.id, {
            duration: 3000,
            easing: 'easeInOut',
            color: '#007bff',
            from: {color: '#007bff'},
            to: {color: '#00cbff'},
            step: (state, bar) => {
                bar.setText(Math.round(bar.value() * 100) + ' %');
            }
        });

        line.animate(rate);
    }

    render() {
        var temp=this.props.id;
        return (
            <div className={'progress progress-'+temp}/>
        )
    }
}

