import React, {Component} from 'react';
import Pushe from "pushe-webpush";
import axios from 'axios';
export default class WebPush extends Component  {
    constructor(props) {
        super(props);
        this.state={
            config :props.config,
            userId :props.user,
            device_token:props.device_token,
        };
        Pushe.init(this.state.config.pushe.app_id);
        Pushe.subscribe();
    }
    componentDidMount() {

        // console.log(Pushe);
        Pushe.getDeviceId()
            .then((deviceId)=>{
                console.log(deviceId);
            this.setState({currentDiviceId: deviceId });
           let previousToken = this.state.device_token.split(',');
                if(previousToken.indexOf(deviceId)== -1){
                    axios.post('/users/addDeviceToken', {
                        id: this.state.userId,
                        device_token:deviceId
                    })
                        .then(function (response) {
                            // console.log(response);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            });

       // console.log('curent DeviceId :'+this.state.currentDiviceId);
    }

    render() {
        return (
          <div></div>
        );
    }
}