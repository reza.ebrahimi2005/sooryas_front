import React, {Component} from 'react';
import dynamic from "next/dynamic";


const options = [
    {value: '1', label: 'کودک'},
    {value: '2', label: 'نوجوان'},
    {value: '3', label: 'بزرگسال'}
];

const Select = dynamic(
    () => {
        return import('react-select');
    },
    {ssr: false}
);

export default class AgeSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null
        }

    }

    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);

        const options = [
            {value: '1', label: 'کودک'},
            {value: '2', label: 'نوجوان'},
            {value: '3', label: 'بزرگسال'}
        ];

        if (url.searchParams.get('age') != null && url.searchParams.get('age') !== 0) {
            this.state = {
                selectedOption: options[url.searchParams.get('age') - 1]
            };
        } else {
            this.state = {
                selectedOption: null
            };
        }
    }

    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
        this.props.func(selectedOption);
    };

    render() {
        const {selectedOption} = this.state;

        return (
            <Select
                value={selectedOption}
                onChange={this.handleChange.bind(this)}
                options={options}
                classNamePrefix={'react-select'}
                placeholder={'رده سنی'}
                isClearable={true}
            />
        );
    }
}
