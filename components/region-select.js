import React, {Component} from 'react';
import axios from 'axios';
import dynamic from "next/dynamic";

const CancelToken = axios.CancelToken;
let cancel;


const AsyncSelect=dynamic(
    () => {
        return import('react-select/async');
    },
    {ssr: false}
);

export default class RegionSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cityId: false,
            selectedOption: this.props.defaultValue,
            defaultOption: []
        };
        this.getOptions = this.getOptions.bind(this);
    }

    componentDidMount() {
        var options = [];
        var url_string1 = window.location.href;
        var url1 = new URL(url_string1);
        var cityId=url1.searchParams.get('cityId');
        axios.get(process.env.NEXT_PUBLIC_API_URL+'/select2Regions?showInList=true',{
            params:{
                city_id:cityId
            }
        }).then((response) => {
            response.data.forEach(function (e) {
                options[e.id] = {'value': e.id, 'label': e.text}
            });
            var url_string = window.location.href;
            var url = new URL(url_string);
            var selectedOption = null;

            if (url.searchParams.get('regionId') != null && url.searchParams.get('regionId') !== 0) {
                this.setState({
                    selectedOption: options[url.searchParams.get('regionId')],
                    defaultOption: options
                })
            } else if (this.props.profile) {
                this.setState({
                    selectedOption: options[this.props.id],
                    defaultOption: options
                })
            } else {
                this.setState({
                    defaultOption: options
                })
            }
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.cityId !== 0 && nextProps.cityId !== this.state.cityId) {
            axios.get(process.env.NEXT_PUBLIC_API_URL+'/select2Regions?showInList=true',{
                params:{
                    city_id:nextProps.cityId
                }
            }).then((response) => {
                var options = [];
                response.data.forEach(function (e) {
                    options[e.id] = {'value': e.id, 'label': e.text}
                });
                this.setState({
                    cityId: nextProps.cityId,
                    selectedOption: options[nextProps.selectedOption],
                    defaultOption: options
                })
            });
        }
    }

    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
        this.props.func(selectedOption);
    };

    mapOptionsToValues(options) {
        return options.map(option => ({
            value: option.id,
            label: option.text
        }));
    };

    getOptions(inputValue, callback) {
        if (!inputValue) {
            return callback([]);
        }

        cancel && cancel();

        axios.get(process.env.NEXT_PUBLIC_API_URL+'/select2Regions?showInList=true', {
            params: {
                city_id: this.state.cityId,
                term:inputValue
            },
            cancelToken: new CancelToken(function executor(c) {
                // An executor function receives a cancel function as a parameter
                cancel = c;
            })
        }).then((response) => {
            const results = response.data;
            if (this.props.mapOptionsToValues)
                callback(this.props.mapOptionsToValues(results));
            else callback(this.mapOptionsToValues(results));
        });

    };


    render() {
        return (
            <AsyncSelect
                onChange={this.handleChange.bind(this)}
                placeholder={'انتخاب منطقه'}
                loadOptions={this.getOptions}
                value={this.state.selectedOption}
                defaultOptions={this.state.defaultOption}
                classNamePrefix={'react-select'}
                name={'region_id'}
                isClearable={true}
            />
        )
    }
}
