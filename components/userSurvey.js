import React, {Component} from 'react';
import axios from "axios";
import Button from '@material-ui/core/Button';

export default class UserSurvey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: (this.props.notProfile) ? null : this.props.data,
            surveyData: null,
            rates: null,
            form: null,
            courseId: null
        };
        if (this.props.notProfile) {
            var url_string = window.location.href;
            var url = new URL(url_string);
            var userId = url.searchParams.get('userId');
            var courseId = url.searchParams.get('courseId');
            axios.post('/admin/api/web/getUserSurvey', {'userId': userId, 'courseId': courseId})
                .then((response) => {
                var rates = [];
                axios.post('/admin/api/web/comments/getResult', {
                    type: 'course',
                    type_id: courseId,
                }).then((response) => {
                    response.data[1].forEach(function (e, index) {
                        rates.push(
                            <div className='form-group' index={index}>
                                <label htmlFor={'rate-' + e.id}>{e.title}</label>
                                <select id={"rate-" + e.id} className='form-control rateBar' name={'rate-' + e.id}>
                                    <option value="1" key={1}>بسیارضعیف</option>
                                    <option value="2" key={2}>ضعیف</option>
                                    <option value="3" key={3}>متوسط</option>
                                    <option value="4" key={4}>خوب</option>
                                    <option value="5" key={5}>عالی</option>
                                </select>
                            </div>
                        )
                    });
                    this.setState({
                        surveyData: response.data[1],
                        rates: rates,
                        courseId: courseId,
                        data: true,
                    });
                    $('#userCourseSurveyForm').removeClass('d-none').addClass('d-flex');
                    $('.rateBar').barrating('clear');
                    $('.rateBar').barrating('show', {
                        theme: 'bars-movie',
                        initialRating: 3,
                    });
                });
            })
                .catch(error => {
                if (error.response.status === 403) {
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "5000",
                    };
                    toastr['info']("برای شرکت در نظر سنجی لطفا وارد حساب کاربری خود شوید.");
                    $('#loginRegisterModal').modal('show');
                }else if(error.response.status === 401){
                    toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-center",
                        "timeOut": "5000",
                    };
                    toastr['error']("شما قبلا به این نظر سنجی پاسخ داده اید");
                    setTimeout(function(){ window.location.href = '/'; }, 3000);

                }
            })
        }
    }

    handleSelectChange() {
        var course_id = $('select[name="courses"]').val();
        $('#userCourseSurveyForm input[name="course_id"]').val(course_id);
        $('#userCourseSurveyForm').removeClass('d-flex').addClass('d-none');
        $('#userCourseSurveyForm')[0].reset();
        var rates = [];
        axios.post('/comments/getResult', {
            type: 'course',
            type_id: course_id,
        }).then((response) => {
            response.data[1].forEach(function (e, index) {
                rates.push(
                    <div className='form-group' index={index}>
                        <label htmlFor={'rate-' + e.id}>{e.title}</label>
                        <select id={"rate-" + e.id} className='form-control rateBar' name={'rate-' + e.id}>
                            <option value="1" key={1}>بسیارضعیف</option>
                            <option value="2" key={2}>ضعیف</option>
                            <option value="3" key={3}>متوسط</option>
                            <option value="4" key={4}>خوب</option>
                            <option value="5" key={5}>عالی</option>
                        </select>
                    </div>
                )
            });
            this.setState({
                surveyData: response.data[1],
                rates: rates,
                courseId: course_id,
            });
        });
    }

    submitUserCourseSurveyForm() {
        var self = this;
        var form = $('#userCourseSurveyForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        $('#userCourseSurveyForm').validate({
            debug: true,
            doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                content: {
                    required: true,
                },
            },

            messages: {
                content: {
                    required: 'این فیلد را کامل نمایید'
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
        });

        if ($('#userCourseSurveyForm').valid()) {
            var formData = new FormData($('#userCourseSurveyForm')[0]);
            formData.append('type', 'course');
            formData.append('type_id', self.state.courseId);
            $.ajax({
                url: '/comments/save',
                type: 'post',
                dataType: "json",
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    if (result.statusCode === 200) {
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-left",
                            "timeOut": "5000",
                        };
                        toastr['success'](result.message);
                        if (self.props.notProfile)
                            setTimeout(function(){ window.location.href = '/'; }, 3000);
                        else
                            window.location.reload();
                    } else {
                        toastr.options = {
                            "progressBar": true,
                            "positionClass": "toast-top-center",
                            "timeOut": "5000",
                        };
                        toastr['error'](result.message);
                    }
                }
            });
        } else {
            return;
        }
    }

    handleShowForm() {
        $('#userCourseSurveyForm').removeClass('d-none').addClass('d-flex');
        $('.rateBar').barrating('clear');
        $('.rateBar').barrating('show', {
            theme: 'bars-movie',
            initialRating: 3,
        });
    }

    render() {
        var content;
        if (this.state.data !== null) {
            content =
                <div className='userSurvey row text-right p-4 justify-content-center align-items-center' id='userSurvey'
                     style={(this.props.notProfile) ? {'height': '100vh'} : null}>
                    {
                        (this.props.notProfile)
                            ?
                            null
                            :
                            (this.state.data.length > 0)
                                ?
                                <>
                                    <strong className='d-block col-md-5 col-12'>لطفا برای شرکت در نظر سنجی دوره مورد نظر
                                        را
                                        انتخاب
                                        کنید</strong>
                                    <select name="courses" className='col-md-5 col-12 form-control'
                                            onChange={this.handleSelectChange.bind(this)}>
                                        <option value={0} defaultValue>انتخاب کنید</option>
                                        {
                                            this.state.data.map(item => (
                                                <option value={item.id}>{item.title + " (" + item.name + ") "}</option>
                                            ))
                                        }
                                    </select>
                                    <div className='col-2'>
                                        <Button className='bg-primary text-white' style={{'fontFamily': 'IRANSans'}}
                                                onClick={this.handleShowForm}>نمایش</Button>
                                    </div>
                                </>
                                :
                                null
                    }
                    <div className='col-12 px-md-5 mt-5'>
                        <form id='userCourseSurveyForm' className='d-none flex-column' data-id="0">
                            <label htmlFor="comment">لطفا با ثبت تجربیاتتون، به سایر کاربران در انتخاب آموزشگاه یا کلاس
                                کمک کنید.</label>
                            <textarea name="content" id='comment' rows="4" className='mb-2'></textarea>
                            <span className='d-block my-2'>به هر یک از سوالات زیر نیز میتوانید از کمترین (بسیارضعیف) تا بیشترین (عالی) امتیاز بدهید</span>
                            {this.state.rates}
                            <input type="hidden" value={this.state.courseId} name='course_id'/>
                            <Button className='bg-primary text-white my-4' style={{'fontFamily': 'IRANSans'}}
                                    onClick={this.submitUserCourseSurveyForm.bind(this)}>
                                ارسال
                            </Button>
                        </form>

                    </div>
                </div>
        } else {
            content =
                <div className='row userSurvey justify-content-center align-items-center' style={{'height': '100vh'}}>
                    <span className='d-block col-12 text-center'>موردی یافت نشد</span>
                </div>
        }
        return (
            <>
                {content}
            </>
        );
    }
}
