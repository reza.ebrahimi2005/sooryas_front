import React, {Component} from 'react';
import dynamic from "next/dynamic";

const Slider = dynamic(
    () => {
        return import('../components/slider');
    },
    {ssr: false}
);

export default class SchoolPageGalleryCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            schoolId: 0,
            data: props.data
        };
    }

    render() {
        let content = [];
        (typeof this.state.data !== "undefined" && this.state.data.length > 0)
            ?
            this.state.data.forEach(function (e, index) {
                content.push(
                    <div className={'slider-cell'} key={index}>
                        <img src={e}/>
                    </div>
                )
            })
            :
            content =
                <>
                    <div className={'slider-cell'}>
                        <img src={('/assets/images/placeholder.jpg')} style={{'objectFit': 'cover'}}/>
                    </div>
                </>;
        return (
            <>
                <Slider
                    options={{
                        wrapAround: true,
                        fullscreen: true,
                        pageDots: false,
                    }}
                >
                    {content}
                </Slider>
            </>
        )
    }
}
