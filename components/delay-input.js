import React from 'react';
import {DelayInput} from 'react-delay-input';

export default class Delay extends React.Component {
    constructor() {
        super();
        this.state = {
            value: ''
        };
    }


    render() {
        return (
            <div>
                <DelayInput
                    minLength={2}
                    delayTimeout={300}
                    onChange={event => this.setState({value: event.target.value})}
                    type={'text'}
                    className='form-control'
                    placeholder={'آموزشگاه، کلاس، شهر و... را جستجو کنید'}
                    value
                />
            </div>
        );


    }
}
