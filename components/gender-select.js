import React, {Component} from 'react';
import dynamic from "next/dynamic";


const options = [
    {value: '1', label: 'زن'},
    {value: '2', label: 'مرد'},
];

const Select = dynamic(
    () => {
        return import('react-select');
    },
    {ssr: false}
);

export default class GenderSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null
        }
    }

    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        const options = [
            {value: '1', label: 'زن'},
            {value: '2', label: 'مرد'},
        ];

        if (url.searchParams.get('gender') !== null && url.searchParams.get('gender') !== 0) {
            this.state = {
                selectedOption: options[url.searchParams.get('gender') - 1]
            };
        } else {
            this.state = {
                selectedOption: null,
            };
        }
    }

    handleChange(selectedOption) {
        this.setState({
            selectedOption: selectedOption
        });
        this.props.func(selectedOption);
    };

    render() {
        const {selectedOption} = this.state;

        return (
            <Select
                value={selectedOption}
                onChange={this.handleChange.bind(this)}
                options={options}
                classNamePrefix={'react-select'}
                placeholder={'جنسیت'}
                isClearable={true}

            />
        );
    }
}
