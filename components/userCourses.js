import React, {Component, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import axios from "axios";
import Loading from "./loading";

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    tableWrapper: {
        maxHeight: '80vh',
        overflow: 'auto',
    },
    tableCell: {
        fontFamily: 'IRANSans',
        fontSize: '13px'

    },
    button: {
        fontFamily: 'IRANSans',
        fontSize: '13px'
    },
    showIcon: {
        marginRight: 10
    }
});

function createData(id, coursePrice, teacher, registerCode, skyroomUrl, title, start_date, end_date, name, score_id) {
    return {id, coursePrice, teacher, registerCode, skyroomUrl, title, start_date, end_date, name, score_id};
}

export default function StickyHeadTable(props) {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    var rows = props.data.map(item => (
        createData(item.id, item.coursePrice, item.teacher, item.registerCode, item.skyroomUrl, item.title, item.start_date, item.end_date, item.name, item.score_id)
    ));

    function showDisableMessage() {
        toastr.options = {
            "progressBar": true,
            "positionClass": "toast-top-left",
            "timeOut": "5000",
        };
        toastr["error"]("کارنامه ای تعریف نشده است!");
    }

    const redirectToSkyRoom = (e) => {
        var courseId = e.target.parentElement.getAttribute('id');
        setLoading(true);
        axios.post(process.env.NEXT_PUBLIC_API_URL + '/profile/getSkyRoomUrl', {
            courseId: courseId
        }).then((response) => {
            setLoading(false);
            if (response.data.status) {
                window.open(response.data.url);
            } else {
                bootbox.alert('دریافت اطلاعات با خطا مواجه شد لطفا مجددا تلاش نمایید');
            }
        });
    };

    return (
        <Paper className={classes.root}>
            {loading && <Loading/>}
            <div className={classes.tableWrapper}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.tableCell} align="center">عنوان دوره</TableCell>
                            <TableCell className={classes.tableCell} align="center">تاریخ شروع</TableCell>
                            <TableCell className={classes.tableCell} align="center">تاریخ پایان</TableCell>
                            <TableCell className={classes.tableCell} align="center">شرکت کننده</TableCell>
                            <TableCell className={classes.tableCell} align="center">عملیات</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.id}>
                                <TableCell className={classes.tableCell} align='center'>{row.title}</TableCell>
                                <TableCell className={classes.tableCell} align="center">{row.start_date}</TableCell>
                                <TableCell className={classes.tableCell} align="center">{row.end_date}</TableCell>
                                <TableCell className={classes.tableCell} align="center">{row.name}</TableCell>
                                <TableCell className={classes.tableCell} align="center">
                                    {
                                        (row.skyroomUrl !== null)
                                            ?
                                            <Button onClick={redirectToSkyRoom.bind(this)}
                                                    variant="contained"
                                                    target='_blank'
                                                    id={row.id}
                                                    color="primary" className={classes.button}>ورود به کلاس
                                                مجازی</Button>
                                            :
                                            <Button href={'/courses/courseReceipt?id=' + row.registerCode}
                                                    variant="contained"
                                                    color="primary" className={classes.button}>رسید ثبت نام</Button>
                                    }

                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        </Paper>
    );
}
