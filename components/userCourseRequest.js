import React, {useEffect, useState} from 'react';
import {create} from 'jss';
import rtl from 'jss-rtl';
import {createMuiTheme, makeStyles, ThemeProvider, StylesProvider, jssPreset} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import TextField from "@material-ui/core/TextField";
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import SubCategorySelect from "./subCategory-select";
import CitySelect from "./city-select";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {useForm} from "react-hook-form";
import $ from 'jquery'
import Cookies from "js-cookie";

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, rtl()]});

const theme = createMuiTheme({
    direction: "rtl",
    typography: {
        fontFamily: 'IRANSans',
        textAlign: 'right'
    },
    palette: {
        primary: {
            main: '#007aff',
        },
        secondary: {
            light: '#0066ff',
            main: '#0044ff',
            contrastText: '#ffcc00',
        },
    },
});

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    formButton: {
        fontFamily: "IRANSans",
        color: "#007aff",
        borderColor: "#007aff",
        width: '100%'
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 2, 2),
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            height: '100%',
        },
        [theme.breakpoints.up('sm')]: {
            width: '350px',
        },
    },
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        margin: '0 auto',
        marginTop: '20px',
    },
    button: {
        fontFamily: "IRANSans"
    }
}));

export default function UsersRequest(props) {
    const classes = useStyles();
    // const {register, handleSubmit, watch, errors} = useForm();
    const [showForm, setShowForm] = useState(false);
    const [loading, setLoading] = useState(false);
    const [subCategorySelect, setSubCategorySelect] = useState(<SubCategorySelect id={0}/>);
    const [subCityId, setCityId] = useState(null);
    const token = Cookies.get('uid')

    useEffect(() => {
        var header = $("#usersRequest button");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 100) {
                header.removeClass("MuiFab-extended");
                if ($(window).scrollTop() + $(window).height() + 10 >= $(document).height()) {
                    header.hide();
                } else {
                    header.show();
                }
            } else {
                header.addClass("MuiFab-extended");
            }
        });

    })

    const handleOpen = () => {
        setShowForm(true);
    };

    const handleClose = () => {
        setShowForm(false);
    };

    const handleCityId = (id) => {
        setCityId(id);
    }

    const getCategoryId = (value) => {
        setSubCategorySelect(<SubCategorySelect id={value.value}/>)
    };

    // const onSubmit = (data) => {
    //     alert(JSON.stringify(data));
    // };

    const handleSubmit = () => {
        // $('#usersRequestForm').validate({
        //     debug: true,
        //     doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        //     errorElement: 'span', //default input error message container
        //     errorClass: 'help-block help-block-error', // default input error message class
        //     focusInvalid: false, // do not focus the last invalid input
        //     rules: {
        //         category_id: {
        //             required: true,
        //         },
        //         subCategory_id: {
        //             required: true
        //         }
        //     },
        //
        //     messages: {
        //         category_id: {
        //             required: 'فیلد نام را وارد نمایید'
        //
        //         },
        //         subCategory_id: {
        //             required: 'فیلد نام را وارد نمایید'
        //         }
        //     },
        //
        //     invalidHandler: function (event, validator) { //display error alert on form submit
        //         success.hide();
        //         error.show();
        //     },
        //
        //     errorPlacement: function (error, element) { // render error placement for each input type
        //         error.insertBefore(element);
        //     },
        //
        //     highlight: function (element) { // hightlight error inputs
        //         $(element).closest('.MuiInput-formControl').addClass('has-error'); // set error class to the control group
        //     },
        //
        //     unhighlight: function (element) { // revert the change done by hightlight
        //         $(element).closest('.MuiInput-formControl').removeClass('has-error'); // set error class to the control group
        //     },
        //
        //     success: function (label) {
        //         label.closest('.MuiInput-formControl').removeClass('has-error'); // set success class to the control group
        //     }
        // });
        // if ($('#usersRequestForm').valid()) {
        if ($('#usersRequestModal input[name="category_id"]').val() === "") {
            $('.categoryLabel').removeClass('d-none').addClass('d-block');
            return;
        }
        if ($('#usersRequestModal input[name="subCategory_id"]').val() === "") {
            $('.subCategoryLabel').removeClass('d-none').addClass('d-block');
            return;
        }
        if ($('#usersRequestModal input[name="city_id"]').val() === "") {
            $('.cityLabel').removeClass('d-none').addClass('d-block');
            return;
        }
        let myForm = document.getElementById('usersRequestForm');
        var data = new FormData(myForm);
        data.set('page_url', window.location.href);
        setLoading(true);
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/users/usersCourseRequest',
            data: data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            method: "post",
            success: function (data) {
                setTimeout(function () {
                    // toastr.options = {
                    //     "progressBar": true,
                    //     "positionClass": "toast-top-center",
                    //     "timeOut": "5000",
                    // };
                    setLoading(false);
                    // toastr["success"](data.message);
                    alert(data.message)
                    handleClose();
                }, 2000);
            },
            error: function (response) {
                // toastr.options = {
                //     "progressBar": true,
                //     "positionClass": "toast-top-center",
                //     "timeOut": "5000",
                // };
                var response = jQuery.parseJSON(response.responseText);
                if (response.statusCode === "400") {
                    var errMsg = new Array();
                    for (var variable in response.message) {
                        errMsg.push(response.message[variable]);
                    }
                    setLoading(false);
                    // toastr.error(errMsg.join('<br/>'));
                    alert(errMsg.join('<br/>'));
                } else {
                    setLoading(false);
                    // toastr.error(response.message);
                    alert(response.message);
                }
            }
        });
    };

    return (
        <div id='usersRequest'>
            <StylesProvider jss={jss}>
                <ThemeProvider theme={theme}>
                    {
                        !showForm
                        /*<Fab variant="extended" color="primary"
                             className={classes.button + ' animated heartBeat slower'}
                             style={{animationIterationCount: '2'}}
                             onClick={handleOpen}>
                            <span className='text d-none ml-md-2'>درخواست کلاسی دارید؟</span>
                            <LiveHelpIcon/>
                        </Fab>*/
                    }
                    <Modal
                        id='usersRequestModal'
                        aria-labelledby="transition-modal-title"
                        aria-describedby="transition-modal-description"
                        className={classes.modal}
                        open={showForm}
                        onClose={handleClose}
                        closeAfterTransition
                        BackdropComponent={Backdrop}
                        BackdropProps={{
                            timeout: 500,
                        }}
                    >
                        <Fade in={showForm}>
                            <div className={classes.paper}>
                                <div className={classes.root}>
                                    <div className='d-flex flex-row justify-content-between align-items-center'>
                                        <span>درخواست خود را برای ما ارسال کنید.</span>
                                        <IconButton onClick={handleClose}>
                                            <CloseIcon/>
                                        </IconButton>
                                    </div>
                                    <form
                                        id={'usersRequestForm'}
                                        className={classes.form}
                                        autoComplete="off"
                                    >
                                        <Grid container spacing={2} justify={'center'} alignItems={'center'}>
                                            <Grid item xs={12}>
                                                <TextField
                                                    className={classes.input}
                                                    variant="outlined"
                                                    fullWidth
                                                    name="title"
                                                    label="چه کلاسی؟"
                                                    size="small"
                                                    type="text"
                                                    // inputRef={register({required: true})}
                                                />
                                            </Grid>
                                            {
                                                (typeof token === "undefined")
                                                    ?
                                                    <>
                                                        <Grid item xs={6}>
                                                            <TextField
                                                                className={classes.input}
                                                                variant="outlined"
                                                                fullWidth
                                                                name="first_name"
                                                                label="نام"
                                                                size="small"
                                                                type="text"
                                                                // inputRef={register({required: true})}
                                                            />
                                                        </Grid>
                                                        < Grid item xs={6}>
                                                            <TextField
                                                                className={classes.input}
                                                                variant="outlined"
                                                                size="small"
                                                                fullWidth
                                                                name="last_name"
                                                                label="نام خانوادگی"
                                                                // inputRef={register({required: true})}
                                                            />
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <TextField
                                                                margin="none"
                                                                variant="outlined"
                                                                fullWidth
                                                                size="small"
                                                                name="mobile"
                                                                label="شماره همراه"
                                                                type="number"
                                                                // inputRef={register({required: true})}
                                                            />
                                                        </Grid>
                                                    </>
                                                    :
                                                    null
                                            }
                                            <Grid item xs={12}>
                                                <CitySelect required={true} func={handleCityId}/>
                                                <label className='d-none cityLabel text-right'
                                                       htmlFor='city_id'>این فیلد الزامی است</label>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    fullWidth
                                                    multiline
                                                    rows='2'
                                                    size="small"
                                                    variant="outlined"
                                                    label="توضیحات"
                                                    name="description"
                                                    // inputRef={register({required: true})}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                {
                                                    (loading)
                                                        ?
                                                        <Button className={classes.formButton} variant="outlined"
                                                                disabled>
                                                            <img className='ml-2'
                                                                 src={'/assets/images/loading.svg'}
                                                                 style={{'width': '5%', 'height': '36px'}}/>
                                                            صبرکنید
                                                        </Button>
                                                        :
                                                        <Button className={classes.formButton} variant="outlined"
                                                                type='button'
                                                                onClick={handleSubmit}
                                                        >
                                                            ارسال
                                                        </Button>
                                                }
                                            </Grid>
                                        </Grid>
                                    </form>
                                </div>
                            </div>
                        </Fade>
                    </Modal>
                </ThemeProvider>
            </StylesProvider>
        </div>
    );
}
