import React, {Component} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';



const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    tableWrapper: {
        maxHeight: '80vh',
        overflow: 'auto',
    },
    tableCell: {
        fontFamily: 'IRANSans',
        fontSize: '13px'

    },
    button: {
        fontFamily: 'IRANSans',
        fontSize: '13px'
    },
    showIcon: {
        marginRight: 10
    }
});

function createData(id, title, start_date, end_date, name, score_id) {
    return {id, title, start_date, end_date, name, score_id};
}


export default function StickyHeadTable(props) {
    const classes = useStyles();

    if (typeof props.data.school !== "undefined")
        var school = props.data.school.map(item => (
            createData(item.id, item.title, "null", "null", "null", "null")
        ));
    else
        var school = null;

    if (typeof props.data.course !== "undefined")
        var course = props.data.course.map(item => (
            createData(item.id, item.title, item.start_date, item.end_date, item.name, item.score_id)
        ));
    else
        var course = null;


    return (
        <Paper className={classes.root}>
            <div className={classes.tableWrapper}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.tableCell} align="center">عنوان</TableCell>
                            <TableCell className={classes.tableCell} align="center">عملیات</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(school !== null)
                            ?
                            school.map(row => (
                                <TableRow key={row.id}>
                                    <TableCell className={classes.tableCell} align='center'>{row.title}</TableCell>
                                    <TableCell className={classes.tableCell} align="center">
                                        <Button
                                            href={'/school/' + row.id}
                                            variant="contained"
                                            color="primary"
                                            className={classes.button}
                                        >
                                            صفحه آموزشگاه
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))
                            :
                            null
                        }
                        {
                            (course !== null)
                                ?
                                course.map(row => (
                                    <TableRow key={row.id}>
                                        <TableCell className={classes.tableCell} align='center'>{row.title}</TableCell>
                                        <TableCell className={classes.tableCell} align="center">
                                            <Button
                                                href={'/singlePage?id=' + row.id}
                                                variant="contained"
                                                color="primary"
                                                className={classes.button}
                                            >
                                                صفحه دوره
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))
                                :
                                null
                        }
                    </TableBody>
                </Table>
            </div>
        </Paper>
    );
}
