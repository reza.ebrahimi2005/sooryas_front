import React, {Component} from 'react';
import SwitchButton from './switch';
import Loading from './loading';

export default class UserSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            loading: false
        };
    }

    emailSwitchValue(value) {
        var data;
        var self = this;
        if (value)
            data = 1;
        else
            data = 0;
        this.setState({loading: true});
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL+'/awares/saveSetting',
            method: 'post',
            data: {'send_email': data},
            success: function (result) {
                self.setState({loading: false});
                if (result.status === 'success') {

                } else {

                }
            }
        });
    }

    smsSwitchValue(value) {
        var data;
        var self = this;
        if (value)
            data = 1;
        else
            data = 0;
        this.setState({loading: true});
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL+'/awares/saveSetting',
            method: 'post',
            data: {'send_sms': data},
            success: function (result) {
                self.setState({loading: false});
                if (result.status) {

                } else {

                }
            }
        });

    }

    notificationSwitchValue(value) {
        var data;
        var self = this;
        if (value)
            data = 1;
        else
            data = 0;
        this.setState({loading: true});
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL+'/awares/saveSetting',
            method: 'post',
            data: {'show_in_profile': data},
            success: function (result) {
                self.setState({loading: false});
                if (result.status) {

                } else {

                }
            }
        });

    }

    render() {
        return (
            <>
                {
                    (this.state.loading) && <Loading/>
                }
                <div className='row' id='userSetting'>
                    <div className='col-12 text-justify text-right'>
                        <span className='d-block mt-4'>کاربر گرامی از قسمت زیر می توانید راه های ارتباطی نیکارو برای اطلاع رسانی به شما را مشخص کنید: </span>
                        <form className='mt-2'>
                            {
                                (this.props.data != null)
                                    ?
                                    <>
                                        <div className='form-group'>
                                            <SwitchButton label={"ارسال ایمیل"} state={this.emailSwitchValue.bind(this)}
                                                          value={this.state.data.send_mail}/>
                                        </div>
                                        <div className='form-group'>
                                            <SwitchButton label={"ارسال پیامک"} state={this.smsSwitchValue.bind(this)}
                                                          value={this.state.data.send_sms}/>
                                        </div>
                                        <div className='form-group'>
                                            <SwitchButton label={"ارسال نوتیفیکیشن"}
                                                          state={this.notificationSwitchValue.bind(this)}
                                                          value={this.state.data.show_in_profile}/>
                                        </div>
                                    </>
                                    :
                                    <>
                                        <div className='form-group'>
                                            <SwitchButton label={"ارسال ایمیل"} state={this.emailSwitchValue.bind(this)}
                                                          value={1}/>
                                        </div>
                                        <div className='form-group'>
                                            <SwitchButton label={"ارسال پیامک"} state={this.smsSwitchValue.bind(this)}
                                                          value={1}/>
                                        </div>
                                        <div className='form-group'>
                                            <SwitchButton label={"ارسال نوتیفیکیشن"}
                                                          state={this.notificationSwitchValue.bind(this)}
                                                          value={1}/>
                                        </div>
                                    </>
                            }

                        </form>
                    </div>
                </div>
            </>
        );
    }
}
