import React, {createContext, useState, useContext, useEffect} from 'react'
import Cookies from 'js-cookie'
import Router, {useRouter} from 'next/router'

//api here is an axios instance
import api from '../api';

const AuthContext = createContext({});

export const AuthProvider = ({children}) => {

    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function loadUserFromCookies() {
            const token = Cookies.get('token')
            if (token) {
                console.log("Got a token in the cookies, let's see if it is valid")
                api.defaults.headers.Authorization = `Bearer ${token}`
                const user = await api.get(process.env.NEXT_PUBLIC_AUTH_API_URL+`/api/v3/user`)
                if (user) setUser(user.data);
            }
            setLoading(false)
        }
        loadUserFromCookies()
    }, [])

    const login = async (username, password) => {
        const result = await api.post(process.env.NEXT_PUBLIC_AUTH_API_URL+'api/v3/login', {username, password});
        const token = result.data.data;

        if (token) {
            console.log("Got token")
            Cookies.set('token', token.token, {expires: 60})
            Cookies.set('uid', token.userData, {expires: 60})
            setUser(token.userData)
        }
    }

    const logout = () => {
        Cookies.remove('token')
        Cookies.remove('uid')
        setUser(null)
        window.location.pathname = '/'
    }


    return (
        <AuthContext.Provider value={{isAuthenticated: !!user, user, login, loading, logout}}>
            {children}
        </AuthContext.Provider>
    )
}

export default function useAuth() {
    const context = useContext(AuthContext)
    return context
};

export function ProtectRoute(Component) {
    return () => {
        const {user, isAuthenticated, loading} = useAuth();
        const router = useRouter()

        useEffect(() => {
            if (!isAuthenticated && !loading) {
                alert('برای مشاهده این صغحه باید وارد شوید');
                router.push('/');
            }
        }, [loading, isAuthenticated])

        return (<Component {...arguments} />)
    }
}
