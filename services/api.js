import Axios from "axios";

let urls = {
    development: 'https://panel.nikaro.ir',
    production: 'https://panel.nikaro.ir'
}
const api = Axios.create({
    baseURL: urls[process.env.NODE_ENV],
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
});

export default api;
``
