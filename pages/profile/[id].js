import React, {Component} from 'react';
import {ProtectRoute} from '../../services/contexts/AuthContext'
import EditUserInfo from '../../components/editUserInfo';
import UserCourse from '../../components/userCourses';
import UserWallet from '../../components/userWallet';
import InviteFriends from '../../components/inviteFriends';
import Payments from '../../components/payments';
import UserSurvey from '../../components/userSurvey';
import UserSetting from '../../components/userSetting';
import UserAwares from '../../components/userAwares';
import Inbox from '../../components/messages/Inbox';
import Loading from "../../components/loading";
import useAuth from "../../services/contexts/AuthContext";
import axios from 'axios';
import Cookies from "js-cookie";
import Head from "next/head";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSignOutAlt,faUser,faAngleLeft,faBookmark,faChartBar,faWallet,faEnvelope,faCheckSquare,faHeart,faRss} from '@fortawesome/free-solid-svg-icons'
import Cookies from "js-cookie";
import {ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function ProfileWithAuth(props) {
    return <Profile {...props} authData={useAuth()}/>
}

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: props.authData.user,
            content: <EditUserInfo data={null}/>,
            activeIndex: 0,
            screenSize: null,
            showMenu: true,
            mobileTitle: null,
            data: []
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.authData.user !== this.state.user) {
            const token = Cookies.get('token');
            const uid = Cookies.get('uid');
            axios.defaults.headers.Authorization = `Bearer ${token}`;
            axios.post(process.env.NEXT_PUBLIC_API_URL+'/profile/' + nextProps.authData.user.id, {'userId': uid}).then((response) => {
                this.setState({
                    data: response.data,
                    content: <EditUserInfo data={response.data}/>,
                    screenSize: (window.innerWidth > 768) ? "desktop" : "mobile",
                });
            });
        }
    }

    loadEditUserInfo(index) {
        this.setState({
            content: <EditUserInfo data={this.state.data}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "اطلاعات کاربری"
        })
    }

    loadUserCourse(index) {
        this.setState({
            content: <UserCourse data={this.state.data.courses_data}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "دوره های من"
        })
    }

    loadPayments(index) {
        this.setState({
            content: <Payments data={this.state.data.paymentData}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "اطلاعات مالی"
        })
    }

    loadUserWallet(index) {
        this.setState({
            content: <UserWallet data={this.state.data}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "کیف پول نیکارو"
        })
    }

    loadInviteFriends(index) {
        this.setState({
            content: <InviteFriends data={this.state.data.referralCode}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "دعوت از دوستان"
        })
    }

    loadUserSurvey(index) {
        this.setState({
            content: <UserSurvey data={this.state.data.courses_data}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "نظرسنجی"
        })
    }

    loadUserSetting(index) {
        this.setState({
            content: <UserSetting data={this.state.data.awareSetting}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "اطلاع رسانی"
        })
    }

    loadUserAwares(index) {
        this.setState({
            content: <UserAwares data={this.state.data.userAwares}/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "علاقه مندی ها"
        })
    }

    loadInbox(index) {
        this.setState({
            content: <Inbox/>,
            activeIndex: index,
            showMenu: (this.state.screenSize !== "mobile"),
            mobileTitle: "پیام ها"
        })
    }

    loadMenu() {
        this.setState({
            showMenu: true
        })
    }

    render() {
        var content;
        {
            (typeof this.state.data.id != "undefined")
                ?
                content =
                    <>
                        <div
                            className={(this.state.showMenu === true) ? 'col-12 col-md-3 profileMenu sticky-top' : 'col-12 col-md-3 profileMenu d-none'}
                            style={{'top': '8vh', 'z-index': '99'}}>
                            <div className="row">
                                <div
                                    className="col-12 userInfo d-flex justify-content-md-start justify-content-center align-items-center py-2">
                                    <img
                                        src={(this.state.data.userPic != null) ? this.state.data.userPic : '/assets/images/avatar.png'}
                                        className="d-inline-block rounded-circle align-middle"/>
                                    <div className="d-inline-block mr-4">
                                <span
                                    className="userName d-block text-black bold">{this.state.data.first_name + " " + this.state.data.last_name}</span>
                                        <a href="/logout" className="text-small">
                                            <FontAwesomeIcon icon={faSignOutAlt} className='ml-1 align-middle'/>
                                            <span>خروج کاربر</span>
                                        </a>
                                    </div>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 0 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadEditUserInfo.bind(this, 0)}>
                                    <span><FontAwesomeIcon icon={faUser} className='ml-2'/>اطلاعات کاربری</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 1 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadUserCourse.bind(this, 1)}>
                                    <span><FontAwesomeIcon icon={faBookmark} className='ml-2'/>دوره های من</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 2 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadPayments.bind(this, 2)}>
                                    <span><FontAwesomeIcon icon={faChartBar} className='ml-2'/>اطلاعات مالی</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 3 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadUserWallet.bind(this, 3)}>
                                    <span><FontAwesomeIcon icon={faWallet} className='ml-2'/>کیف پول</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 4 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadInviteFriends.bind(this, 4)}>
                                    <span><FontAwesomeIcon icon={faEnvelope} className='ml-2'/>دعوت دوستان</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 5 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadUserSurvey.bind(this, 5)}>
                                    <span><FontAwesomeIcon icon={faCheckSquare} className='ml-2'/>نظرسنجی</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 6 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadUserSetting.bind(this, 6)}>
                                    <span><FontAwesomeIcon icon={faRss} className='ml-2'/>اطلاع رسانی</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 7 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadUserAwares.bind(this, 7)}>
                                    <span><FontAwesomeIcon icon={faHeart} className='ml-2'/>علاقه مندی ها</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                                <div
                                    className={(this.state.activeIndex === 8 && this.state.screenSize === "desktop") ? "col-12 d-flex justify-content-between p-3 text-black" : "col-12 d-flex justify-content-between p-3"}
                                    onClick={this.loadInbox.bind(this, 8)}>
                                    <span><FontAwesomeIcon icon={faEnvelope} className='ml-2'/>پیام ها</span>
                                    <FontAwesomeIcon icon={faAngleLeft}/>
                                </div>
                            </div>
                        </div>
                        <div
                            className={(this.state.screenSize === "mobile" && this.state.showMenu === true) ? 'd-none' : 'col-md-9 col-12 profileContent pb-5'}>
                            <div className="row d-md-none mt-4">
                                <div className="col-12">
                                    <span className="text-black float-right">{this.state.mobileTitle}</span>
                                    <a className="text-black float-left text-primary"
                                       onClick={this.loadMenu.bind(this)}>بازگشت<i
                                        className="fa fa-arrow-left mr-2"/></a>
                                </div>
                            </div>
                            {this.state.content}
                        </div>
                    </>
                :
                content =
                    <>
                        <div
                            className={(this.state.screenSize === "mobile" && this.state.showMenu === true) ? 'd-none' : 'col-md-9 col-12 profileContent pb-5'}>
                        </div>
                        <Loading/>
                        <ToastContainer />
                    </>;

        }
        return (
            <>
                <Head>
                    <title>پروفایل کاربری | نیکارو</title>
                    <meta name="robots" content="noindex"/>
                </Head>
                <div className='row' id='profile'>
                    {content}
                </div>
                <ToastContainer />
            </>
        );
    }
}

export default ProtectRoute(ProfileWithAuth)
