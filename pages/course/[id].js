import React, {Component} from 'react';
import axios from 'axios';
import CoursePageGalleryCarousel from "../../components/coursePageGallery-carousel";
import OtherSchoolsCarousel from "../../components/otherSchools-carousel";
import SchoolPageCoursesCarousel from "../../components/schoolPageCourses-carousel";
import FinalRegister from "../../components/finalRegister";
import Rating from "react-rating";
import InstagramIcon from '@material-ui/icons/Instagram';
import TelegramIcon from '@material-ui/icons/Telegram';
import CitySelect from '../../components/city-select';
import DateRangeIcon from '@material-ui/icons/DateRange';
import ContactsIcon from '@material-ui/icons/Contacts';
import TodayIcon from '@material-ui/icons/Today';
import AlarmIcon from '@material-ui/icons/Alarm';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import Divider from '@material-ui/core/Divider';
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Paper from "@material-ui/core/Paper";
import Avatar from '@material-ui/core/Avatar';
import CountDownTimer from '../../components/countDownTimer'
import LinkIcon from '@material-ui/icons/Link';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import {motion} from "framer-motion";
import Loading from "../../components/loading";
import {useRouter} from 'next/router';
import $ from 'jquery'
import Head from "next/head";
import Cookies from "js-cookie";
import api from "../../services/api";
import {ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export async function getServerSideProps(context) {
    // Fetch data from external API
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + '/search?courseId=' + context.query.id);
    const data = await res.json();
    // Pass data to the page via props
    return {props: {"data": data}}
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function usersActivitySave(){

    const token = Cookies.get('token');
    $.ajax({
        url: process.env.NEXT_PUBLIC_API_URL + '/usersTractiongActivity/save',
        type: "GET",
        processData: false,
        contentType: false,

        headers: {
            "Authorization": `Bearer ${token}`,
            "Access-Control-Allow-Headers": "*"
        },
        data: {'subject': 'course', 'url':window.location.href,'pathName':window.location.pathname},
        success: function (result) {
            alert("hhhhhhhhh");
        }
    });


}
/* Function to animate height: auto */
function autoHeightAnimate(element, time) {
    var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
    element.height(curHeight); // Reset to Default Height
    element.stop().animate({height: autoHeight}, time); // Animate to Auto Height
}

function splitString(str, s) {
    if (str !== null) {
        var temp = str.split('-');
        var middle = Math.ceil(temp[0].length / 2);
        var s1 = temp[0].slice(0, middle);
        var s2 = temp[0].slice(middle);
        for (var i = 1; i < temp.length; i++) {
            s2 += '-' + temp[i];
        }
    }

    if (s === 1) {
        return s1;
    } else if (s === 2) {
        return s2;
    }
}

async function getUserData() {
    const token = Cookies.get('token')
    api.defaults.headers.Authorization = `Bearer ${token}`;
    const user = await api.get(process.env.NEXT_PUBLIC_AUTH_API_URL+`/api/v3/user`);
    return user.data;
}

export default class Course extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobile: false,
            courseId: props.query.id,
            user_id: Cookies.get('uid'),
            user: null,
            data: props.data[0][0],
            errorMsg: false,
            targetModal: '#loginRegisterModal',
            cityId: false,
            loading: false,
            isAware: false,
            value: 0,
            videos: null,
            isLogin: false,
            awareIcon: <FavoriteBorderIcon className='text-danger ml-2'/>,
            courseGallery: <CoursePageGalleryCarousel id={props.query.id}/>,
            showMoreBtn: <span className='d-block text-primary text-center moreBtn bg-white'
                               style={{'cursor': 'pointer'}}
                               onClick={this.handleScrollToDesc.bind(this)}>
                                بیشتر
                                <KeyboardArrowDownIcon className='text-primary mr-2'/>
                        </span>,

        };
    }

    componentDidMount() {
        this.setState({
            isMobile: (window.innerWidth <= 575)
        })


        if (typeof this.state.data !== "undefined") {
            if (this.state.data.isAware) {
                this.setState({
                    awareIcon: <FavoriteIcon className='text-danger ml-2'/>
                })
            }
        } else {
            this.setState({
                data: {id: '404'},
            })
        }

        if (typeof this.state.user_id !== "undefined") {
            getUserData().then(r => {
                    this.setState({user: r});
                    if (this.state.data.registerStatus === "inactive") {
                        this.setState({
                            errorMsg: 'مهلت ثبت نام به پایان رسیده است'
                        })
                    } else {
                        if (this.state.data.maximum_participant > 0) {
                            if (r.profile_is_complete) {
                                this.setState({
                                    errorMsg: '',
                                    targetModal: '#finalRegisterModal'
                                })
                                if (window.location.hash === '#courseRegister') {
                                    $('#registerBtn').trigger('click');
                                }
                            } else {
                                this.setState({
                                    errorMsg: '',
                                    targetModal: '#completeProfileModal'
                                })
                            }
                        } else {
                            this.setState({
                                errorMsg: 'ظرفیت کلاس به پایان رسیده است'
                            })
                        }
                    }
                }
            );

        }
        //send user tracking activity
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/usersTractiongActivity/save',
            type: "POST",
            data: {'subject': 'course', 'url':window.location.href,'pathName':window.location.pathname},
            success: function (result) {

            }
        });

    }

    handleAwareHoverIn() {
        if (this.state.data.isAware === 0 && !this.state.isAware) {
            this.setState({
                awareIcon: <FavoriteIcon className='text-danger ml-2'/>
            })
        }
    }

    handleAwareHoverOut() {
        if (this.state.data.isAware === 0 && !this.state.isAware) {
            this.setState({
                awareIcon: <FavoriteBorderIcon className='text-danger ml-2'/>
            })
        }
    }

    handleOpenCompleteProfileModal() {
        $('#completeProfileModal').modal('show');
    }

    showLostNumber() {
        if (typeof this.state.user_id == "undefined") {
            $('.showPhoneBtn').hide();
            $('.phone').find('.lostData').text($('.phone').data('lost'));
        } else {

            toast.error("برای مشاهده این قسمت ابتدا باید وارد شوید", {
                position: toast.POSITION.TOP_CENTER
            })
            $('#loginRegisterModal').modal('show');
        }
    }

    saveAware() {

        if (typeof this.state.user_id == "undefined") {

            toast.error("ابتدا باید وارد شوید", {
                position: toast.POSITION.TOP_CENTER
            })
           // $('#loginRegisterModal').modal('show');
        } else {
            const token = Cookies.get('token')
            axios({
                method: 'post',
                url: process.env.NEXT_PUBLIC_API_URL+'/awares/save',
                headers: {
                    "Authorization": `Bearer ${token}`
                },
                data: {
                    'type': 'course',
                    'id': this.state.courseId,
                }
            }).then(function (response) {

                toast.success(response.data.message, {
                    position: toast.POSITION.TOP_CENTER
                });
                this.setState({
                    isAware: true,
                    awareIcon: <FavoriteIcon className='text-danger ml-2'/>
                })
            }).catch(function (error) {
                if (error.status === 401) {
                    toast.error("مشکلی پیش آمد",{
                        position: toast.POSITION.TOP_CENTER
                    })
                }
            });
        }

    }

    showLoginMessage() {
        if ($('.registerBtn').attr('data-target') === '#loginRegisterModal') {
            toast.error("برای ثبت نام ابتدا باید وارد شوید",{
                position: toast.POSITION.TOP_CENTER
            })
        }
    }

    cityId(value) {
        this.setState({
            cityId: value
        })
    }

    submitCompleteProfileForm(event) {
        event.preventDefault();
        var self = this;
        var form = $('#completeProfileForm');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);

        // $.validator.addMethod("regex", function (value, element, regexpr) {
        //     return regexpr.test(value);
        // }, "");
        //
        // $('#completeProfileForm').validate({
        //     debug: true,
        //     doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        //     errorElement: 'span', //default input error message container
        //     errorClass: 'help-block help-block-error', // default input error message class
        //     focusInvalid: false, // do not focus the last invalid input
        //     rules: {
        //         //account
        //         first_name: {
        //             required: true,
        //         },
        //         last_name: {
        //             required: true,
        //         },
        //
        //     },
        //
        //     messages: {
        //         last_name: {
        //             required: 'فیلد فامیل را وارد نمایید'
        //         },
        //         first_name: {
        //             required: 'فیلد نام را وارد نمایید'
        //         },
        //     },
        //
        //     invalidHandler: function (event, validator) { //display error alert on form submit
        //         success.hide();
        //         error.show();
        //     },
        //
        //     errorPlacement: function (error, element) { // render error placement for each input type
        //         error.insertAfter(element);
        //     },
        //
        //     highlight: function (element) { // hightlight error inputs
        //         $(element)
        //             .closest('.form-group').addClass('has-error'); // set error class to the control group
        //     },
        //
        //     unhighlight: function (element) { // revert the change done by hightlight
        //         $(element)
        //             .closest('.form-group').removeClass('has-error'); // set error class to the control group
        //     },
        //
        //     success: function (label) {
        //         label.closest('.form-group').removeClass('has-error'); // set success class to the control group
        //     }
        // });

        var formdata = new FormData($('#completeProfileForm')[0]);
        self.setState({
            loading: true
        });
        window.location.hash = 'courseRegister'
        const token = Cookies.get('token')
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/saveEditedData',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            headers: {
                "Authorization": `Bearer ${token}`
            },
            success: function (result) {
                if (result.status === 'success') {
                    alert(result.message + ' | لطفا تا بارگزاری مجدد صفحه صبر کنید');
                    self.setState({
                        loading: false
                    });
                    window.location.reload();
                } else if (result.status === 'failed') {
                    alert(result.message);
                    self.setState({
                        loading: false
                    });
                }
            },
        });
    }

    handleTabChange(event, newValue) {
        this.setState({
            value: newValue
        });
    }

    renderTabSwitch(param) {
        switch (param) {
            case 0:
                return (
                    <div className='school row'>
                        <div className='col-md-8 col-12 d-flex flex-row justify-content-start pt-5'>
                            <div className='row' style={{'width': '100%'}}>
                                <div className='col-12 col-md-5 text-center'>
                                    <Avatar alt="Remy Sharp" src={this.state.data.schoolLogo}
                                            className='schoolAvatar mb-2 mx-auto'/>
                                    <strong className='d-block mb-2'>{this.state.data.schoolTitle}</strong>
                                    <span className='m-2'>
                                        {
                                            (this.state.data.schoolInstagram !== "")
                                                ?
                                                <a href={'https://instagram.com/' + this.state.data.schoolInstagram}>
                                                    <InstagramIcon className='text-primary'/>
                                                </a>
                                                :
                                                <InstagramIcon className='text-dark'/>
                                        }
                                    </span>
                                    <span className='m-2'>
                                        {
                                            (this.state.data.schoolTelegram !== "")
                                                ?
                                                <a href={'https://t.me/' + this.state.data.schoolTelegram}>
                                                    <TelegramIcon className='text-primary'/>
                                                </a>
                                                :
                                                <TelegramIcon className='text-dark'/>
                                        }
                                    </span>
                                </div>
                                <div className='col-12 col-md-6 aboutTeacher text-right'>
                                    <span className='d-block text-primary mb-2'>درباره آموزشگاه:</span>
                                    <div style={{'fontFamily': 'IRANSans'}} className='text-right'
                                       dangerouslySetInnerHTML={{__html: this.state.data.schoolDesc}}/>
                                </div>
                            </div>
                        </div>
                        <div
                            className='schoolAddress col-md-4 col-12 d-flex flex-column justify-content-between text-right pt-5'>
                            {
                                (this.state.data.schoolRegion !== null)
                                    ?
                                    <>
                                    <span>
                                        <span className='text-primary ml-2'>ناحیه شهری:</span>
                                        <span>{this.state.data.schoolRegion}</span>
                                    </span>
                                        <Divider/>
                                    </>
                                    :
                                    null
                            }

                            <span>
                                <span className='text-primary ml-2'>آدرس:</span>
                                {
                                    (this.state.isLogin)
                                        ?
                                        <span>{this.state.data.schoolAddress}</span>
                                        :
                                        <span className='text-primary' style={{'cursor': 'pointer'}} data-toggle='modal'
                                              data-target='#loginRegisterModal'> (نمایش) </span>
                                }

                            </span>
                            <Divider/>
                            <span>
                                <span className='text-primary ml-2'>تلفن:</span>
                                <span className='phone' data-lost={splitString(this.state.data.schoolPhone, 2)}>
                                    {splitString(this.state.data.schoolPhone, 1)}
                                    <span className='lostData'>******</span>
                                </span>
                                <span onClick={this.showLostNumber.bind(this)} className='text-primary showPhoneBtn'
                                      style={{'cursor': 'pointer'}}>
                                    (نمایش کامل)
                                </span>
                            </span>
                            <Divider/>
                            <span>
                                <span className='text-primary ml-2'>وب سایت:</span>
                                {
                                    (this.state.data.schoolWebsite !== null)
                                        ?
                                        <a href={this.state.data.schoolWebsite}>{this.state.data.schoolWebsite}</a>
                                        :
                                        <a href={'/' + this.state.data.schoolShortLink}>{'www.nikaro.ir/' + this.state.data.schoolShortLink}</a>
                                }
                            </span>
                            <Divider/>
                            <Button variant='outlined' className='text-primary border-primary rounded-pill'
                                    style={{'fontFamily': 'IRANSans'}} href={'/school/'+this.state.data.schoolId}>صفحه
                                آموزشگاه</Button>
                        </div>
                    </div>
                );
            case 1:
                var data = this.state.data.teachers.map(function (e) {
                    return (
                        <div className='teacher row'>
                            <div className='col-md-8 col-12 pt-5'>
                                <div className='row' style={{'width': '100%'}}>
                                    <div className='col-12 col-md-4 text-center'>
                                        <Avatar alt="Remy Sharp" src={e.pic}
                                                className='teacherAvatar mb-2 mx-auto'/>
                                        <strong className='d-block text-nowrap'>{e.full_name}</strong>
                                    </div>
                                    <div className='col-12 col-md-8 aboutTeacher text-right'>
                                        <span className='d-block text-primary mb-2'>درباره مدرس:</span>
                                        <span className='text-justify'>
                                        <p dangerouslySetInnerHTML={{__html: e.ability}}/>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div className='col-4'/>
                        </div>
                    )
                })

                return data;
            case 2:
                return this.state.courseGallery;
            case 3:
                return (
                    <span
                        className='col-12 col-md-4 mx-auto mt-5 d-block bg-primary text-white rounded text-center p-2'>در حال آماده سازی...</span>
                );
            case 4:
                return (
                    <div className='row justify-content-center align-items-center py-5'>
                        <Button href={'/resultPage?tags=' + this.state.data.title}
                                className='col border border-dark p-2 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{this.state.data.title}</Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'بهترین کلاس ' + this.state.data.categoriesTitle}</Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'بهترین کلاس ' + this.state.data.categoriesTitle + ' در ' + this.state.data.schoolCity}
                        </Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاسهای ' + this.state.data.categoriesTitle + ' مجموعه ' + this.state.data.schoolTitle}
                        </Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاسهای  ' + this.state.data.categoriesTitle + ' مخصوص نوجوانان'}
                        </Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاسهای  ' + this.state.data.categoriesTitle + ' مخصوص بزرگسالان'}
                        </Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' کودک'}</Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' کودک در ' + this.state.data.schoolCity}
                        </Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' با متد جدید '}</Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' ' + this.state.data.course_total_sessions + ' جلسه ای در' + this.state.data.schoolCity}
                        </Button>
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' '}</Button>
                        {
                            (this.state.data.gender === '00020003')
                                ?
                                <>
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' مخصوص پسران'}
                                    </Button>
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' مخصوص دختران'}
                                    </Button>
                                </>
                                :
                                (this.state.data.gender === '00020001')
                                    ?
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' مخصوص پسران'}
                                    </Button>
                                    :
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' مخصوص دختران'}
                                    </Button>
                        }
                        <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                            className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' با ارائه مدرک'}
                        </Button>
                        {
                            (this.state.data.amountWithDiscount < 200)
                                ?
                                <>
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' با قیمت مناسب'}
                                    </Button>
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'کلاس  ' + this.state.data.categoriesTitle + ' ارزان تر'}
                                    </Button>
                                    <Button href={'/resultPage?tags=' + this.state.data.categoriesTitle}
                                            className='col border border-dark p-2 px-4 mx-2 rounded-pill text-nowrap mt-2'
                                            style={{'fontFamily': 'IRANSans', 'minWidth': 'auto'}}><LinkIcon
                                        className='ml-1'/>{'ارزان ترین کلاس  ' + this.state.data.categoriesTitle}
                                    </Button>
                                </>
                                :
                                null
                        }
                    </div>
                )
        }
    }

    handleScrollToDesc() {
        var animateTime = 500;
        var nav = $('.shortInfo');
        if (nav.height() === 200) {
            autoHeightAnimate(nav, animateTime);
            this.setState({
                showMoreBtn: <span className='d-block text-primary text-center moreBtn bg-white'
                                   style={{'cursor': 'pointer'}}
                                   onClick={this.handleScrollToDesc.bind(this)}>
                                کمتر
                                <KeyboardArrowUpIcon className='text-primary mr-2'/>
                        </span>
            })
        } else {
            nav.stop().animate({height: '200'}, animateTime);
            this.setState({
                showMoreBtn: <span className='d-block text-primary text-center moreBtn bg-white'
                                   style={{'cursor': 'pointer'}}
                                   onClick={this.handleScrollToDesc.bind(this)}>
                                بیشتر
                                <KeyboardArrowDownIcon className='text-primary mr-2'/>
                        </span>
            })
        }
        window.scrollTo({
            top: $('.tab').offset().top - 50,
            behavior: 'smooth'
        });
        $('.tab .MuiTabs-flexContainer button:nth-child(3)').trigger('click');
    }

    render() {
        let carousel;
        let content;
        let desc;
        if (this.state.data.description != null) {
            desc = this.state.data.description.replace(/<[^>]+>|&nbsp;|&ndash;|&amp;|zwnj;/g, '').replace(/^\s*[\r\n]/gm, '');
        } else {
            desc = 'در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید.';
        }

        if (this.state.data.id) {
            {
                (this.state.data.haveOtherCourses)
                    ?
                    carousel = <SchoolPageCoursesCarousel schoolId={this.state.data.schoolId}/>
                    :
                    carousel = <OtherSchoolsCarousel schoolId={this.state.data.schoolId}
                                                     categoryId={this.state.data.categoriesId}/>
            }
            content =
                <div id={'coursePage'}>
                    <div className='headerBanner row'>
                        <img className='header-bg' src={this.state.data.coursePic.original[0].image}
                             alt={this.state.data.title}
                             title={this.state.data.title}/>
                        <img className='header-bg-1' src={this.state.data.coursePic.original[0].image}
                             alt={this.state.data.title} title={this.state.data.title}/>
                        {
                            (this.state.data.installments.length > 0)
                                ?
                                <div className='installmentIcon p-1 p-md-4 text-left'>
                                    <BorderColorIcon className='text-white ml-2'/>
                                    <span className='text-white'>امکان پرداخت قسطی</span>
                                </div>
                                :
                                null
                        }
                        <div className='title-rate text-right p-md-4 p-2'>
                            <h1 className='h4 d-block text-white'>{this.state.data.title}</h1>
                            <Rating
                                className='rating text-white'
                                emptySymbol="fa fa-star-o"
                                fullSymbol="fa fa-star"
                                placeholderSymbol="fa fa-star"
                                fractions={2}
                                readonly={true}
                                placeholderRating={(this.state.data.avg_rate != null) ? this.state.data.avg_rate : 3.5}
                            />

                        </div>
                    </div>
                    <div className='row shadow info mb-4 py-2'>
                        <div
                            className='col-6 col-md-3 border-left bg-white startData d-flex flex-column justify-content-center align-items-center'>
                            <DateRangeIcon className='text-primary icon mb-1'/>
                            <span className='d-block text-dark mb-1 text-nowrap'>تاریخ شروع دوره:</span>
                            <h5 className='d-block'>{this.state.data.course_start_date}</h5>
                        </div>
                        <div
                            className='col-6 col-md-3 border-left bg-white category d-flex flex-column justify-content-center align-items-center'>
                            <ContactsIcon className='text-primary icon mb-1'/>
                            <span className='d-block text-dark mb-1 text-nowrap'>تعداد جلسات:</span>
                            <h5 className='d-block text-nowrap'>{this.state.data.course_total_sessions}</h5>
                        </div>
                        <div
                            className='col-6 col-md-3 border-left bg-white days d-flex flex-column justify-content-center align-items-center'>
                            <TodayIcon className='text-primary icon mb-1'/>
                            <span className='d-block text-dark mb-1 text-nowrap'>روزهای برگزاری:</span>
                            {
                                (this.state.data.formationDaysWeek.id === 1)
                                    ?
                                    <h5 className='d-block text-nowrap'>{this.state.data.formationDays}</h5>
                                    :
                                    <h5 className='d-block text-nowrap'>{this.state.data.formationDaysWeek.title}</h5>
                            }
                        </div>
                        <div
                            className='col-6 col-md-3 border-left bg-white time d-flex flex-column justify-content-center align-items-center'>
                            <AlarmIcon className='text-primary icon mb-1'/>
                            <span className='d-block text-dark mb-1 text-nowrap'>زمان برگزاری:</span>
                            <h5 className='d-block text-nowrap'>{this.state.data.course_formation_start_time + ' الی ' + this.state.data.course_formation_end_time}</h5>
                        </div>
                    </div>
                    <div className='row mb-4 justify-content-between'>
                        <div className='col-12 col-md-4 text-right px-1 order-1 order-md-1'>
                            <span
                                className='d-flex justify-content-between align-items-center border border-dark remainingTime'>
                                <span className='ml-2 text-nowrap'>مهلت ثبت نام: </span>
                                <div className='d-flex flex-column'>
                                    <CountDownTimer date={this.state.data.registerEndDate}/>
                                    <div className='label text-center'>
                                        <span className='counterText px-1 d-inline-block'>ثانیه</span>
                                        <span className='counterText px-1 d-inline-block'>دقیقه</span>
                                        <span className='counterText px-1 d-inline-block'>ساعت</span>
                                        <span className='counterText px-1 d-inline-block'>روز</span>
                                    </div>
                                </div>
                            </span>
                        </div>
                        {
                            (this.state.data.is_virtual)
                                ?
                                <div className='col-12 col-md-4 order-2 order-md-2 mt-3 mt-md-0'>
                                    <span className='text-center text-white bg-success p-2 d-block rounded'>این دوره به صورت مجازی برگزار میشود</span>
                                </div>
                                :
                                null
                        }

                        <div className='col-12 col-md-4 text-left px-1 order-3 order-md-3'>
                            <span
                                onClick={this.saveAware.bind(this)}
                                onMouseEnter={this.handleAwareHoverIn.bind(this)}
                                onMouseLeave={this.handleAwareHoverOut.bind(this)}
                                className='addAwareBtn d-flex justify-content-center align-items-center border border-secondary text-nowrap float-md-left'>
                                {this.state.awareIcon}
                                افزودن به مورد علاقه ها
                            </span>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-6 col-12 shortInfo text-right pl-md-5'>
                            <strong className='d-block text-primary mb-2'>توضیحات دوره:</strong>
                            <div dangerouslySetInnerHTML={{__html: this.state.data.description}}
                               style={{'fontFamily': 'IRANSans'}} className='text-justify'/>
                            {this.state.showMoreBtn}
                        </div>
                        <div
                            className='col-md-6 col-12 paymentInfo d-flex flex-column justify-content-between text-right pr-6'>
                            <span className='d-block'>
                                <span className='text-primary ml-2'>نام آموزشگاه:</span>
                                <span>{this.state.data.schoolTitle}</span>
                            </span>
                            <Divider/>
                            <span className='d-block'>
                                <span className='text-primary ml-2'>امکان پرداخت قسطی:</span>
                                {
                                    (this.state.data.installments.length > 0)
                                        ?
                                        <span>دارد</span>
                                        :
                                        <span>ندارد</span>
                                }

                            </span>
                            <Divider/>
                            <span className='d-flex flex-row justify-content-between'>
                                <div>
                                    <span className='text-primary d-block'>قیمت دوره:</span>
                                    <span>{(this.state.data.total_amount === "free") ? 'رایگان' : numberWithCommas(this.state.data.total_amount) + 'تومان'}</span>
                                </div>
                                {
                                    (this.state.data.discount_percent > 0)
                                        ?
                                        <div className='text-center'>
                                            <span className='text-primary d-block'>تخفیف:</span>
                                            <span
                                                className='text-danger'>% {this.state.data.discount_percent}</span>
                                        </div>
                                        :
                                        null
                                }
                                <div className='text-center'>
                                    <span className='text-primary d-block'>قابل پرداخت:</span>
                                    <h4>{(this.state.data.amountWithDiscount === 0) ? 'رایگان' : numberWithCommas(this.state.data.amountWithDiscount) + ' تومان'}</h4>
                                </div>
                            </span>
                            <Button id='registerBtn'
                                    className='bg-primary text-white rounded-pill'
                                    style={{'fontFamily': 'IRANSans'}}
                                    disabled={!!(this.state.errorMsg && this.state.errorMsg !== '')}
                                    data-toggle='modal'
                                    data-target={this.state.targetModal}>ثبت نام</Button>
                            {this.state.errorMsg !== '' &&
                            <span className='text-danger d-block text-center'>{this.state.errorMsg}</span>}

                        </div>
                    </div>
                    <Divider className='my-4'/>
                    <Paper className='col-12 p-0 px-md-2 tab'>
                        <Tabs
                            value={this.state.value}
                            onChange={this.handleTabChange.bind(this)}
                            indicatorColor="primary"
                            variant="scrollable"
                            scrollButtons="on"
                        >
                            <Tab label="درباره آموزشگاه" style={{'width': '20%', 'fontFamily': 'IRANSans'}}
                                 className='text-primary'/>
                            <Tab label="درباره مدرس" style={{'width': '20%', 'fontFamily': 'IRANSans'}}
                                 className='text-primary'/>
                            <Tab label="تصاویر دوره" style={{'width': '20%', 'fontFamily': 'IRANSans'}}
                                 className='text-primary'/>
                            <Tab label="پرسش و پاسخ" style={{'width': '20%', 'fontFamily': 'IRANSans'}}
                                 className='text-primary'/>
                            <Tab label="برچسب ها" style={{'width': '20%', 'fontFamily': 'IRANSans'}}
                                 className='text-primary'/>
                        </Tabs>
                    </Paper>
                    <div className='tabContent mb-5'>
                        {this.renderTabSwitch(this.state.value)}
                    </div>
                    <div className='row courseSchool-carousel my-5'>
                        {carousel}
                    </div>
                    <FinalRegister data={this.state.data}/>
                    <div className="modal fade" id="completeProfileModal" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style={{'padding': '0'}}>
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title text-center" id="exampleModalLongTitle">
                                        تکمیل اطلاعات کاربری
                                        <hr/>
                                    </h5>
                                    <button type="button" className="close float-left" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <span className='d-block text-danger text-right text-nowrap'>اطلاعات کاربری شما برای ثبت نام دوره کامل نمی باشد. لطفا اطلاعات زیر را وارد کنید.</span>
                                    <form id={'completeProfileForm'}
                                          onSubmit={this.submitCompleteProfileForm.bind(this)}>
                                        <div className='form-group'>
                                            <input className='form-control' type="text" placeholder='نام'
                                                   name='first_name'/>
                                            <input className='form-control' type="text" placeholder='نام خانوداگی'
                                                   name='last_name'/>
                                            <CitySelect func={this.cityId.bind(this)}/>
                                        </div>
                                        {
                                            (this.state.loading)
                                                ?
                                                <button className='btn btn-outline-primary' disabled>
                                                    <img className='ml-2' src='/assets/images/loading.svg'
                                                         style={{'width': '5%'}}/>
                                                </button>
                                                :
                                                <button className='btn btn-outline-primary' type='submit'>ارسال</button>
                                        }
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ToastContainer />
                </div>
        } else if (this.state.data.id !== "404") {
            content =
                <div id={'coursePage'}>
                    <Loading/>
                </div>;
        }
        return (
            <>
                <Head>
                    <title>{this.state.data.schoolTitle + ' | ' + this.state.data.title}</title>
                    <meta name="description" property="description" content={desc}/>
                    <meta property="og:url" content={"https://nikaro.ir" + this.props.asPath}/>
                    <meta property="og:title" content={this.state.data.title}/>
                    <meta property="og:description" content={desc}/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image" content={this.state.data.coursePic.original[0].image}/>
                    <meta property="og:site_name" content="نیکارو"/>
                    <meta name="twitter:card" content="summary"/>
                    <meta name="twitter:site" content="@nikaro_ir"/>
                    <meta name="twitter:title" content={this.state.data.title}/>
                    <meta name="twitter:description" content={desc}/>
                    <link rel="canonical" href={"https://nikaro.ir" + this.props.asPath}/>
                </Head>
                {content}
            </>
        )
    }
}


