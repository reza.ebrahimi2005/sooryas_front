import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {create} from 'jss';
import rtl from 'jss-rtl';
import {createMuiTheme, makeStyles, ThemeProvider, StylesProvider, jssPreset} from '@material-ui/core/styles';
import {motion} from 'framer-motion';
import {useState, useEffect} from 'react'
import Head from 'next/head'

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {"تمامی حقوق این سایت متعلق به "}
            <Link color="primary" href="https://nikaro.ir">نیکارو مهر</Link>
            {' می باشد.'}
            {'© Copyright'}
        </Typography>
    );
}

// Configure JSS
const jss = create({plugins: [...jssPreset().plugins, rtl()]});

const theme = createMuiTheme({
    direction: "rtl",
    typography: {
        fontFamily: 'IRANSans',
        textAlign: 'right'
    },
    palette: {
        primary: {
            main: '#007aff',

        },
        secondary: {
            light: '#0066ff',
            main: '#0044ff',
            contrastText: '#ffcc00',
        },
    },
});

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(' + '/assets/images/adminLoginVector.svg' + ')',
        backgroundRepeat: 'no-repeat',
        backgroundColor: theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        width: '30%',
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        margin: theme.spacing(2),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    input: {},
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: "#007aff",
        '&:hover': {
            backgroundColor: "#0068d5",
        }
    },
    logoWrapper: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        left: 0,
        top: 0,
        overflow: 'hidden',
    },
    logoWrapperBg: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        left: 0,
        top: 0,
        backgroundColor: '#fff',
        zIndex: 1,
    },
    logoSvg: {
        zIndex: 2,
    },
    welcomeText: {
        zIndex: 2
    },
    [theme.breakpoints.down('sm')]: {
        paper: {
            width: '85%',
        }
    }
}));

const icon = {
    hidden: {
        pathLength: 0,
        pathOffset: 0,
        fill: "rgba(255, 255, 255, 0)"
    },
    visible: {
        pathLength: 1,
        pathOffset: 1,
        fill: "url(#linear-gradient)",
        transition: {
            default: {duration: 1, ease: "easeInOut"},
            fill: {duration: 1, ease: [1, 0, 0.8, 1]}
        },
    }
};

const mainSvg = {
    hide: {
        opacity: 0,
        scale: 1,
    },
    show: {
        opacity: 1,
        scale: 4.5,
        x: '-150%',
        y: '-100%',
    },

};

const icon1 = {
    hidden: {
        opacity: .3,
        mixBlendMode: 'multiply',
        fill: "url(#linear-gradient-2)",
    },
    visible: {
        opacity: .3,
    }
};

const icon2 = {
    hidden: {
        opacity: 1,
        fill: "rgba(255, 255, 255, 1)"
    },
    visible: {}
};

export default function Login() {
    const temp = useStyles();
    const [isMobile, setIsMobile] = useState(false)

    useEffect(() => {
        console.log(temp)
        if (window.innerWidth < 960) {
            setIsMobile(true);
        }
    })

    return (
        <>
            <Head>
                <title>پنل آموزشگاه ها | نیکارو</title>
                <meta name="description" property="description"
                      content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                <meta property="og:url" content="https://nikaro.ir/login"/>
                <meta property="og:title" content="پنل آموزشگاه ها | نیکارو"/>
                <meta property="og:description"
                      content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                <meta property="og:type" content="website"/>
                <meta property="og:image"
                      content="https://nikaro.ir/assets/images/nikaroWithBg.png"/>
                <meta property="og:site_name" content="نیکارو"/>
                <meta name="twitter:card" content="summary"/>
                <meta name="twitter:site" content="@nikaro_ir"/>
                <meta name="twitter:title" content="پنل آموزشگاه ها | نیکارو"/>
                <meta name="twitter:description"
                      content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                <link rel="canonical" href="https://nikaro.ir/login"/>
            </Head>
            <StylesProvider jss={jss}>
                <Grid container component="main" className={' makeStyles-root-1'} id={'loginPage'}>
                    <CssBaseline/>
                    <ThemeProvider theme={theme}>
                        <Grid item xs={12} component={Paper} elevation={6} square dir='rtl'>
                            <motion.div className={temp.paper} initial={{opacity: 0}} animate={{opacity: 1}}
                                        transition={(isMobile) ? {delay: 1, duration: 1} : {
                                            delay: 4,
                                            duration: 1
                                        }}>
                                <img src={'/assets/images/logo.svg'} width={'30%'} className={temp.image}/>
                                <Typography component="h1" variant="h5">
                                    ورود به پنل آموزشگاه
                                </Typography>
                                <form className={temp.form} noValidate action={'/admin/login'}
                                      method={'post'}>
                                    <TextField
                                        className={' makeStyles-input-6'}
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="username"
                                        label="نام کاربری"
                                        name="username"
                                        autoFocus
                                    />
                                    <TextField
                                        className={temp.input}
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="password"
                                        label="کلمه عبور"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                    />
                                    <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        className={temp.input}
                                    >
                                        ورود
                                    </Button>
                                    <Button className='d-block text-center text-primary'
                                            href="https://nikaro.ir/app/NikaroSchool-4.1.apk">دانلود اپلیکیشن نیکارو
                                        (مدیریت
                                        آموزشگاه ها)</Button>

                                    <Box mt={5}>
                                        <Copyright/>
                                    </Box>
                                </form>
                            </motion.div>
                        </Grid>
                    </ThemeProvider>
                    <motion.div
                        className={temp.logoWrapper + ' d-flex justify-content-center align-items-center flex-column'}
                        animate={(isMobile) ? {width: '0%'} : {width: '65%'}}
                        transition={{delay: 3, duration: .5}}
                    >
                        <motion.div className={temp.logoWrapperBg} animate={{zIndex: 0}}
                                    transition={{delay: 3}}/>
                        <motion.svg className={' makeStyles-logoSvg-10'} xmlns="http://www.w3.org/2000/svg"
                                    xlinkHref="http://www.w3.org/1999/xlink"
                                    viewBox="0 0 84.45 84.45"
                                    width={(isMobile) ? '80%' : '250px'}
                                    variants={mainSvg}
                                    initial={'hide'}
                                    animate={'show'}
                                    overflow={'hidden'}
                                    transition={{
                                        scale: {
                                            delay: 3,
                                            duration: 1,
                                        },
                                        x: {
                                            delay: 3,
                                            duration: 1,
                                        },
                                        y: {
                                            delay: 3,
                                            duration: 1,
                                        }
                                    }}
                        >
                            <defs>
                                <linearGradient id="linear-gradient" x1="1.36" y1="2.35" x2="90.11" y2="88.95"
                                                gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stopColor="#3fffbc"/>
                                    <stop offset="1" stopColor="#007bff"/>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="24.51" y1="49.84" x2="75.19" y2="49.84"
                                                xlinkHref="#linear-gradient"/>
                            </defs>
                            <g className="cls-1">
                                <g id="Layer_1" data-name="Layer 1">
                                    <motion.path className="cls-2 item"
                                                 d="M84.45,22A61.93,61.93,0,0,1,73.69,57a64.4,64.4,0,0,1-7.57,9.13A61.21,61.21,0,0,1,57,73.67,61.76,61.76,0,0,1,22,84.45a22.1,22.1,0,0,1-22-22V22A22.1,22.1,0,0,1,22,0H62.41A22.1,22.1,0,0,1,84.45,22Z"
                                                 variants={icon}
                                                 initial="hidden"
                                                 animate="visible"
                                    />
                                    <motion.path className="cls-3"
                                                 d="M75.19,54.66c-.5.79-1,1.56-1.51,2.31a64.4,64.4,0,0,1-7.57,9.13A61.21,61.21,0,0,1,57,73.67c-.77.51-1.54,1-2.33,1.51L24.51,45,45,24.51Z"
                                                 variants={icon1}
                                                 initial="hidden"
                                                 animate="visible"
                                    />
                                    <motion.rect className="cls-4" x="27.71" y="27.7" width="29.03" height="29.03"
                                                 rx="3.48"
                                                 ry="3.48"
                                                 transform="translate(42.22 -17.49) rotate(45)"
                                                 variants={icon2}
                                                 initial="hidden"
                                                 animate="visible"
                                    />
                                </g>
                            </g>
                        </motion.svg>
                        <motion.span className={temp.welcomeText + ' h2 text-center mt-5 d-block'}
                                     initial={{opacity: 0}}
                                     animate={{
                                         opacity: 1,
                                         scale: 0
                                     }}
                                     transition={{
                                         ease: "easeIn",
                                         duration: 2,
                                         scale: {
                                             delay: 3
                                         }
                                     }}
                                     style={{'textShadow': '0px 0px 5px #fff'}}

                        >خوش آمدید
                        </motion.span>
                    </motion.div>
                </Grid>
            </StylesProvider>
        </>
    );
}
