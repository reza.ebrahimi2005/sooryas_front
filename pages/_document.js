import Document, {Html, Head, Main, NextScript} from 'next/document'
import React from "react";

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return {...initialProps}
    }

    render() {
        return (
            <Html>
                <Head>
                    <meta name="theme-color" content="#032c60"/>
                    <link rel="icon" href="/assets/images/logoWithBg.png"/>
                    <link rel="stylesheet" href="/assets/bootstrap/bootstrap.min.css"/>
                    <link rel="stylesheet" href="/assets/flickity/flickity.css"/>
                    <link rel="stylesheet" href="/assets/leaflet/neshan-leaflet.css"/>
                    <link rel="stylesheet" href="/assets/bootstrap-toastr/toastr-rtl.css"/>
                    <script src="/assets/jquery/jquery.min.js"/>
                    <script src="/assets/bootstrap/bootstrap.js"/>
                    <script src="/assets/bootstrap-toastr/toastr.js"/>
                    <script src="/assets/jquery-validate/jquery.validate.min.js"/>
                    <script src="/assets/jquery-validate/additional-methods.min.js"/>
                    <script src="/assets/leaflet/neshan-leaflet.js"/>
                    <script src="/assets/rayChat/rayChat.js"/>
                    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125303310-1"></script>
                </Head>
                <body lang="fa">
                <Main/>
                <NextScript/>
                </body>
            </Html>
        )
    }
}

export default MyDocument

