import React from "react";
import DefaultLayout from "../components/layouts/DefaultLayout";
import SchoolLandingLayout from "../components/layouts/SchoolLandingLayout";
import {AuthProvider} from '../services/contexts/AuthContext';
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/styles.scss';

import {config} from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'

config.autoAddCss = false

function MyApp({Component, pageProps, router}) {
    if (router.pathname.startsWith('/landing/school')) {
        return (
            <SchoolLandingLayout>
                <Component {...pageProps} {...router} />
            </SchoolLandingLayout>
        )
    } else if (router.pathname === '/login') {
        return <Component {...pageProps} {...router} />
    }
    return (
        <AuthProvider>
            <DefaultLayout>
                <Component {...pageProps} {...router} />
            </DefaultLayout>
        </AuthProvider>
    )
}

export default MyApp
