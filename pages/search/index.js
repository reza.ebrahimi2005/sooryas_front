import React, {Component} from 'react';
import SearchAutoSuggest from '../../components/search-autoSuggest';
import CitySelect from '../../components/city-select';
import RegionSelect from '../../components/region-select';
import CategorySelect from '../../components/category-select';
import AgeSelect from '../../components/age-select';
import GenderSelect from '../../components/gender-select';
import ActiveCourses from '../../components/activeCourses-switch';
import HasDiscount from '../../components/has-discout-switch';
import CourseSchoolSelect from '../../components/course-school-select';
import SortSelect from '../../components/sort-select';
import InfiniteItems from '../../components/infinitScroll';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFilter, faRandom, faSortAmountDown} from '@fortawesome/free-solid-svg-icons'
import Head from "next/head";

export default class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            q: null,
            sortUpdate: false,
            courseSchoolUpdate: false,
            regionId: false,
            cityId: false,
            categoryFilter: false,
            ageFilter: false,
            genderFilter: false,
            active: false,
            discount: false,
            LatLng: null,
            isMobile: null
        };
    }

    updateDimensions() {
        if (window.innerWidth < 768) {
            this.setState({isMobile: true});
        } else {
            this.setState({isMobile: false});
        }
    }

    componentDidMount() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        this.setState({
            q: url.searchParams.get('q'),
            isMobile: (window.innerWidth < 768)
        })
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    handleClick() {
        $('.selects-wrapper').toggle();
        $('.mobile-selects button i').toggleClass('fa-filter fa-times');
    }

    sortValue(value) {
        if (value != null) {
            this.setState({sortUpdate: value.value});
        } else {
            this.setState({sortUpdate: 0});
        }
    }

    LatLngValue(value) {
        this.setState({
            LatLng: value
        })
    }

    courseSchoolValue(value) {
        if (value != null) {
            this.setState({courseSchoolUpdate: value.value});
        } else {
            this.setState({courseSchoolUpdate: 0});
        }
    }

    regionValue(value) {
        if (value != null) {
            this.setState({regionId: value.value});
        } else {
            this.setState({regionId: 0});
        }
    }

    cityValue(value) {
        if (value != null) {
            this.setState({cityId: value.value});
        } else {
            this.setState({cityId: 0});
        }

    }

    categoryFilter(value) {
        if (value != null) {
            this.setState({categoryFilter: value.value});
        } else {
            this.setState({categoryFilter: 0});
        }
    }

    ageFilter(value) {
        if (value != null) {
            this.setState({ageFilter: value.value});
        } else {
            this.setState({ageFilter: 0});
        }

    }

    genderFilter(value) {
        if (value != null) {
            this.setState({genderFilter: value.value});
        } else {
            this.setState({genderFilter: 0});
        }
    }

    active(value) {
        this.setState({active: value});
    }

    discount(value) {
        this.setState({discount: value});
    }

    submitAutoSuggestForm(event) {
        event.preventDefault();
        if ($('input[name="q"]').val() !== "") {
            this.setState({
                q: $('input[name="q"]').val()
            })
        }
    }

    render() {
        return (
            <>
                <Head>
                    <title>موسسه علمی آموزشی فرهنگی سوریاس مهر</title>
                    <meta name="description" property="description"
                          content="در موسسه علمی آموزشی فرهنگی سوریاس مهر می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:url" content="https://nikaro.ir/search"/>
                    <meta property="og:title" content="سامانه معرفی و ثبت نام کلاس های آموزشی | موسسه علمی آموزشی فرهنگی سوریاس مهر"/>
                    <meta property="og:description"
                          content="در موسسه علمی آموزشی فرهنگی سوریاس مهر می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image" content="assets/images/logoWithBg.png"/>
                    <meta property="og:site_name" content="موسسه علمی آموزشی فرهنگی سوریاس مهر"/>
                    <meta name="twitter:card" content="summary"/>
                    <meta name="twitter:site" content="@sooryas.com"/>
                    <meta name="twitter:title" content="سامانه معرفی و ثبت نام کلاس های آموزشی | موسسه علمی آموزشی فرهنگی سوریاس مهر"/>
                    <meta name="twitter:description"
                          content="در موسسه علمی آموزشی فرهنگی سوریاس مهر می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                </Head>
                <div className='searchResult row' id={'resultPage'}>
                    {
                        (this.state.isMobile)
                            ?
                            <div className={'mobile-selects d-md-none d-xl-none d-lg-none'}>
                                <button onClick={this.handleClick.bind(this)} className='btn bg-nikaro'>
                                    <FontAwesomeIcon icon={faFilter} className='text-white'/>
                                </button>
                                <div className='selects-wrapper' style={{'display': 'none'}}>
                                    <div className='selects-container bg-white'>
                                        <div className='row filters '>
                                            <div className='col-12 header justify-content-center'>
                                                <span>فیلتر جستجو</span>
                                            </div>
                                            <div className='col-12 selects'>
                                                <div className='select text-right'>
                                                    <CitySelect
                                                        func={this.cityValue.bind(this)}
                                                        provinceId={this.state.provinceId}
                                                    />
                                                </div>
                                                <div className='select text-right'>
                                                    <RegionSelect cityId={this.state.cityId}
                                                                  func={this.regionValue.bind(this)}/>
                                                </div>
                                                <div className='select text-right'>
                                                    <CategorySelect func={this.categoryFilter.bind(this)}/>
                                                </div>
                                                <div className='select text-right'>
                                                    <AgeSelect func={this.ageFilter.bind(this)}/>
                                                </div>
                                                <div className='select text-right'>
                                                    <GenderSelect func={this.genderFilter.bind(this)}/>
                                                </div>
                                                <div
                                                    className='select d-flex justify-content-center align-items-center'>
                                                    <ActiveCourses func={this.active.bind(this)}/>
                                                </div>
                                                <div
                                                    className='select d-flex justify-content-center align-items-center'>
                                                    <HasDiscount
                                                        func1={this.discount.bind(this)}
                                                        discount={this.state.discount}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :
                            <div className='col-sm-2 d-none d-sm-block d-xl-block'
                                 style={{'borderLeft': '1px solid #dddddd'}}>
                                <div className='row filters'>
                                    <div className='col-12 header'>
                                        <i className='fa fa-filter'/>
                                        <span>فیلتر جستجو</span>
                                    </div>
                                    <div className='col-12 selects'>
                                        <div className='select text-right'>
                                            <CitySelect
                                                func={this.cityValue.bind(this)}
                                                provinceId={this.state.provinceId}
                                            />
                                        </div>
                                        <div className='select text-right'>
                                            <RegionSelect cityId={this.state.cityId}
                                                          func={this.regionValue.bind(this)}/>
                                        </div>
                                        <div className='select text-right'>
                                            <CategorySelect func={this.categoryFilter.bind(this)}/>
                                        </div>
                                        <div className='select text-right'>
                                            <AgeSelect func={this.ageFilter.bind(this)}/>
                                        </div>
                                        <div className='select text-right'>
                                            <GenderSelect func={this.genderFilter.bind(this)}/>
                                        </div>
                                        <div
                                            className='select text-right d-flex justify-content-center align-items-center'>
                                            <ActiveCourses func={this.active.bind(this)}/>
                                        </div>
                                        <div
                                            className='select text-center d-flex justify-content-center align-items-center'>
                                            <HasDiscount
                                                func1={this.discount.bind(this)}
                                                discount={this.state.discount}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    }
                    <div className='col-sm-10 col-12'>
                        <div className='results align-items-center'>
                            <div className='row header'>
                                <form className='col-md-5 col-12' onSubmit={this.submitAutoSuggestForm.bind(this)}
                                      id='autoSuggestForm'>
                                    <SearchAutoSuggest/>
                                </form>
                                <div className='col-md-3 col-6 text-center pl-1 my-2 my-md-0'>
                                    <SortSelect func={this.sortValue.bind(this)} LatLng={this.LatLngValue.bind(this)}/>
                                    <FontAwesomeIcon icon={faSortAmountDown}/>
                                </div>
                                <div className='col-md-3 col-6 text-center pr-1 my-2 my-md-0'>
                                    {/*<CourseSchoolSelect func={this.courseSchoolValue.bind(this)}/>
                                    <FontAwesomeIcon icon={faRandom}/>*/}
                                </div>
                                {/*<span className='col-md-1 col-4 numFound d-block text-md-left text-center px-0'></span>*/}
                            </div>
                            <InfiniteItems
                                q={this.state.q}
                                sort={this.state.sortUpdate}
                                courseSchoolSelect={this.state.courseSchoolUpdate}
                                regionId={this.state.regionId}
                                cityId={this.state.cityId}
                                categoryFilter={this.state.categoryFilter}
                                ageFilter={this.state.ageFilter}
                                genderFilter={this.state.genderFilter}
                                active={this.state.active}
                                LatLng={this.state.LatLng}
                                discount={this.state.discount}
                            />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
