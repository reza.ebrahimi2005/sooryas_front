import React, {Component} from 'react';
import Button from "@material-ui/core/Button";
import CountUp from "react-countup";
import JoinSchools from '../../components/joinSchools.js'
import Particles from 'react-particles-js';
import $ from 'jquery'
import Head from "next/head";

export default class SchoolsLanding extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobile: false,
            open: false,
            loading: false,
            school: 8935,
            city: 66,
        };
        this.handleClose = this.handleClose.bind(this);
    }

    handleOpen() {
        this.setState({
            open: true,
        });
    }

    handleClose(value) {
        this.setState({
            open: !value
        })
    }

    componentDidMount() {
        this.setState({
            isMobile: (window.innerWidth < 768)
        })

        // Cache selectors
        var topMenu = $(".navbar-nav"),
            topMenuHeight = topMenu.outerHeight() + 100,
            // All list items
            menuItems = topMenu.find("a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function () {
                var item = $($(this).attr("href"));
                if (item.length) {
                    return item;
                }
            });

        // Bind to scroll
        $(window).scroll(function () {
            // Get container scroll position
            var fromTop = $(this).scrollTop() + topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function () {
                if ($(this).offset().top <= fromTop
                )
                    return this;
            });
            // Get the id of the current element
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";
            // Set/remove active class
            menuItems.removeClass("active").filter("[href='#" + id + "']").addClass("active");
        });

        document.querySelectorAll('a[href^="#"]').forEach(anchor => {
            anchor.addEventListener('click', function (e) {
                e.preventDefault();
                document.querySelector(this.getAttribute('href')).scrollIntoView({
                    behavior: 'smooth'
                });
            });
        });
    }

    render() {
        return (
            <>
                <Head>
                    <title>به جمع هزاران آموزشگاه ثبت شده در نیکارو بپیوندید | نیکارو</title>
                    <meta name="description" property="description"
                          content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:url" content={"https://nikaro.ir" + this.props.asPath}/>
                    <meta property="og:title" content="به جمع هزاران آموزشگاه ثبت شده در نیکارو بپیوندید | نیکارو"/>
                    <meta property="og:description"
                          content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image" content="https://nikaro.ir/assets/images/nikaroWithBg.png"/>
                    <meta property="og:site_name" content="نیکارو"/>
                    <meta name="twitter:card" content="summary"/>
                    <meta name="twitter:site" content="@nikaro_ir"/>
                    <meta name="twitter:title" content="به جمع هزاران آموزشگاه ثبت شده در نیکارو بپیوندید | نیکارو"/>
                    <meta name="twitter:description"
                          content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <link rel="canonical" href={"https://nikaro.ir" + this.props.asPath}/>
                </Head>
                <div id='schoolLanding'>
                    <nav
                        className='row navbar navbar-expand-lg sticky-top justify-content-between flex-md-row flex-column'>
                        <a href={'/'} className='navbar-brand mx-auto mb-2 mb-lg-0 mx-lg-0'>
                            <img width="150" id={'logo'} src={'/assets/images/logo.svg'}/>
                        </a>
                        <ul className='navbar-nav text-white pr-0 ml-lg-5 mx-auto d-none d-md-flex flex-row'>
                            <li className='nav-item '>
                                <a href="#home" className='p-md-2 px-md-3 p-1 rounded-pill active'>#</a>
                            </li>
                            <li className='nav-item '>
                                <a href="#panel" className='p-md-2 px-md-3 p-1 rounded-pill'>مدیریت کلاس ها</a>
                            </li>
                            <li className='nav-item '>
                                <a href="#features" className='p-md-2 px-md-3 p-1 rounded-pill'>امکانات</a>
                            </li>
                            <li className='nav-item '>
                                <a href="#virtual" className='p-md-2 px-md-3 p-1 rounded-pill'>کلاس مجازی</a>
                            </li>
                            <li className='nav-item '>
                                <a href="#ad" className='p-md-2 px-md-3 p-1 rounded-pill'>بیشتر دیده شوید</a>
                            </li>
                            <li className='nav-item '>
                                <a href="#commission" className='p-md-2 px-md-3 p-1 rounded-pill'>پرداخت به ازای ثبت
                                    نام</a>
                            </li>
                            <li className='nav-item '>
                                <a href="#support" className='p-md-2 px-md-3 p-1 rounded-pill'>پشتیبانی</a>
                            </li>
                        </ul>
                    </nav>
                    <section id='home'
                             className='row header justify-content-center align-items-center px-2 px-md-0 pt-0 pb-0'>
                        <div className='text-center'>
                            <Particles style={{position: 'fixed', top: 0, right: 0, opacity: '.4'}} params={{
                                "absorbers": [],
                                "background": {},
                                "backgroundMask": {
                                    "cover": {
                                        "color": {
                                            "value": "#fff"
                                        },
                                        "opacity": 1
                                    },
                                    "enable": false
                                },
                                "detectRetina": true,
                                "emitters": [],
                                "fpsLimit": 30,
                                "interactivity": {
                                    "detectsOn": "canvas",
                                    "events": {
                                        "onClick": {
                                            "enable": true,
                                            "mode": "push"
                                        },
                                        "onDiv": {
                                            "elementId": "",
                                            "enable": false,
                                            "mode": []
                                        },
                                        "onHover": {
                                            "enable": true,
                                            "mode": "repulse",
                                            "parallax": {
                                                "enable": false,
                                                "force": 60,
                                                "smooth": 10
                                            }
                                        },
                                        "resize": true
                                    },
                                    "modes": {
                                        "absorbers": [],
                                        "bubble": {
                                            "distance": 400,
                                            "duration": 2,
                                            "opacity": 0.8,
                                            "size": 40
                                        },
                                        "connect": {
                                            "distance": 80,
                                            "lineLinked": {
                                                "opacity": 0.5
                                            },
                                            "radius": 60
                                        },
                                        "emitters": [],
                                        "grab": {
                                            "distance": 400,
                                            "lineLinked": {
                                                "opacity": 1
                                            }
                                        },
                                        "push": {
                                            "quantity": 4
                                        },
                                        "remove": {
                                            "quantity": 2
                                        },
                                        "repulse": {
                                            "distance": 200,
                                            "duration": 0.4,
                                            "speed": 1
                                        },
                                        "slow": {
                                            "factor": 3,
                                            "radius": 200
                                        }
                                    }
                                },
                                "particles": {
                                    "collisions": {
                                        "enable": false,
                                        "mode": "bounce"
                                    },
                                    "color": {
                                        "value": "#ffffff"
                                    },
                                    "lineLinked": {
                                        "blink": false,
                                        "color": {
                                            "value": "#ffffff"
                                        },
                                        "consent": false,
                                        "distance": 150,
                                        "enable": true,
                                        "opacity": 0.4,
                                        "shadow": {
                                            "blur": 5,
                                            "color": {
                                                "value": "lime"
                                            },
                                            "enable": false
                                        },
                                        "width": 1
                                    },
                                    "move": {
                                        "attract": {
                                            "enable": false,
                                            "rotate": {
                                                "x": 600,
                                                "y": 1200
                                            }
                                        },
                                        "direction": "none",
                                        "enable": true,
                                        "outMode": "out",
                                        "random": false,
                                        "speed": 2,
                                        "straight": false,
                                        "trail": {
                                            "enable": false,
                                            "length": 10,
                                            "fillColor": {
                                                "value": "#000000"
                                            }
                                        }
                                    },
                                    "number": {
                                        "density": {
                                            "enable": true,
                                            "area": 800
                                        },
                                        "limit": 0,
                                        "value": 80
                                    },
                                    "opacity": {
                                        "animation": {
                                            "enable": false,
                                            "minimumValue": 0.1,
                                            "speed": 1,
                                            "sync": false
                                        },
                                        "random": {
                                            "enable": false,
                                            "minimumValue": 1
                                        },
                                        "value": 0.5
                                    },
                                    "rotate": {
                                        "animation": {
                                            "enable": false,
                                            "speed": 0,
                                            "sync": false
                                        },
                                        "direction": "clockwise",
                                        "random": false,
                                        "value": 0
                                    },
                                    "shadow": {
                                        "blur": 0,
                                        "color": {
                                            "value": "#000000"
                                        },
                                        "enable": false,
                                        "offset": {
                                            "x": 0,
                                            "y": 0
                                        }
                                    },
                                    "shape": {
                                        "options": {
                                            "character": {
                                                "fill": true,
                                                "close": true,
                                                "font": "Verdana",
                                                "style": "",
                                                "value": "*",
                                                "weight": "400"
                                            },
                                            "char": {
                                                "fill": true,
                                                "close": true,
                                                "font": "Verdana",
                                                "style": "",
                                                "value": "*",
                                                "weight": "400"
                                            },
                                            "polygon": {
                                                "nb_sides": 5
                                            },
                                            "star": {
                                                "nb_sides": 5
                                            }
                                        },
                                        "image": {
                                            "fill": true,
                                            "close": true,
                                            "height": 100,
                                            "replaceColor": true,
                                            "src": "https://cdn.matteobruni.it/images/particles/github.svg",
                                            "width": 100
                                        },
                                        "type": "circle"
                                    },
                                    "size": {
                                        "animation": {
                                            "enable": false,
                                            "minimumValue": 0.1,
                                            "speed": 40,
                                            "sync": false
                                        },
                                        "random": {
                                            "enable": true,
                                            "minimumValue": 1
                                        },
                                        "value": 5
                                    },
                                    "stroke": {
                                        "color": {
                                            "value": "#000000"
                                        },
                                        "width": 0,
                                        "opacity": 1
                                    },
                                    "twinkle": {
                                        "lines": {
                                            "enable": false,
                                            "frequency": 0.05,
                                            "opacity": 1
                                        },
                                        "particles": {
                                            "enable": false,
                                            "frequency": 0.05,
                                            "opacity": 1
                                        }
                                    }
                                },
                                "pauseOnBlur": true,
                                "polygon": {
                                    "draw": {
                                        "enable": false,
                                        "stroke": {
                                            "color": {
                                                "value": "#fff"
                                            },
                                            "width": 0.5,
                                            "opacity": 1
                                        }
                                    },
                                    "enable": false,
                                    "inline": {
                                        "arrangement": "one-per-point"
                                    },
                                    "move": {
                                        "radius": 10,
                                        "type": "path"
                                    },
                                    "scale": 1,
                                    "type": "none",
                                    "url": ""
                                }
                            }}/>
                            <h1 className='h1 d-block text-white'>به جمع هزاران آموزشگاه ثبت شده در نیکارو بپیوندید</h1>
                            <span className='d-block h5 my-4' style={{'color': '#9ea8e9'}}>
                            تاکنون
                            <span className='text-success h4 mx-2'>
                                {<CountUp start={this.state.school - 500} end={this.state.school} duration={3}/>}
                            </span>
                             آموزشگاه از
                            <span className='text-success h4 mx-2'>
                                {<CountUp end={this.state.city} duration={3}/>}
                            </span>
                             شهر مختلف ایران در نیکارو ثبت شده اند
                        </span>
                            <Button className='text-white mt-2 p-3 rounded-pill bg-success' variant='contained'
                                    onClick={this.handleOpen.bind(this)}
                                    style={{'fontFamily': 'IRANSans'}}>ثبت رایگان آموزشگاه در نیکارو</Button>
                        </div>
                    </section>
                    <section id='panel' className='row justify-content-center align-items-center flex-wrap px-md-0'>
                        <div className='col-md-5 col-12 text-center'>
                            <h2 className='h1 text-white text-center'>همیشه همه چیز تحت کنترل شما !</h2>
                            <div className='text-white h5 mt-4 d-flex justify-content-center align-items-center'>
                                <div className='text-right' style={{'color': '#9ea8e9'}}>
                                    <p>از خدمات سکوی فرصت نیکارو بهره مند شوید:</p>
                                    <ul className='mr-2'>
                                        <li className='my-3'>حساب کاربری پنل آموزشگاه و اپلیکیشن دریافت کنید</li>
                                        <li className='my-3'>دوره هایتان را معرفی کنید</li>
                                        <li className='my-3'>با متقاضیان گفتگو کنید</li>
                                        <li className='my-3'> کاربر بیشتری جذب کنید</li>
                                    </ul>
                                </div>
                            </div>
                            <Button
                                variant={"contained"}
                                disabled
                                className='rounded-pill p-2 px-4 bg-success mt-2 text-white h4'
                                style={{'fontFamily': 'IRANSans'}}>
                                حق اشتراک سالیانه پنل نیکارو 100 هزار تومان
                            </Button>
                        </div>
                        <img
                            src={(!this.state.isMobile) ? '/assets/images/process.png' : '/assets/images/process-m.png'}
                            alt="nikaro process"
                            className='col-md-7 col-12'
                            style={{objectFit: 'contain'}}
                            height='600px'
                        />
                    </section>
                    <section id='features'
                             className='row flex-column justify-content-center align-items-center px-2 px-md-0'>
                        <div
                            className='text-white mt-4 d-flex justify-content-center align-items-center flex-wrap px-md-4 px-2 items'>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/pay.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>درگاه پرداخت امن</span>
                                    <span
                                        className='text-justify'>بدون دردسر های اداری از درگاه امن پرداخت بهره مند شوید.</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/accounting.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>شفافیت مالی</span>
                                    <span
                                        className='text-justify'>بدون نیاز به حسابدار، در جریان گردش مالی آموزشگاه باشید</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/discount-percent.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>طرح های تخفیفی</span>
                                    <span
                                        className='text-justify'>بدون هزینه کاربرانتان را از تخفیف های منتوع برخوردار کنید</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/ad.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>بستر تبلیغاتی رایگان</span>
                                    <span
                                        className='text-justify'>به صورت نامحدود و رایگان در نیکارو کلاس هایتان را تعریف کنید</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/domain.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>آدرس اختصاصی</span>
                                    <span
                                        className='text-justify'>برای خود یک آدرس اینترنتی مستقل داشته باشید</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/onlineClass.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>فضای آموزش آنلاین</span>
                                    <span
                                        className='text-justify'>آنلاین کلاس برگزار کنید و نگران تعطیلی ها نباشید</span>
                                </div>
                            </div>

                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/dataBase.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>نگهداری اطلاعات</span>
                                    <span
                                        className='text-justify'>تاریخچه و سوابق همه کلاس ها ثبت میگردند تا برای همیشه قابل پیگیری باشند</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/intrestUSer.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>کاربر بیشتری جذب کنید</span>
                                    <span
                                        className='text-justify'>با خدمات شتابدهی،  بازدیدکننده بیشتری را به مشتری تبدیل کنید</span>
                                </div>
                            </div>
                            <div className='col-12 col-md-4 d-flex justify-content-between align-items-center'>
                                <img src={'/assets/images/reachable.png'} width='30%'/>
                                <div
                                    className='desc text-right mr-2 d-flex flex-column align-items-start justify-content-center'>
                                    <span className='d-block h5'>همیشه پاسخگو باشید</span>
                                    <span
                                        className='text-justify'>با اپلیکیشن به پرسش های کاربران پاسخ دهید</span>
                                </div>
                            </div>

                        </div>
                    </section>
                    <section id='virtual'
                             className='row flex-column justify-content-center align-items-center px-2 px-md-0'>
                        <h2 className='h1 text-center text-white'>ارائه کلاس آموزشی آسان تر از همیشه !</h2>
                        <div className='px-md-4 text-white h5 my-4 d-flex justify-content-center align-items-center'>
                            <p className='col-12 col-md-5 text-justify' style={{'color': '#9ea8e9'}}>
                                با استفاده از ابزار کلاس مجازی نیکارو می توانید کلاس های خود را به صورت آنلاین برگزار
                                کرده و
                                انجام کلیه امور مربوط به ثبت نام تا اطلاع رسانی به کاربران را به ما بسپارید.
                            </p>
                        </div>
                        <div className='items'>

                            <div
                                className='item d-flex flex-column justify-content-center align-items-center text-white'>
                                <span className='d-block mb-2 h6'>10 کاربر همزمان</span>
                                <span className='d-block mb-2 h6'>3 ویدئو همزمان</span>
                                <div className='bg-success rounded-pill py-2 px-4 h5 price'>100,000 تومان</div>
                            </div>

                            <div
                                className='item d-flex flex-column justify-content-center align-items-center text-white'>
                                <span className='d-block mb-2 h6'>20 کاربر همزمان</span>
                                <span className='d-block mb-2 h6'>3 ویدئو همزمان</span>
                                <div className='bg-success rounded-pill py-2 px-4 h5 price'>200,000 تومان</div>
                            </div>

                            <div
                                className='item d-flex flex-column justify-content-center align-items-center text-white'>
                                <span className='d-block mb-2 h6'>30 کاربر همزمان</span>
                                <span className='d-block mb-2 h6'>3 ویدئو همزمان</span>
                                <div className='bg-success rounded-pill py-2 px-4 h5 price'>270,000 تومان</div>
                            </div>


                            <div
                                className='item d-flex flex-column justify-content-center align-items-center text-white'>
                                <span className='d-block mb-2 h6'>50 کاربر همزمان</span>
                                <span className='d-block mb-2 h6'>3 ویدئو همزمان</span>
                                <div className='bg-success rounded-pill py-2 px-4 h5 price'>400,000 تومان</div>
                            </div>
                        </div>
                    </section>
                    <section id='ad' className='row justify-content-center align-items-center px-2 px-md-0 flex-wrap'>
                        <div className='col-12 col-md-6 text-right'>
                            <h2 className='col-12 h1 text-center text-white mt-md-5'>بیشتر دیده شوید !</h2>
                            <div
                                className='col-12 text-white px-md-5 px-0 h5 mt-4 d-flex justify-content-center align-items-center'>
                                <p className='text-justify' style={{'color': '#9ea8e9'}}>
                                    با استفاده از بسته تبلیغاتی نیکارو می توانید بیشتر دیده شوید. کاربرانی را که در
                                    موتورهای
                                    جستجو به دنبال کلاس های در حال برگزاری شما می گردند را به صفحه آموزشگاه خود هدایت و
                                    متقاضیان بیشتری را جذب کنید.
                                    <br/>
                                    <br/>
                                    سالانه بیش از 2 میلیون نفر از وبسایت نیکارو بازدید میکنند. با قرار گرفتن آموزشگاهتان
                                    در
                                    لیست بهترین های منطقه، بهترین های موتور جستجو و بنر تبلیغاتی از 200 تا 900 درصد
                                    بازدید
                                    کاربران از صفحه آموزشگاهتان را افزایش دهید و کاربران بیشتری را ثبت نام کنید.
                                </p>
                            </div>
                            <Button className='col-12 col-md-5 mx-md-5 text-white mt-2 p-3 rounded-pill bg-success'
                                    onClick={this.handleOpen.bind(this)}
                                    color={"primary"} variant='contained'
                                    style={{'fontFamily': 'IRANSans'}}>همین حالا آموزشگاه خود را ثبت کنید</Button>
                        </div>
                        <div
                            className='col-12 col-md-5 p-2 p-md-4 ml-md-2 mt-4 my-md-0 bg-white text-center text-black d-flex justify-content-center align-items-center flex-wrap item'>
                            <span className='col-4 text-black h5 border-left'>تبلیغات بنری در وبلاگ</span>
                            <span className='col-4 text-black h5 border-left'>معرفی در پست بهترین های منطقه</span>
                            <span className='col-4 text-black h5'>رپورتاژ آگهی</span>
                            <Button href={'#support'} className='col-9 mx-5 text-white p-3 rounded-pill text-nowrap'
                                    color={"primary"} variant='contained'
                                    onClick={this.handleOpen.bind(this)}
                                    style={{'fontFamily': 'IRANSans', 'backgroundColor': '#354BBB'}}>
                                همین حالا آموزشگاهتان را شتاب دهید
                            </Button>
                        </div>
                    </section>
                    <section id='commission'
                             className='row flex-column justify-content-center align-items-center px-2 px-md-0'>
                        <h2 className='h1 text-center text-white'>به ازای ثبت نام قطعی پرداخت کنید !</h2>
                        <div>
                            <p className='col-12 col-md-7 text-justify h5 mt-4 mx-auto'>
                                شما میتوانید با تعریف دوره های خود از طریق پنل کاربری نیکارو به ازای هر ثبت نام قطعی
                                <span className='rounded text-white p-1 mx-1' style={{'backgroundColor': '#ababab6e'}}> %9 از مبلغ دوره </span>
                                را به عنوان کارمزد پرداخت کنید.
                            </p>
                            <div className='col-12 col-md-7 text-right mx-auto h6 mt-4' style={{'color': '#9ea8e9'}}>
                                <p>با استفاده از پنل کاربری نیکارو می توانید از خدماتی شامل:</p>
                                <ul className='mr-2'>
                                    <li className='my-3'>داشبورد مدیریتی و اپلیکیشن آموزشگاه من</li>
                                    <li className='my-3'>حسابداری مالی</li>
                                    <li className='my-3'>پنل اطلاع رسانی پیامکی</li>
                                    <li className='my-3'>و ده ها خدمت دیگر</li>
                                </ul>
                                <p>بهره مند شوید.</p>
                            </div>
                        </div>
                        <Button className='mx-md-5 text-white p-3 rounded-pill bg-success mt-md-4'
                                color={"primary"} variant='contained'
                                onClick={this.handleOpen.bind(this)}
                                style={{'fontFamily': 'IRANSans'}}>به جمع آموزشگاه های نیکارو بپیوندید</Button>
                    </section>
                    <section id='support'
                             className='row flex-column justify-content-center align-items-center px-2 px-md-0'>
                        <h2 className='h1 text-white text-center mb-5'>هنوز سوالی دارید؟</h2>
                        <div>
                            <p className='col-md-5 px-md-5 text-center h4 mt-4 mx-auto'>
                                برای اطلاعات بیشتر یا هرگونه ابهام در مورد چگونگی همکاری با نیکارو با شماره زیر تماس
                                بگیرید.
                                مشتاق شنیدن صدای شما هستیم.
                            </p>
                        </div>
                        <a href="tel:05136024778"
                           className='bg-success text-white p-3 text-center mt-5 h4 rounded-pill'>051-36024778</a>
                        <a href="tel:09916367451"
                           className='bg-success text-white p-3 text-center mt-2 h4 rounded-pill mt-2'>09916367451</a>
                    </section>
                    <JoinSchools open={this.state.open} close={this.handleClose}/>
                </div>
            </>
        );
    }
}
