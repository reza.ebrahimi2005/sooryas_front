import React, {Component} from 'react';
import Head from "next/head";
import CoursesCarousel from '../components/courses-carousel';
import SchoolCarousel from '../components/schools-carousel';
import SearchAutoSuggest from '../components/search-autoSuggest';
import CountdownTimer from "../components/countDownTimer";
import DateRangeIcon from '@material-ui/icons/DateRange';
import ContactsIcon from '@material-ui/icons/Contacts';
import TodayIcon from '@material-ui/icons/Today';
import AlarmIcon from '@material-ui/icons/Alarm';
import Button from "@material-ui/core/Button";
import $ from 'jquery';
import axios from 'axios';
import LazyLoad from 'react-lazyload';

export async function getServerSideProps() {
    // Fetch data from external API
    const res = await fetch(process.env.NEXT_PUBLIC_BLOG_API_URL +`/courses/categories`);
    const data = await res.json();
    // Pass data to the page via props
    return {props: {"tags": data}}
}

const handleTags = (tags) => {
    let items = [];
    var words = ['کلاس', 'دوره', 'آموزشگاه', 'آموزش', 'مهارت', 'کلاس کودک', 'کلاس بزرگسالان', 'معرفی آموزشگاه', 'معرفی کلاس', 'ثبت نام آنلاین', 'ثبت نام آنلاین کلاس', 'ثبت نام اقساطی', ' ثبت نام ارزان', 'کلاس تخفیف دار', 'تهران', 'مشهد', 'کرج', 'اصفهان', 'شیراز', 'تبریز'];
    words.forEach(function (e) {
        {
            items.push(
                <Button href={'/search?q=' + e}
                        className='border p-2 m-1 m-sm-2 rounded-pill'>{e}</Button>
            );
        }
    });
    tags.forEach(function (e) {
        {
            items.push(
                <Button href={'/search?q=' + e.title}
                        className='border p-2 m-1 m-sm-2 rounded-pill'>{e.title}</Button>
            );
            e.subCategories.forEach(function (i) {
                items.push(
                    <Button href={'/search?q=' + i.title}
                            className='border p-2 m-1 m-sm-2 rounded-pill'>{i.title}</Button>
                )
            })
        }
    });
    return items;
}

const handleScrollToCampaigns = () => {
    window.scrollTo({
        top: $('#campaigns').offset().top,
        behavior: 'smooth'
    })
};

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: null,
            orientation: null,
            loading: false,
            bannerData: null,
            offerData: null,
            categories: handleTags(props.tags),
            preLoading: true,
        };
    }

    updateDimensions() {
        var orientation;
        if (window.screen.width > window.screen.height) {
            orientation = 'landscape';
        } else {
            orientation = 'portrait';
        }
        if (window.innerWidth < 575) {
            this.setState({
                width: 768,
                orientation: orientation
            });
        } else {
            this.setState({
                width: window.innerWidth,
                orientation: orientation
            });
        }
    }

    updateLogoOnScroll() {
        // var hT = $('.campaigns').offset().top,
        //     hH = $('.campaigns').outerHeight(),
        //     wH = $(window).height(),
        //     hwS = window.pageYOffset,
        //     hT2 = $('.joinUs-app').offset().top,
        //     hH2 = $('.joinUs-app').outerHeight(),
        //     hT4 = $('footer').offset().top,
        //     hH4 = $('footer').outerHeight();
        //
        // let vh = window.innerHeight * 0.01;
        // document.documentElement.style.setProperty('--vh', `${vh}px`);
        //
        // var hT3 = $('.app').offset().top,
        //     hH3 = $('.app').outerHeight();
        //
        // if ((wS > (hT2 + hH2 - wH + 150) && wS < (hT2 + hH2))) {
        //     document.getElementById("logo").src = '/assets/images/logoWhite.svg';
        //     $('.top-nav .mobile_menu_btn i').css('color', '#ffffff');
        //     $('.top-nav .menu ul li:not(:last-child) a').addClass('text-white');
        //     $('#loginBtn').removeClass('btn-outline-primary').addClass('bg-white').addClass('text-primary');
        //     $('.top-nav').removeClass('bg-white');
        //     $('.campaigns .animated').addClass('pulse');
        //     $('.join-us .animated').addClass('fadeIn');
        // } else if ((wS > (hT3 + hH3 - wH - 200) && wS < (hT3 + hH3))) {
        //     document.getElementById("logo").src = '/assets/images/logo.svg';
        //     $('.top-nav .mobile_menu_btn i').css('color', '#007bff');
        //     $('.top-nav .menu ul li:not(:last-child) a').removeClass('text-white');
        //     $('#loginBtn').removeClass('bg-white').addClass('btn-outline-primary');
        //     $('.top-nav').removeClass('bg-white');
        //     $('.app .animated').removeClass('d-none').addClass('fadeInLeftBig');
        // } else {
        //     document.getElementById("logo").src = '/assets/images/logo.svg';
        //     $('.top-nav .mobile_menu_btn i').css('color', '#007bff');
        //     $('.top-nav .menu ul li:not(:last-child) a').removeClass('text-white');
        //     $('#loginBtn').removeClass('bg-white').addClass('btn-outline-primary');
        //     $('.top-nav ').addClass('bg-white').css('paddingTop', '0%');
        // }
    }

    componentDidMount() {
        this.setState({
            width: window.innerWidth,
            orientation: (window.screen.width > window.screen.height) ? 'landscape' : 'portrait',
        })
        this.updateDimensions();
        this.updateLogoOnScroll();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        window.addEventListener("scroll", this.updateLogoOnScroll.bind(this));
        axios.get(process.env.NEXT_PUBLIC_API_URL+'/templates/modules/banner').then((response) => {
            if(response.data !==""){
                this.setState({
                    bannerData: response.data,
                })
            }

        }).catch(function (error) {
            // handle error
            console.log(error);
        });
        axios.get(process.env.NEXT_PUBLIC_API_URL+'/templates/modules/amazingOffer').then((response) => {
            const self = this;
            if(response.data !==""){
                self.setState({
                    offerData: response.data,
                })
            }



        }) .catch(function (error) {
            // handle error
            console.log(error);
        });
        // if (typeof Pushe != "undefined") {
        // Pushe.getDeviceId()
        //     .then((deviceId) => {
        //         if (parseInt($('input[name="user_id"]').val()) > 0) {
        //             axios.post('https://nikaro.ir/users/addDeviceToken', {
        //                 id: $('input[name="user_id"]').val(),
        //                 device_token: deviceId
        //             }).then(function (response) {
        //                 // console.log(response);
        //             }).catch(function (error) {
        //                 console.log(error);
        //             });
        //         }
        //     });
        // }
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
        window.removeEventListener("scroll", this.updateLogoOnScroll());
    }

    submitSmsAppLinkForm(e) {
        e.preventDefault();
        this.setState({loading: true});
        var self = this;
        var data = {};
        if ($('#smsAppLinkForm input[name="phoneNumber"]').val() === "") {
            toastr.options = {
                "progressBar": true,
                "positionClass": "toast-top-left",
                "timeOut": "5000",
            };
            toastr["error"]("لطفا شماره را وارد کنید.");
            return;
        }
        data.mobile = $('#smsAppLinkForm input[name="phoneNumber"]').val();
        data.url = 'http://online.sooryas.com/app/sooryas.apk';
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL+'/smsAppLink',
            type: 'POST',
            data: data,
            success: function (response) {
                self.setState({loading: false});
                toastr.options = {
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "timeOut": "5000",
                };
                toastr["success"](response.message);
            }
        });
    }

    render() {
        var content;
        var campaign;
        var joinUs_app;
        const app_background = {
            backgroundImage: "url('assets/images/pic-app.jpg')",
            backgroundSize: "contain",
            backgroundRepeat :"no-repeat",
            resize: "both"
        }
        if (this.state.width <= 768) {
            content =
                <>
                    <section>
                        <div className='values-courses row d-flex flex-column align-items-center'>
                            <div className='col-12 text-center pt-2'>
                                <span className='text-black'>چراسوریاس؟</span>
                                <hr/>
                                <div className='row justify-content-center values-vector'>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='جامع و کاربردی' src={'/assets/images/edu-cap.svg'}/>
                                        </LazyLoad>
                                        <span>جامع و کاربردی</span>
                                    </div>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='حامی و هدفمند' src={'/assets/images/umberlla.svg'}/>
                                        </LazyLoad>
                                        <span>حامی و هدفمند</span>
                                    </div>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='آسان و مطمئن' src={'/assets/images/shild.svg'}/>
                                        </LazyLoad>
                                        <span>آسان و مطمئن</span>
                                    </div>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='هوشمند و سریع' src={'/assets/images/rocket.svg'}/>
                                        </LazyLoad>
                                        <span>هوشمند و سریع</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div className='values-courses row d-flex flex-column align-items-center'>
                            <div className='row courses justify-content-center'>
                                <div className='col-12 text-center pt-2'>
                                    <span className='text-black'>محبوب ترین دوره ها</span>
                                    <hr/>
                                    <div className="courseCarousel">
                                        {/*<CoursesCarousel/>*/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </>;
            campaign =
                <>
                    <section>
                        <div className='row campaigns align-items-center'>
                            {
                                (this.state.offerData !== null)
                                    ?
                                    <>
                                        <LazyLoad>
                                            <img alt={'offer'} src={this.state.offerData.image} className='offer'/>
                                        </LazyLoad>
                                        <div className='offerData text-right'>
                                            <div
                                                className='border border-white rounded d-flex justify-content-between align-items-center'>
                                                <h4 className='mb-0 mr-2 text-white d-inline-block'>پیشنهاد شگفت
                                                    انگیز</h4>

                                                            <div className='p-2 ml-2 countDown text-white text-center'>
                                                    <CountdownTimer date={this.state.offerData.course.registerEndDate}/>
                                                    <span
                                                    className='text-white counterText px-1 d-inline-block'>ثانیه</span>
                                                    <span
                                                    className='text-white counterText px-1 d-inline-block'>دقیقه</span>
                                                    <span
                                                    className='text-white counterText px-1 d-inline-block'>ساعت</span>
                                                    <span
                                                    className='text-white counterText px-1 d-inline-block'>روز</span>
                                                            </div>





                                            </div>
                                            <h4 className='text-center text-white mt-sm-4 mt-2 text-nowrap overflow-hidden mt-1'>{this.state.offerData.course.title}</h4>
                                            <div className='p-1 text-white rounded shortInfo d-flex mt-sm-4 mt-1'>
                                                <div
                                                    className='col-3 border-left d-flex flex-column justify-content-center align-items-center p-2'>
                                                    <DateRangeIcon className='icon'/>
                                                    <span
                                                        className='d-block text-center text-nowrap mt-1'>تاریخ شروع دوره:</span>
                                                    <span
                                                        className='d-block text-center mt-1'>{this.state.offerData.course.course_start_date}</span>
                                                </div>
                                                <div
                                                    className='col-3 border-left d-flex flex-column justify-content-center align-items-center'>
                                                    <ContactsIcon className=' icon'/>
                                                    <span
                                                        className='d-block text-center text-nowrap mt-1'>تعداد جلسات:</span>
                                                    <span
                                                        className='d-block text-center mt-1'>{this.state.offerData.course.course_total_sessions}</span>
                                                </div>
                                                <div
                                                    className='col-3 border-left d-flex flex-column justify-content-center align-items-center'>
                                                    <TodayIcon className='icon'/>
                                                    <span
                                                        className='d-block text-center text-nowrap mt-1'>روزهای برگزاری:</span>
                                                    {
                                                        (this.state.offerData.course.formationDaysWeek.id === 1)
                                                            ?
                                                            <span
                                                                className='d-block text-nowrap mt-1'>{this.state.offerData.course.formationDays}</span>
                                                            :
                                                            <span
                                                                className='d-block text-nowrap mt-1'>{this.state.offerData.course.formationDaysWeek.title}</span>
                                                    }
                                                </div>
                                                <div
                                                    className='col-3 d-flex flex-column justify-content-center align-items-center'>
                                                    <AlarmIcon className='icon'/>
                                                    <span
                                                        className='d-block text-center text-nowrap mt-1'>زمان برگزاری:</span>
                                                    <span
                                                        className='d-block text-center text-nowrap mt-1'>{this.state.offerData.course.course_formation_start_time + ' الی ' + this.state.offerData.course.course_formation_end_time}</span>
                                                </div>
                                            </div>
                                            <Button className='col-12 mt-2' variant="outlined"
                                                    href={'/course/' + this.state.offerData.course.id}
                                                    color={"primary"} style={{
                                                'fontFamily': 'IRANSans',
                                                'borderColor': '#fff',
                                                'color': '#fff'
                                            }}
                                                    target="_blank"
                                            >
                                                اطلاعات بیشتر
                                            </Button>
                                        </div>
                                    </>
                                    :
                                    null
                            }
                        </div>
                    </section>
                    <section>
                        <div className='row campaigns align-items-center'>
                            {
                                (this.state.bannerData !== null)
                                    ?
                                    <a href={this.state.bannerData.link} className='banner'>
                                        <LazyLoad>
                                            <img alt={'banner'} src={this.state.bannerData.image}/>
                                        </LazyLoad>
                                    </a>
                                    :
                                    null
                            }
                        </div>
                    </section>
                </>;
            joinUs_app =
                <>
                   {/* <section>
                        <div className='row joinUs-app'>
                            <div className='col-sm-6 col-12 schoolsApp d-flex justify-content-end align-items-center'>
                                <LazyLoad>
                                    <img alt={'schoolsApp'} src={'/assets/images/schoolApp.png'}/>
                                </LazyLoad>
                                <Button className='schoolAppBtn p-2' variant="contained"
                                        href='https://cafebazaar.ir/app/com.nikaroschool'
                                        color={"primary"}
                                        style={{
                                            'fontFamily': 'IRANSans',
                                            'backgroundColor': '#17ace6',
                                            'color': '#fff'
                                        }}
                                        target="_blank"
                                >
                                    دانلود اپلیکیشن آموزشگاه
                                </Button>
                            </div>
                            <div
                                className='col-12 col-sm-6 text-white text-center joinUs d-flex flex-column align-items-center justify-content-sm-center justify-content-start pr-sm-5'>
                                <h2 className='animated delay-1s'>آموزشگاه هستید؟ به سوریاس بپیوندید.</h2>
                                <span className='d-block animated delay-1s'>پلتفرمسوریاس آماده همکاری با موسسات و آموزشگاه های سراسر کشور می باشد.</span>
                                <Button variant="contained"
                                        color={"primary"}
                                        href={'/landing/school'}
                                        style={{
                                            'fontFamily': 'IRANSans',
                                            'backgroundColor': '#17ace6',
                                            'color': '#fff',
                                            'fontSize': '20px'
                                        }}
                                        className='mt-4'
                                >
                                    ثبت آموزشگاه
                                </Button>
                            </div>
                        </div>
                    </section>*/}
                    <section>
                        <div className='row schools-carousel'>
                            <div className='col-12 text-center'>
                                <span className='text-black mt-4 d-block'>محبوب ترین آموزشگاه ها</span>
                                <hr/>
                                <SchoolCarousel/>
                            </div>
                        </div>
                    </section>
                </>
        } else {
            content =
                <section>
                    <div className='values-courses d-flex flex-column justify-content-center'>
                        <div className='row why-nikaro justify-content-center'>
                            <div
                                className='col-12 text-center d-flex flex-column justify-content-end align-items-center'>
                                <span className='text-black'>چراسوریاس؟</span>
                                <hr/>
                                <div className='row justify-content-center values-vector'>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='جامع و کاربردی' src={'/assets/images/edu-cap.svg'}/>
                                        </LazyLoad>
                                        <span>جامع و کاربردی</span>
                                    </div>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='حامی و هدفمند' src={'/assets/images/umberlla.svg'}/>
                                        </LazyLoad>
                                        <span>حامی و هدفمند</span>
                                    </div>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='آسان و مطمئن' src={'/assets/images/shild.svg'}/>
                                        </LazyLoad>
                                        <span>آسان و مطمئن</span>
                                    </div>
                                    <div className='col-sm-2 col-12'>
                                        <LazyLoad>
                                            <img alt='هوشمند و سریع' src={'/assets/images/rocket.svg'}/>
                                        </LazyLoad>
                                        <span>هوشمند و سریع</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='row courses justify-content-center'>
                            <div className='col-12 text-center'>
                                <span className='text-black'>محبوب ترین دوره ها</span>
                                <hr/>
                                <div className="courseCarousel">
                                    <CoursesCarousel/>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>;
            campaign =
                <section>
                    <div className='row campaigns align-items-center' id={'campaigns'}>
                        {
                            (this.state.offerData !== null)
                                ?
                                <>
                                    <LazyLoad>
                                        <img src={this.state.offerData.image} className='offer' alt='پبشنهاد ویژه'/>
                                    </LazyLoad>
                                    <div className='offerData text-right'>
                                        <div
                                            className='border border-white rounded d-flex justify-content-between align-items-center'>
                                            <h4 className='mb-0 mr-2 text-white d-inline-block'>پیشنهاد شگفت انگیز</h4>
                                            <div className='p-2 ml-2 countDown text-white text-center'>
                                             
                                                <CountdownTimer date={this.state.offerData.course.registerEndDate}/>
                                                <span
                                                    className='text-white counterText px-1 d-inline-block'>ثانیه</span>
                                                <span
                                                    className='text-white counterText px-1 d-inline-block'>دقیقه</span>
                                                <span className='text-white counterText px-1 d-inline-block'>ساعت</span>
                                                <span className='text-white counterText px-1 d-inline-block'>روز</span>
                                               
                                            </div>
                                        </div>
                                        <h4 className='text-center text-white mt-sm-4 mt-2 text-nowrap overflow-hidden'>{this.state.offerData.course.title}</h4>
                                        <div className='text-white p-1 rounded shortInfo d-flex mt-sm-4 mt-1'>
                                            <div
                                                className='col-3 border-left d-flex flex-column justify-content-center align-items-center p-2'>
                                                <DateRangeIcon className='icon'/>
                                                <span
                                                    className='d-block text-center text-nowrap'>تاریخ شروع دوره:</span>
                                                <span
                                                    className='d-block text-center'>{this.state.offerData.course.course_start_date}</span>
                                            </div>
                                            <div
                                                className='col-3 border-left d-flex flex-column justify-content-center align-items-center'>
                                                <ContactsIcon className='icon'/>
                                                <span className='d-block text-center text-nowrap'>تعداد جلسات:</span>
                                                <span
                                                    className='d-block text-center'>{this.state.offerData.course.course_total_sessions}</span>
                                            </div>
                                            <div
                                                className='col-3 border-left d-flex flex-column justify-content-center align-items-center'>
                                                <TodayIcon className='icon'/>
                                                <span className='d-block text-center text-nowrap'>روزهای برگزاری:</span>
                                                {
                                                    (this.state.offerData.course.formationDaysWeek.id === 1)
                                                        ?
                                                        <span
                                                            className='d-block text-nowrap'>{this.state.offerData.course.formationDays}</span>
                                                        :
                                                        <span
                                                            className='d-block text-nowrap'>{this.state.offerData.course.formationDaysWeek.title}</span>
                                                }
                                            </div>
                                            <div
                                                className='col-3 d-flex flex-column justify-content-center align-items-center'>
                                                <AlarmIcon className='icon'/>
                                                <span className='d-block text-center text-nowrap'>زمان برگزاری:</span>
                                                <span
                                                    className='d-block text-center text-nowrap'>{this.state.offerData.course.course_formation_start_time + ' الی ' + this.state.offerData.course.course_formation_end_time}</span>
                                            </div>
                                        </div>
                                        <Button className='col-12 mt-2' variant="outlined"
                                                href={'/course/' + this.state.offerData.course.id}
                                                color={"primary"} style={{
                                            'fontFamily': 'IRANSans',
                                            'borderColor': '#fff',
                                            'color': '#fff'
                                        }}
                                                target="_blank"
                                        >
                                            اطلاعات بیشتر
                                        </Button>
                                    </div>
                                </>
                                :
                                null
                        }
                        {
                            (this.state.bannerData !== null)
                                ?
                                <a href={this.state.bannerData.link} className='banner'>
                                    <LazyLoad>
                                        <img alt={'banner'} src={this.state.bannerData.image}/>
                                    </LazyLoad>
                                </a>
                                :
                                null
                        }
                    </div>
                </section>
            joinUs_app =''
             /*   <section>
                    <div className='row joinUs-app'>
                        <LazyLoad>
                            <img alt={'joinUs'} src={'/assets/images/hand-shake.jpg'}
                                 className='handShake'/>
                        </LazyLoad>
                        <div className='col-sm-6 col-12 schoolsApp d-flex justify-content-end align-items-center'>
                            <LazyLoad>
                                <img alt={'schoolApp'} src={'/assets/images/schoolApp.png'}
                                     className='animated schoolAppImg'/>
                            </LazyLoad>
                            <Button className='schoolAppBtn p-3' variant="contained"
                                    href='https://cafebazaar.ir/app/com.nikaroschool'
                                    style={{
                                        'fontFamily': 'IRANSans',
                                        'backgroundColor': '#17ace6',
                                        'color': '#fff'
                                    }}
                                    target="_blank"
                            >
                                دانلود اپلیکیشن آموزشگاه
                            </Button>
                        </div>
                        <div
                            className='col-12 col-sm-6 text-white text-center joinUs d-flex flex-column align-items-center justify-content-sm-center justify-content-start pr-sm-5'>
                            <h2 className='animated delay-1s'>آموزشگاه هستید؟ به سوریاس بپیوندید.</h2>
                            <span className='d-block animated delay-1s'>پلتفرمسوریاس آماده همکاری با موسسات و آموزشگاه های سراسر کشور می باشد.</span>
                            <Button variant="contained"
                                    color={"primary"}
                                    href={'/landing/school'}
                                    style={{
                                        'fontFamily': 'IRANSans',
                                        'backgroundColor': '#17ace6',
                                        'color': '#fff',
                                        'fontSize': '20px'
                                    }}
                                    className='mt-4'
                            >
                                ثبت آموزشگاه
                            </Button>
                        </div>
                    </div>
                    <div className='row schools-carousel'>
                        <div className='col-12 text-center'>
                            <span className='text-black mt-4 d-block'>محبوب ترین آموزشگاه ها</span>
                            <hr/>
                            <SchoolCarousel/>
                        </div>
                    </div>
                </section>*/
        }
        return (
            <>
                <Head>
                    <title>سامانه معرفی و ثبت نام کلاس های آموزشی |سوریاس</title>
                    <meta name="description" property="description" content="درسوریاس می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:url" content="https://nikaro.ir"/>
                    <meta property="og:title" content="موسسه علمی آموزشی فرهنگی سوریاس مهر |سوریاس"/>
                    <meta property="og:description"
                          content="در موسسه علمی آموزشی فرهنگی سوریاس مهر می توانید به بهترین و مناسب ترین کلاس های آموزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image"
                          content="assets/images/logoWithBg.png"/>
                    <meta property="og:site_name" content=""/>
                    <meta name="twitter:card" content="summary" />
                    <meta name="twitter:site" content="@sooryas.com" />
                    <meta name="twitter:title" content="سامانه معرفی و ثبت نام کلاس های آموزشی | موسسه علمی آموزشی فرهنگی سوریاس مهر" />
                    <meta name="twitter:description" content="در موسسه علمی آموزشی فرهنگی سوریاس مهر می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید." />
                    <link rel="canonical" href="https://sooryas.com"/>
                </Head>
                <div id={'home'}>
                    <section>
                        <div className={'intro row'}>
                            {
                                (this.state.width !== null)
                                    ?
                                    <>
                                        <div
                                            className={(this.state.width > 575 && this.state.orientation === 'landscape') ? 'text col-sm-6 d-flex justify-content-center align-items-start' : 'text col-12 pb-5 d-flex justify-content-center align-items-start'}>
                                            <h1 className='h4 d-block animated fadeIn delay-1s mx-auto'
                                                style={{'lineHeight': '40px'}}>مدرسه هستید یا آموزشگاه ؟ مدرس هستید یا فراگیر ؟ آموزشگاه آنلاین سوریاس جایی برای شماست</h1>
                                            <form
                                                className={this.state.orientation === 'landscape' ? 'justify-content-center text-center' : 'justify-content-center text-center'}
                                                action={'/search'}
                                                id='autoSuggestForm'>
                                                <SearchAutoSuggest/>
                                            </form>
                                        </div>
                                        <div
                                            className={(this.state.width > 575 && this.state.orientation === 'landscape') ? 'vector col-sm-6 d-flex align-items-center' : 'vector col-12 order-first d-flex align-items-center justify-content-center'}>
                                            <LazyLoad>
                                                <img alt={'nikaro'} src={'/assets/images/pic-left-suggestion-box.png'}/>
                                            </LazyLoad>
                                        </div>
                                    </>
                                    :
                                    null
                            }
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.36 60" className='scrollSvg'
                                 onClick={handleScrollToCampaigns}>
                                <g>
                                    <g>
                                        <rect x="0.7" y="0.7" width="21.97" height="38.51" rx="10.98" ry="10.98"/>
                                        <line x1="11.68" y1="11.61" x2="11.68" y2="16.36"/>
                                        <polyline className="chevron" points="7.13 46.05 11.85 50.76 16.4 46.2"/>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </section>
                    {campaign}
                    {content}
                    {joinUs_app}
                    <section>
                        <div className='row app align-items-center'>
                            <LazyLoad>
                                <img alt={'pattern'} className={'pattern'}
                                     src={'/assets/images/app-pattern.svg'}/>
                            </LazyLoad>
                            <div
                                className='col-12 col-sm-6 aboutNikaro d-flex flex-column justify-content-sm-center justify-content-start align-items-center order-2 order-sm-1 pt-4 pt-sm-0'>

                                 <p className='text-justify'>
                                          آموزشگاه دارید؟
      <br />
مدرسه دارید؟
      <br />
مدرس هستید؟
      <br />
دانش آموز هستید؟
      <br />
دانشجو هستید؟
      <br />
      <br />
      <b>به سوریاس بپوندید</b>
                                </p>
                              
                            </div>
                            <div
                                className='col-12 col-sm-6 vector d-flex flex-column justify-content-center align-items-center order-1 order-sm-2'>
                               <LazyLoad>
                                    <img alt={'app'} className='animated'
                                         src={'/assets/images/app.png'}/>
                                </LazyLoad>
                                <span className='text-large d-block'>دانلود اپلیکیشن سوریاس</span>
                                <span
                                    className='d-block'>برای دریافت اپلیکیشن سوریاس, شماره همراه خود را وارد کنید.</span>
                                <form id='smsAppLinkForm' onSubmit={this.submitSmsAppLinkForm.bind(this)}>
                                    <div className="form-group">
                                        <input className='form-control' type='number' placeholder={'مثال: 1313****0936'}
                                               name='phoneNumber'/>
                                        {
                                            (this.state.loading)
                                                ?
                                                <img alt={'loading'} className='ml-2'
                                                     src={'/assets/images/loading.svg'}
                                                     style={{'width': '5%'}}/>
                                                :
                                                <img alt={'download'} src={'/assets/images/download.svg'}
                                                     onClick={this.submitSmsAppLinkForm.bind(this)}/>
                                        }

                                    </div>
                                    <a href="https://cafebazaar.ir/app/ir.sooryas" className='text-nowrap'>
                                        <button  type="button" className='btn text-white  float-md-right'>
                                            دریافت از کافه بازار
                                            <img alt={'bazar'} src={'/assets/images/bazar.png'}/>
                                        </button>
                                    </a>
                                    <a href="/app/sooryas.apk" className='text-nowrap'>
                                        <button  type="button" className='btn text-white float-md-left mr-2'>
                                            دانلود مستقیم
                                            <img alt={'download'} src={'/assets/images/download.png'}/>
                                        </button>
                                    </a>
                                </form>
                            </div>
                        </div>
                    </section>
                    {/*<section className='tagsSection'>
                        <div className='row tags align-items-center'>
                            <div
                                className='col-12 d-flex justify-content-sm-between justify-content-center align-items-center flex-wrap mt-2'>
                                {this.state.categories}
                            </div>
                        </div>
                    </section>*/}
                </div>
            </>
        );
    }
}