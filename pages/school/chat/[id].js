import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ChatBot from 'react-simple-chatbot';
import {ThemeProvider} from 'styled-components';
import axios from 'axios';
import $ from 'jquery'
import {useRouter} from "next/router";
import Loading from "../../../components/loading";
import useAuth from "../../../services/contexts/AuthContext";
import Cookies from "js-cookie";


// all available props
const theme = {
    background: '#f5f8fb',
    fontFamily: 'IRANSans',
    headerBgColor: '#e40800',
    headerFontColor: '#fff',
    headerFontSize: '15px',
    botBubbleColor: '#e40800',
    botFontColor: '#fff',
    userBubbleColor: '#fff',
    userFontColor: '#4a4a4a',
};

class Review extends Component {
    constructor(props) {
        super(props);

        this.state = {
            save: 0,
            content: '',
        };
    }

    componentWillMount() {
        const {steps} = this.props;
        const {content} = steps;
        this.setState({content});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.save !== this.state.save && typeof nextProps.save !== "undefined") {
            this.setState({
                save: nextProps.save
            });
            var self = this;
            const token = Cookies.get('token');
            axios.defaults.headers.Authorization = `Bearer ${token}`;
            axios({
                method: 'post',
                url: 'http://nikaro.co/api/web/helpdesk/save',
                data: {
                    'typeId': nextProps.schoolId,
                    'textBody': this.state.content.value,
                    'title': nextProps.title,
                    'end': (!!nextProps.end)
                }
            }).then(function (response) {
                console.log(response)
            });
        }
    }

    render() {
        const {content} = this.state;
        return (
            <>
                {/*{*/}
                {/*(this.props.end)*/}
                {/*?*/}
                {/*<div style={{width: '100%'}}>*/}
                {/*<span>پیام شما:</span>*/}
                {/*<p>*/}
                {/*{content.value}*/}
                {/*</p>*/}
                {/*</div>*/}
                {/*:*/}
                {/*null*/}
                {/*}*/}

            </>
        );
    }
}

Review.propTypes = {
    steps: PropTypes.object,
};

Review.defaultProps = {
    steps: undefined,
};

class SchoolChat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            schoolId: null,
            schoolData: null,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.id !== this.state.schoolId) {
            var self = this;
            axios({
                method: 'get',
                url: 'http://nikaro.co/search?schoolId=' + nextProps.id,
            }).then(function (response) {
                self.setState({
                    schoolId: nextProps.id,
                    schoolData: response.data[0][0]
                })
            });
        }
    }

    render() {
        console.log(this.props.auth);
        var date = new Date();
        var timestamp = date.getTime();
        return (
            (this.props.auth.loading)
                ?
                <Loading/>
                :
                (this.props.auth.isAuthenticated)
                    ?
                    (this.state.schoolData !== null)
                        ?
                        <div className='row justify-content-center align-items-center bg-nikaro schoolChat'>
                            <div className='col-12 col-sm-7'>
                                <ThemeProvider theme={theme}>
                                    <ChatBot
                                        width={'100%'}
                                        headerTitle={this.state.schoolData.title}
                                        placeholder={'اینجا تایپ کنید...'}
                                        steps={[
                                            {
                                                id: '1',
                                                message: 'سلام چطور میتونم کمکتون کنم؟',
                                                trigger: 'content',
                                            },
                                            {
                                                id: 'content',
                                                user: true,
                                                trigger: '2',
                                            },
                                            {
                                                id: '2',
                                                message: 'سپاس، پیام شما دریافت شد.',
                                                trigger: 'temp',
                                            },
                                            {
                                                id: 'temp',
                                                component: <Review/>,
                                                trigger: 'update'
                                            },
                                            {
                                                id: 'update',
                                                message: 'پیام بیشتر؟',
                                                trigger: 'update-question',
                                            },
                                            {
                                                id: 'update-question',
                                                options: [
                                                    {value: 'yes', label: 'بله', trigger: 'save'},
                                                    {value: 'no', label: 'خیر', trigger: 'review-save'},
                                                ],
                                            },
                                            {
                                                id: 'save',
                                                component: <Review save={timestamp} schoolId={this.state.schoolData.id}
                                                                   title={this.state.schoolData.title}/>,
                                                trigger: '3'
                                            },
                                            {
                                                id: '3',
                                                message: 'پیام خود را وارد کنید',
                                                trigger: 'content'
                                            },
                                            {
                                                id: 'review-save',
                                                component: <Review save={timestamp} schoolId={this.state.schoolData.id}
                                                                   title={this.state.schoolData.title} end={true}/>,
                                                trigger: 'end-message',
                                            },
                                            {
                                                id: 'end-message',
                                                message: 'متشکریم. پیام شما با موفقیت ثبت شد. آموزشگاه در اسرع وقت پاسخگو خواهد بود و از طریق پیامک به شما اطلاع رسانی خواهد شد.',
                                                end: true,
                                            },
                                        ]}
                                    />
                                </ThemeProvider>
                            </div>
                        </div>
                        :
                        <Loading/>
                    :
                    <div className='row justify-content-center align-items-center bg-nikaro schoolChat'>
                        <h3 className='text-white d-block'>برای مشاهده این بخش باید وارد شوید.</h3>
                        {/*{*/}
                        {/*    setTimeout($('#loginRegisterModal').modal('show'), 2000)*/}
                        {/*}*/}
                    </div>
        );
    }
}

export default function Chat(props) {
    const router = useRouter()
    const {id} = router.query
    return <SchoolChat {...props} id={id} auth={useAuth()}/>
}
