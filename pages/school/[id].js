import React, {Component} from 'react';
import axios from 'axios';
import SchoolPageGalleryCarousel from "../../components/schoolPageGallery-carousel";
import SchoolPageCoursesCarousel from "../../components/schoolPageCourses-carousel";
import OtherSchoolsCarousel from "../../components/otherSchools-carousel";
import Comments from "../../components/comments";
import ShowRatingBar from "../../components/showRatingBar";
import ShowRatingCircle from "../../components/showRatingCircle";
import Divider from '@material-ui/core/Divider';
import StarIcon from '@material-ui/icons/Star';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import Avatar from '@material-ui/core/Avatar';
import RoomIcon from '@material-ui/icons/Room';
import PhoneInTalkIcon from '@material-ui/icons/PhoneInTalk';
import LanguageIcon from '@material-ui/icons/Language';
import ShareIcon from '@material-ui/icons/Share';
import InstagramIcon from '@material-ui/icons/Instagram';
import TelegramIcon from '@material-ui/icons/Telegram';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Map from '../../components/map';
import Button from "@material-ui/core/Button";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LinkIcon from '@material-ui/icons/Link';
import VisibilityIcon from '@material-ui/icons/Visibility';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import Tooltip from '@material-ui/core/Tooltip';
import Loading from "../../components/loading";
import $ from 'jquery'
import Head from "next/head";
import Cookies from "js-cookie";
import {ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export async function getServerSideProps(context) {

    // Fetch data from external API
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + '/search?schoolId=' + context.query.id,
    {
        method: "GET",
            headers:
        {
            Accept: "application/json; charset=UTF-8",
        }
    });

    const data = await res.json();

    // Pass data to the page via props
    return {props: {"data": data}}
}

const handleScrollToCarousel = () => {
    window.scrollTo({
        top: $('.courseSchool-carousel').offset().top - 200,
        behavior: 'smooth'
    })
};

const handleScrollToDescription = () => {
    window.scrollTo({
        top: $('.tab').offset().top - 100,
        behavior: 'smooth'
    })
};

function splitString(str, s) {
    if (str !== null) {
        var temp = str.split('-');
        var middle = Math.ceil(temp[0].length / 2);
        var s1 = temp[0].slice(0, middle);
        var s2 = temp[0].slice(middle);
        for (var i = 1; i < temp.length; i++) {
            s2 += '-' + temp[i];
        }
    }

    if (s === 1) {
        return s1;
    } else if (s === 2) {
        return s2;
    }
}

export default class School extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false,
            schoolId: props.query.id,
            data: props.data[0][0],
            userId: Cookies.get('token'),
            lidsData: null,
            carousel: null,
            rateData: null,
            totalAvg: 0,
            value: 0,
            isAware: false,
            gallery: null,
            comments: null,
            videos: null,
            awareIcon: <FavoriteBorderIcon className='text-danger ml-2'/>,

        };
    }

    componentDidMount() {

        $('#number').click(function () {
            $(this).find('span').text($(this).data('lost'));
        });

        var self = this;
        var content = [];
        if (this.state.data !== "undefined") {
            if (this.state.data.lids != null) {
                this.state.data.lids.map(item =>
                    content.push(
                        <div className='col-12 col-sm-6 school-property text-right d-flex flex-column my-2'>
                            {
                                (typeof item.checkboxes !== "undefined" && Object.entries(item.checkboxes).length !== 0)
                                    ?
                                    <>
                                        <strong className='d-block mb-2'>{item.label}</strong>
                                        <Divider/>
                                        <div
                                            className='items d-flex justify-content-start align-items-center flex-wrap'>
                                            {
                                                item.checkboxes.map(e =>
                                                        <span className='m-2 py-2'>
                                                    {
                                                        (e.checked)
                                                            ?
                                                            <CheckCircleIcon className='text-success'/>
                                                            :
                                                            <CancelIcon className='text-danger ml-1'/>
                                                    }
                                                            {e.label}
                                                </span>
                                                )
                                            }
                                        </div>
                                    </>
                                    :
                                    null
                            }
                        </div>
                    )
                );
                self.setState({
                    lidsData: content,
                })
            }
            var carousel;
            if (this.state.data.haveCourse) {
                carousel = <SchoolPageCoursesCarousel schoolId={self.state.schoolId}/>
            } else {
                carousel =
                    <OtherSchoolsCarousel schoolId={self.state.schoolId}
                                          categoryId={this.state.data.category_id}/>
            }
            self.setState({
                data: this.state.data,
                isLogin: ($('input[name="user_id"]').val() > 0),
                carousel: carousel,
            })

        } else {
            self.setState({
                data: {id: '404'},
                isLogin: ($('input[name="user_id"]').val() > 0),
            })
        }
        //send user tracking activity
        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/usersTractiongActivity/save',
            type: "GET",
            data: {'subject': 'school', 'url':window.location.href,'pathName':window.location.pathname},
            success: function (result) {

            }
        });

        $.ajax({
            url: process.env.NEXT_PUBLIC_API_URL + '/rates/getList',
            type: "POST",
            data: {'type': 'school', 'type_id': this.state.schoolId},
            success: function (result) {
                var rates = [];
                var temp = 0;
                if (typeof result.data != "undefined") {
                    result.data.map(item => {
                        temp += item.avg / 5;
                        rates.push(
                            <span
                                className='d-flex justify-content-between align-items-start align-items-sm-center position-relative flex-column flex-sm-row'>
                            <label className='d-inline-block'>{item.title}</label>
                            <ShowRatingBar id={item.id} rate={item.avg / 5}/>
                        </span>
                        );
                    });
                    temp = temp / result.data.length;
                    self.setState({
                        totalAvg: <ShowRatingCircle rate={temp}/>,
                        rateData: rates
                    });
                }
            }
        });

        axios({
            method: 'get',
            url: process.env.NEXT_PUBLIC_API_URL + '/nikarobank/getSchoolGallery?id=' + this.state.schoolId,
        }).then(function (response) {
            var videos = [];
            if (typeof response.data[0] !== "undefined")
                response.data[0].forEach(function (e, index) {
                    videos.push(
                        <>
                            <video controls key={index} className='my-3'>
                                <source src={e}/>
                            </video>
                            <Divider/>
                        </>
                    )
                });
            self.setState({
                gallery: <SchoolPageGalleryCarousel data={response.data[1]}/>,
                videos: videos,
            });
        });
    }

    showLostNumber() {
        if (typeof this.state.userId !=="undefined") {
            $('.showPhoneBtn').hide();
            $('.phone').find('.lostData').text($('.phone').data('lost'));
        } else {
            // toastr.options = {
            //     "progressBar": true,
            //     "positionClass": "toast-top-center",
            //     "timeOut": "5000",
            // };
            // toastr['error']("برای مشاهده این قسمت ابتدا باید وارد شوید");
          //  alert("برای مشاهده این قسمت ابتدا باید وارد شوید");
            toast.error("برای مشاهده این قسمت ابتدا باید وارد شوید",{
                "postion":toast.POSITION.TOP_CENTER

            })
            $('#loginRegisterModal').modal('show');
        }
    }

    handleTabChange(event, newValue) {
        this.setState({
            value: newValue
        });
    }

    handleAwareHoverIn() {
        if (this.state.data.isAware === 0 && !this.state.isAware) {
            this.setState({
                awareIcon: <FavoriteIcon className='text-danger ml-2'/>
            })
        }
    }

    handleAwareHoverOut() {
        if (this.state.data.isAware === 0 && !this.state.isAware) {
            this.setState({
                awareIcon: <FavoriteBorderIcon className='text-danger ml-2'/>
            })
        }
    }

    handleAwaresSubmit() {
        if (typeof this.state.userId !== "undefined") {
            options = {
                "posistion": toast.POSITION.TOP_CENTER,

            };
            toast.error("برای مشاهده این قسمت ابتدا باید وارد شوید",{
                "postion":toast.POSITION.TOP_CENTER

            })
            $('#loginRegisterModal').modal('show');
        } else {
            var self = this;
            $.ajax({
                url: process.env.NEXT_PUBLIC_API_URL + '/awares/save',
                type: "POST",
                data: {'type': 'school', 'id': this.state.schoolId},
                success: function (result) {
                    self.setState({
                        isAware: true,
                        awareIcon: <FavoriteIcon className='text-danger ml-2'/>
                    });
               /*     toastr.options = {
                        "progressBar": true,
                        "positionClass": "toast-top-left",
                        "timeOut": "5000",
                    };
                    toastr['success'](result.message);*/
                    toast.success(result.message,{
                        "postion":toast.POSITION.TOP_CENTER

                    })
                }
            });
        }
    }

    render() {
        let desc;
        if (this.state.data.description != null) {
            desc = this.state.data.description.replace(/<[^>]+>|&nbsp;|&ndash;|&amp;|zwnj;/g, '').replace(/^\s*[\r\n]/gm, '');
        } else {
            desc = 'در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید.';
        }

        var items;
        if (this.state.data.id && this.state.data.id !== '404') {
            if (this.state.data.tags !== null) {
                var tags = this.state.data.tags.split('-');
                items = tags.map((e) => {
                    return <Button href={'/result-page?searchInput=' + e}
                                   className='border border-dark p-2 px-4 mx-2 rounded-pill my-2'
                                   style={{'fontFamily': 'IRANSans'}}><LinkIcon className='ml-1'/>{e}</Button>
                });
            } else {
                items = null;
            }
            return (
                <>
                    <ToastContainer
                        rtl
                       style={{
                           "text-align":"right"
                       }}
                    />
                    <Head>
                        <title>{this.state.data.title}</title>
                        <meta name="description" property="description" content={desc}/>
                        <meta property="og:url" content={"https://nikaro.ir" + this.props.asPath}/>
                        <meta property="og:title" content={this.state.data.title}/>
                        <meta property="og:description" content={desc}/>
                        <meta property="og:type" content="website"/>
                        <meta property="og:image" content={this.state.data.schoolPic}/>
                        <meta property="og:site_name" content="نیکارو"/>
                        <meta name="twitter:card" content="summary"/>
                        <meta name="twitter:site" content="@nikaro_ir"/>
                        <meta name="twitter:title" content={this.state.data.title}/>
                        <meta name="twitter:description" content={desc}/>
                        <link rel="canonical" href={"https://nikaro.ir" + this.props.asPath}/>
                    </Head>
                    <div id={'schoolPage'}>
                        <div className='row schoolGallery'>
                            <div className='col-12 schoolGalleryCarousel p-0'>
                                {this.state.gallery}
                            </div>
                        </div>
                        <div className='row info position-relative my-4'>
                            <div className='col-sm-4 col-5 pl-0 d-flex justify-content-start'>
                                {
                                    (this.state.data.haveCourse)
                                        ?
                                        <span
                                            className='activeCourseStamp bg-nikaro border-dark text-white d-flex align-items-center justify-content-center text-nowrap'
                                            onClick={handleScrollToCarousel}>
                                    <StarIcon className='text-white ml-2'/>
                                    دوره فعال در نیکارو دارد
                                </span>
                                        :
                                        <span
                                            className='activeCourseStamp bg-dark border-dark text-white d-flex align-items-center justify-content-center text-nowrap'>
                                    <StarIcon className='text-white ml-2'/>
                                    دوره فعال در نیکارو ندارد
                                </span>
                                }
                            </div>
                            <div className='col-sm-4 col-2 text-center d-flex justify-content-center'>
                                <Avatar
                                    src={(this.state.data.defaultPic) ? '/assets/images/logo-sign-disable.svg' : this.state.data.schoolPic}
                                    className={(this.state.data.defaultPic) ? 'schoolLogo default' : 'schoolLogo bg-white'}
                                    alt={'avatar'}
                                    title={'avatar'}
                                />
                            </div>
                            <div className='col-sm-4 col-5 pr-0 d-flex justify-content-end'>
                                {
                                    (this.state.data.conversationBoxHidden)
                                        ?
                                        <Tooltip
                                            title="حساب کاربری ویژه آموزشگاه فعال نشده است.لطفا از دکمه پایین سمت راست سایت، سوالات خود را از پشتیبانی نیکارو بپرسید."
                                            className='chatTooltip'
                                            enterTouchDelay={0}

                                        >
                                        <span>
                                                <Button href={"/school/chat/" + this.state.schoolId}
                                                        disabled
                                                        aria-label="test"
                                                        style={{'fontFamily': 'IRANSans'}}
                                                        className='addAwareStamp addAwareBtn d-flex justify-content-center align-items-center border border-secondary text-nowrap'>
                                                    <QuestionAnswerIcon className='ml-3 text-primary'/>
                                                    چت با آموزشگاه
                                                </Button>
                                        </span>
                                        </Tooltip>
                                        :
                                        <Button href={"/school/chat/" + this.state.schoolId}
                                                style={{'fontFamily': 'IRANSans'}}
                                                className='addAwareStamp addAwareBtn d-flex justify-content-center align-items-center border border-secondary text-nowrap'>
                                            <QuestionAnswerIcon className='ml-3 text-primary'/>
                                            چت با آموزشگاه
                                        </Button>
                                }
                            </div>
                            <div className='col-12 col-md-6 details text-right mt-4 pl-md-4'>
                                <h1 className='text-black'>{this.state.data.title}
                                    <span className='float-left'>
                                    <span onClick={this.handleAwaresSubmit.bind(this)}
                                          onMouseEnter={this.handleAwareHoverIn.bind(this)}
                                          onMouseLeave={this.handleAwareHoverOut.bind(this)}
                                    >
                                    {this.state.awareIcon}
                                    </span>
                                <VisibilityIcon className='ml-2'/>
                                        {this.state.data.seenCounter}
                            </span>
                                </h1>
                                <Divider/>
                                <span className='d-block my-2'><span className='text-primary'>زمینه فعالیت: </span><h2
                                    className="d-inline-block">{this.state.data.category}</h2></span>
                                <Divider/>
                                <span className='d-block my-2'><span
                                    className='text-primary'>مدیریت آموزشگاه: </span>{this.state.data.manager}</span>
                                <Divider/>
                                <div className='my-2 shortDesc'
                                     dangerouslySetInnerHTML={{__html: this.state.data.description}}/>
                                <span className='d-block text-primary text-center' style={{'cursor': 'pointer'}}
                                      onClick={handleScrollToDescription.bind(this)}>
                                بیشتر
                                <KeyboardArrowDownIcon className='text-primary mr-2'/>
                            </span>
                            </div>
                            <div className='col-12 col-md-6 address text-right mt-4 pr-md-4'>
                                <div className='map border border-secondary rounded mb-2' id='map'>
                                    <Map lat={this.state.data.lat}
                                         lng={this.state.data.lng}
                                         schoolPage={true}
                                    />;
                                </div>
                                <span className='d-block my-2'>
                            <RoomIcon className='ml-2'/>
                                    {(this.state.data.region !== null) ? this.state.data.region + " - " : null}
                                    {
                                        (typeof this.state.userId !== "undefined")
                                            ?
                                            this.state.data.address
                                            :
                                            <span className='text-primary' style={{'cursor': 'pointer'}}
                                                  data-toggle='modal'
                                                  data-target='#loginRegisterModal'> ( نمایش کامل ) </span>
                                    }
                            </span>
                                <span className='d-block my-2'>
                                <PhoneInTalkIcon className='ml-2'/>
                                <span className='phone' data-lost={splitString(this.state.data.phone, 2)}>
                                    {splitString(this.state.data.phone, 1)}
                                    <span className='lostData'>******</span>
                                </span>
                                <span onClick={this.showLostNumber.bind(this)} className='text-primary showPhoneBtn'
                                      style={{'cursor': 'pointer'}}>
                                    (نمایش کامل)
                                </span>
                            </span>
                                <span className='d-block my-2'>
                            <LanguageIcon className='ml-2'/>
                            <a href={this.state.data.website} target="_blank">
                                {this.state.data.website}
                            </a>
                        </span>
                                <span className='mr-0 ml-2 my-2'>
                                {
                                    (this.state.data.instagram !== "")
                                        ?
                                        <a href={'https://instagram.com/' + this.state.data.instagram}>
                                            <InstagramIcon className='text-primary'/>
                                        </a>
                                        :
                                        <InstagramIcon className='text-dark'/>
                                }
                            </span>
                                <span className='m-2'>
                                {
                                    (this.state.data.telegram !== "")
                                        ?
                                        <a href={'https://t.me/' + this.state.data.telegram}>
                                            <TelegramIcon className='text-primary'/>
                                        </a>
                                        :
                                        <TelegramIcon className='text-dark'/>
                                }
                            </span>
                                <span className='m-2'>
                                <a title='اشتراک گزاری'
                                   href={'https://telegram.me/share/url?url=https://nikaro.ir/' + this.state.data.shortLink}>
                                    <ShareIcon className='text-primary'/>
                                </a>

                        </span>
                            </div>
                            {
                                (this.state.rateData != null)
                                    ?
                                    <>
                                        <Divider className='col-12 my-4 rateDivider'/>
                                        <div
                                            className={(true) ? 'col-12 col-sm-6 questions d-inline-block text-right' : 'col-12 col-sm-6 overallRate d-flex justify-content-center align-items-center mt-4 mt-sm-0 filter'}>
                                            {this.state.rateData}
                                        </div>
                                        <div
                                            className={(true) ? 'col-12 col-sm-6 overallRate d-flex justify-content-center align-items-center mt-4 mt-sm-0' : 'col-12 col-sm-6 overallRate d-flex justify-content-center align-items-center mt-4 mt-sm-0 filter'}>
                                            <label className='ml-4'>امتیاز کلی:</label>
                                            {this.state.totalAvg}
                                        </div>

                                    </>
                                    :
                                    null
                            }

                            {
                                (this.state.lidsData != null)
                                    ?
                                    <>
                                        <Divider className='col-12 my-4'/>
                                        <div
                                            className={(true) ? 'col-12 schoolProperties mt-2' : 'col-12 schoolProperties mt-2 filter'}>
                                            <div className='row'>
                                                {this.state.lidsData}
                                            </div>
                                        </div>
                                    </>
                                    :
                                    null
                            }

                        </div>
                        <Divider className='col-12 mt-4'/>
                        <div className='row courseSchool-carousel'>
                            {this.state.carousel}
                        </div>
                        <div className='row tab justify-content-center'>
                            <Paper className='col-12'>
                                <Tabs
                                    value={this.state.value}
                                    onChange={this.handleTabChange.bind(this)}
                                    indicatorColor="primary"
                                    centered
                                >
                                    <Tab label="توضیحات تکمیلی" style={{'width': '33%', 'fontFamily': 'IRANSans'}}
                                         className='text-primary'/>
                                    <Tab label="نظرات کاربران" style={{'width': '33%', 'fontFamily': 'IRANSans'}}
                                         className='text-primary'/>
                                    <Tab label="برچسب ها" style={{'width': '33%', 'fontFamily': 'IRANSans'}}
                                         className='text-primary'/>
                                </Tabs>
                            </Paper>
                            {
                                (this.state.value === 0)
                                    ?
                                    <div className='col-12 completeInfo text-center'>
                                        <div id='video'>

                                        </div>
                                        {this.state.videos}
                                        <div className='my-2 p-4 text-right'
                                             dangerouslySetInnerHTML={{__html: this.state.data.description}}>
                                        </div>
                                    </div>
                                    :
                                    (this.state.value === 1)
                                        ?
                                        <div className='col-12 comments'>
                                            <Comments type={'school'} id={this.state.schoolId}/>
                                        </div>
                                        :
                                        <div className='p-4 d-flex flex-wrap justify-content-center '>
                                            {items}
                                        </div>
                            }
                        </div>
                    </div>
                </>
            )
        } else if (this.state.data.id !== "404") {
            return (
                <div id='schoolPage'>
                    <Loading/>
                </div>
            )
        }
    }
}
