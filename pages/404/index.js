import React from 'react';

export default function Page404() {
    return (
        <div className="row justify-content-center align-items-center flex-column" id='page404'>
            <img className="img-fluid d-block" src={'/assets/images/404.png'}/>
            <h2 className='d-block text-white mt-4'>صفحه موردنظر یافت نشد!</h2>
        </div>
    );
}