import React from 'react';
import api from "../../services/api";


class ShortLink extends React.Component {
  static async getInitialProps(ctx) {
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL+'/shortLinks/isShortLink?code='+ctx.query.code);
    const json = await res.json();
    console.log(json)
    if(json.status=='success'){
       // console.log(json.link);
        ctx.res.writeHead(301, { Location: json.link }).end();
    }else{
        ctx.res.writeHead(301, { Location: '/404' }).end();
    }
  
  }

  render() {
    return <div></div>
  }
}

export default ShortLink;
