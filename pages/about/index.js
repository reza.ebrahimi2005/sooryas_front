import React, {Component} from 'react';
import Head from 'next/head'

export default class About extends Component {
    render() {
        return (
            <>
                <Head>
                    <title>درباره ما | نیکارو</title>
                    <meta name="description" property="description" content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:url" content="https://nikaro.ir/about"/>
                    <meta property="og:title" content="درباره ما | نیکارو"/>
                    <meta property="og:description"
                          content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image"
                          content="https://nikaro.ir/assets/images/nikaroWithBg.png"/>
                    <meta property="og:site_name" content="نیکارو"/>
                    <meta name="twitter:card" content="summary" />
                    <meta name="twitter:site" content="@nikaro_ir" />
                    <meta name="twitter:title" content="درباره ما | نیکارو" />
                    <meta name="twitter:description" content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید." />
                    <link rel="canonical" href="https://nikaro.ir/about"/>
                </Head>
                <div className="row justify-content-center" id='aboutUs'>
                    <img src='/assets/images/aboutUs.gif' className='header'/>
                    <div className='col-12 col-sm-6 mt-4 title text-justify'>
                        <p>
                            دو سال پیش تنها یک ایده بود مثل یک قاصدک که بر دوش کودکی نشسته باشد.
                        </p>
                        <p>
                            در ابتدا شاید تنها دلیل ما برای جمع شدن کنار هم، ساختن فضایی آنلاین بود برای والدین. خواست
                            ما این بود که بتوانند فرزندانشان را هدفمندتر به سمت یادگیری هدایت کنند تا مهارت هایی که می
                            آموزند کاربردی و هدفمند باشد.
                        </p>
                        <p>
                            اما بعد از مدتی متوجه شدیم که این نیاز تنها متعلق به کودکان نیست،چون این نیاز را در راننده
                            تاکسی، همکلاسی دانشگاه و حتی خانواده خودمان را هم دیدیم. پس تصمیمی بزرگ گرفتیم و بازارمان را
                            برای همه ی متقاضیان گسترش دادیم. رفته رفته کنار شما رشد کردیم، بزرگ شدیم و حالا مرجع معرفی و
                            ثبت نام کلاسهای آموزشی هستیم.
                            اما این تمام خواسته ما نبوده و نیست.
                        </p>
                        <p>
                            نارضایتی در ثبت نام به روش سنتی از موضوعات مهم دیگر بود، چالشی که بسیار دیده میشد .
                        </p>
                        <p>
                            پس از ثبت نام در روش سنتی - کاربران - به دلیل عدم آگاهی از آنچه در طول آموزش در انتظارشان
                            است نا امید و پشیمان میشدند ، زیرا در ابتدا و پیش از ثبت نام اطلاعات کافی به آنها نمیرسید،
                            ... به همین دلیل مصمم شدیم با استفاده از کامل ترین اطلاعات و همچنین ارایه بستری برای مشاهده
                            تجربه کاربران سابق یک کلاس، شما کوچک ترین دغدغه ای برای انتخاب صحیح خود در ارتباط با یک کلاس
                            و یا آموزشگاه نداشته باشند .
                        </p>
                        <p>
                            امروز کارشناسان نیکارو روزانه 12 ساعت، آماده پاسخگویی به متقاضیانی هستند که ممکن است برای
                            انتخاب کلاس و یا آموزشگاه ابهامی داشته باشند.
                        </p>
                        <p>
                            مسیر رشد نیکارو در کنار شما ادامه دارد... و درگام بعدی به این مهم فکر میکنیم که در مسیر
                            یادگیری نیز همراهتان باشیم.
                        </p>

                    </div>
                </div>
            </>
        );
    }
}

