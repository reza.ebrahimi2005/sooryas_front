import React, {Component} from 'react';
import Cookies from "js-cookie";

export async function getServerSideProps(context) {
    // Fetch data from external API
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + '/paymentStatus?refId=' + context.query.refId);
    const data = await res.json();
    // Pass data to the page via props
    return {props: {"data": data}}
}

export default class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            app: (this.props.type === 'app')
        };
    }

    render() {
        console.log(this.state.data);
        var content;
        if (this.state.data.statusCode === "200") {
            content =
                <>
                    <img className='d-flex justify-content-center align-items-center'
                         src={'/assets/images/success.png'}/>
                    <h5 className='text-success d-block mt-4'>پرداخت موفق</h5>
                    <span className='d-block mt-2'>کد پیگیری: {this.state.data.code}</span>
                    {
                        (this.state.app)
                            ?
                            <a href="nikarocustomer://" className="btn btn-primary">بازگشت به برنامه نیکارو</a>
                            :
                            <a href='/' className='btn btn-primary text-white mt-4'>بازگشت</a>
                    }
                </>
        } else {
            content =
                <>
                    <img className='d-flex justify-content-center align-items-center'
                         src={'/assets/images/fail.png'}/>
                    <h5 className='text-danger d-block mt-4'>پرداخت ناموفق</h5>
                    <span className='d-block mt-2'>علت خطا:{this.state.data.message}</span>
                    {
                        (this.state.app)
                            ?
                            <a href="nikarocustomer://" className="btn btn-primary">بازگشت به برنامه </a>
                            :
                            <a href='/' className='btn btn-primary text-white mt-4'>بازگشت</a>
                    }
                </>
        }
        return (
            <div id='paymentResult'>
                {content}
            </div>
        );
    }
}
