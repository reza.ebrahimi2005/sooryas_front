import React, {Component} from 'react';
import Head from 'next/head'

export default class Contact extends Component {
    render() {
        return (
            <>
                <Head>
                    <title>تماس با ما | نیکارو</title>
                    <meta name="description" property="description" content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:url" content="https://nikaro.ir/contact"/>
                    <meta property="og:title" content="تماس با ما | نیکارو"/>
                    <meta property="og:description"
                          content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید."/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image"
                          content="https://nikaro.ir/assets/images/nikaroWithBg.png"/>
                    <meta property="og:site_name" content="نیکارو"/>
                    <meta name="twitter:card" content="summary" />
                    <meta name="twitter:site" content="@nikaro_ir" />
                    <meta name="twitter:title" content="تماس با ما | نیکارو" />
                    <meta name="twitter:description" content="در نیکارو می توانید به بهترین و مناسب ترین کلاس های آموزشی، ورزشی و مهارتی مورد نیاز خود دسترسی داشته باشید و به صورت آنلاین ثبت نام کنید." />
                    <link rel="canonical" href="https://nikaro.ir/contact"/>
                </Head>
                <div className='row d-flex justify-content-center' id='contactUs'>
                    <div className='col-12 title align-self-start mt-md-5 mt-2'>
                        <span className='d-block text-center'>تماس با ما</span>
                        <hr className='rounded-pill'/>
                    </div>
                    <div className='col-12 col-md-8 text-right'>
                        <p className='text-justify'>
                            باعث افتخار و خرسندی ماست که میخواهید با نیکارو تماس بگیرید. برای اینکه بتوانیم به بهترین و
                            سریع
                            ترین شکل ممکن بتوانیم پاسخگوی شما همکاران گرامی باشیم روش های‌ مشخصی در نظر گرفته شده است.
                            پیش
                            از
                            اینکه با نیکارو تماس بگیرید، صفحه <a className='text-primary' href="https://help.nikaro.ir">سولات
                            متداول</a> را مطالعه فرمایید. ممکن است سؤال
                            شما را قبلا در لیست پاسخ داده شده باشد.
                        </p>
                        <span className='d-block'>
                        <span className='text-primary'>نشانی: </span>
                         مشهد - بلوار سجاد، نبش امین 2 شرقی، پلاک 28، ساختمان پارک علم و فناوری، کد پستی: 9187934789
                    </span>
                        <span className='d-block mt-2'>
                        <span className='text-primary'>نشانی: </span>
                         تهران - دهکده المپیک، میدان دهکده، ساختمان مرکز رشد دانشگاه علامه طباطبایی، طبقه چهارم
                    </span>
                        <span className='d-block d-md-inline-block mt-2'>
                        <span className='text-primary'>واحد مشتریان و امور ثبت نام:  </span>
                        36024778-051
                    </span>
                        <span className='d-block d-md-inline-block mt-2 mr-md-3'>
                        <span className='text-primary'>واحد امور آموزشگاه ها:  </span>
                        36024819-051
                    </span>
                        <span className='d-block  d-md-inline-block mt-2 mr-md-3'>
                        <span className='text-primary'>پشتیبانی 24 ساعته: </span>
                         09916367451
                    </span>
                    </div>
                    <div className='col-12 col-md-8 text-right'>
                        <span className='d-block mb-4 mt-2 mt-md-0'>کاربر گرامی هرگونه نظر، انتقاد یا مشکلی را میتوانید از طریق فرم زیر با ما در میان بگذارید :</span>
                        <form className='justify-content-center'>
                            <div className="form-group col-4 d-inline-block">
                                <label className="control-label"> نام و نام خانواگی</label>
                                <input type="text" className="form-control"/>
                            </div>
                            <div className="form-group col-4 d-inline-block">
                                <label className="control-label"> ایمیل</label>
                                <input type="email" className="form-control"/>
                            </div>
                            <div className="form-group col-4 d-inline-block">
                                <label className="control-label">شماره همراه</label>
                                <input type="number" className="form-control"/>
                            </div>
                            <div className="form-group col-12">
                                <label className="control-label">متن پیام</label>
                                <textarea className="form-control" rows="3"/>
                            </div>
                            <div className='form-group col-12'>
                                <button className='btn btn-primary d-block mx-auto' type="button">ارسال</button>
                            </div>
                        </form>
                    </div>
                </div>
            </>
        );
    }
}

