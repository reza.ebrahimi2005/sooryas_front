const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css')
const withFonts = require('nextjs-fonts');

module.exports =
    withCSS(
        withSass(
            withFonts({
                    webpack(config, {isServer}) {
                        if (!isServer) {
                            config.node = {
                                fs: 'empty'
                            }
                        }
                        return config;
                    },
                }
            )
        )
    );
